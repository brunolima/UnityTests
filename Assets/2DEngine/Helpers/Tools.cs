using UnityEngine;

public class Tools {
  public static bool IsInLayerMask(GameObject obj, LayerMask mask) {
    return ((mask.value & (1 << obj.layer)) > 0);
  }

  public static bool CollidedWithMask(Collision2D collision, Collider2D myCollider, LayerMask layerMask) {
    if (Tools.IsInLayerMask(collision.gameObject, layerMask)) {
      foreach (ContactPoint2D hit in collision.contacts) {
        if (hit.otherCollider == myCollider) {
          return true;
        };
      }
    }
    return false;
  }

  public static int NextEnumValue(System.Type enumType, int initialValue) {
    int length = System.Enum.GetValues(enumType).Length;
    initialValue++;
    if ((int)initialValue == length) { initialValue = 0; }
    return initialValue;
  }

  public static string ToS(object value) {
    return value.ToString();
  }

  public static int GetValueSign(float value) {
    if (value == 0) {
      return 0;
    }
    return (int)Mathf.Sign(value);
  }

  public static Rect RectLerp(Rect a, Rect b, float t) {
    float x = Mathf.Lerp(a.x, b.x, t);
    float y = Mathf.Lerp(a.y, b.y, t);
    float width = Mathf.Lerp(a.width, b.width, t);
    float height = Mathf.Lerp(a.height, b.height, t);
    return new Rect(x, y, width, height);
  }
}