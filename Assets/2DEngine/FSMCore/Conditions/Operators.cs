using System;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public enum Operators { EQUAL, NOT_EQUAL, GREATER_THAN, LESS_THAN }

      static class OperatorsMethods {
        public static bool Compare(this Operators o, bool param1, bool param2) {
          switch (o) {
          case Operators.EQUAL:
            return param1 == param2;
          case Operators.NOT_EQUAL:
            return param1 != param2;
          default:
            return false;
          }
        }

        public static bool Compare(this Operators o, float param1, float param2) {
          switch (o) {
          case Operators.EQUAL:
            return param1 == param2;
          case Operators.NOT_EQUAL:
            return param1 != param2;
          case Operators.LESS_THAN:
            return param1 > param2;
          case Operators.GREATER_THAN:
            return param1 < param2;
          default:
            return false;
          }
        }

        public static bool Compare(this Operators o, int param1, int param2) {
          switch (o) {
          case Operators.EQUAL:
            return param1 == param2;
          case Operators.NOT_EQUAL:
            return param1 != param2;
          case Operators.LESS_THAN:
            return param1 > param2;
          case Operators.GREATER_THAN:
            return param1 < param2;
          default:
            return false;
          }
        }

        public static bool Compare(this Operators o, int[] param1, int param2) {
          switch (o) {
          case Operators.EQUAL:
            return  Array.IndexOf(param1, param2) > -1;
          case Operators.NOT_EQUAL:
            return  Array.IndexOf(param1, param2) == -1;
          default:
            return false;
          }
        }
      }
    }
  }
}