using System;
using UnityEngine;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class ObjectCondition : ICondition {

        public ObjectCondition(string conditionVariable) {
          this.conditionVariable = conditionVariable;
        }

        public override bool IsValid(object value) {
          return value != null;
        }

      }
    }
  }
}