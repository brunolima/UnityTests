using UnityEngine;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class TimeCondition : ICondition {
        public float baseValue;
        public float startTime;

        public TimeCondition(float baseValue) {
          this.baseValue = baseValue;
        }

        public override bool IsValid(object value) {
          return (float)value - this.startTime > this.baseValue;
        }

      }
    }
  }
}