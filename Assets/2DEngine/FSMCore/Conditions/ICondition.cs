namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public abstract class ICondition {
        public string conditionVariable;
        public Operators conditionOperator;
        public abstract bool IsValid(object value);
      }
    }
  }
}