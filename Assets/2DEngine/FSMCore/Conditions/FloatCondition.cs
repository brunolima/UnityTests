using System;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class FloatCondition : ICondition {
        public float baseValue;
        public bool absolute;

        public FloatCondition(string conditionVariable, Operators conditionOperator, float baseValue, bool absolute = false) {
          this.conditionVariable = conditionVariable;
          this.conditionOperator = conditionOperator;
          this.baseValue = baseValue;
          this.absolute = absolute;
        }

        public override bool IsValid(object value) {
          float floatValue = (float) value;
          if (this.absolute) {
            floatValue = Math.Abs(floatValue);
          }
          return this.conditionOperator.Compare(this.baseValue, floatValue);
        }
      }
    }
  }
}