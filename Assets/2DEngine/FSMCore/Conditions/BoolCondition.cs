namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class BoolCondition : ICondition {
        public bool baseValue;

        public BoolCondition(string conditionVariable, Operators conditionOperator, bool baseValue) {
          this.conditionVariable = conditionVariable;
          this.conditionOperator = conditionOperator;
          this.baseValue = baseValue;
        }

        public override bool IsValid(object value) {
          return this.conditionOperator.Compare(this.baseValue, (bool)value);
        }
      }
    }
  }
}