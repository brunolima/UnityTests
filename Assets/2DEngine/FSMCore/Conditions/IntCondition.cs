using System;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class IntCondition : ICondition {
        public int baseValue;
        public bool absolute;

        public IntCondition(string conditionVariable, Operators conditionOperator, int baseValue, bool absolute = false) {
          this.conditionVariable = conditionVariable;
          this.conditionOperator = conditionOperator;
          this.baseValue = baseValue;
          this.absolute = absolute;
        }

        public override bool IsValid(object value) {
          int intValue = (int) value;
          if (this.absolute) {
            intValue = (int) Math.Abs(intValue);
          }
          return this.conditionOperator.Compare(this.baseValue, intValue);
        }
      }
    }
  }
}