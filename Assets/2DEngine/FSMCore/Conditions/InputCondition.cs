using System;
using TwoDEngine.Inputs;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class InputCondition : ICondition {
        public int[] baseValues;

        public InputCondition(string conditionVariable, Operators conditionOperator, params InputModes[] baseValues) {
          this.conditionVariable = conditionVariable;
          this.conditionOperator = conditionOperator;
          this.baseValues = Array.ConvertAll(baseValues, value => (int) value);
        }

        public override bool IsValid(object value) {
          return this.conditionOperator.Compare(this.baseValues, (int)value);
        }
      }
    }
  }
}