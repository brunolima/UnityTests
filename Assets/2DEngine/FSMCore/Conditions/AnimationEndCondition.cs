using System;
using UnityEngine;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Conditions {
      public class AnimationEndCondition : ICondition {
        public Animator animator;
        public string animationState;
        public float startTime;

        public AnimationEndCondition(Animator animator, string animationState) {
          this.animator = animator;
          this.animationState = animationState;
        }

        public override bool IsValid(object value) {
          if (this.animator.GetCurrentAnimatorStateInfo(0).IsName(this.animationState)) {
            return (float)value - this.startTime > this.animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
          }
          return false;
        }

      }
    }
  }
}