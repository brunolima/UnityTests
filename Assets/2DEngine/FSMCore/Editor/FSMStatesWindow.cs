using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace FSMCore {

    public class FSMStatesWindow : EditorWindow {
      private Vector2 scrollPos;

      [MenuItem("TwoDEngine/FSM/States")]
      public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(FSMStatesWindow));
      }

      void OnGUI() {
        if (Selection.activeGameObject == null) {
          GUILayout.Label("No GameObject Selected", EditorStyles.boldLabel);
          return;
        }
        FiniteStateMachineController fsmController = Selection.activeGameObject.GetComponent<FiniteStateMachineController>();
        if (fsmController == null || fsmController.fsm == null) {
          GUILayout.Label("GameObject '" + Selection.activeGameObject.name + "' doesn't have a FiniteStateMachineController", EditorStyles.boldLabel);
          return;
        }
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);
        this.ShowStates(fsmController.fsm);
        EditorGUILayout.EndScrollView();
      }

      public void OnInspectorUpdate() {
        this.Repaint();
      }

      void ShowStates(FiniteStateMachine fsm) {
        GUILayout.Label("States", EditorStyles.boldLabel);
        if (fsm.currentState != null) {
          GUILayout.Space(2);
          GUILayout.BeginHorizontal();
          EditorGUILayout.LabelField("Current State", fsm.currentState.state, EditorStyles.boldLabel);
          GUILayout.EndHorizontal();
        }
        GUILayout.Space(2);
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Previous State", fsm.previousStateName, EditorStyles.boldLabel);
        GUILayout.EndHorizontal();
        GUILayout.Label("Registered States", EditorStyles.boldLabel);
        Dictionary<string, FSMState> states = fsm.states;
        foreach (string variable in states.Keys) {
          GUILayout.Space(2);
          GUILayout.BeginHorizontal();
          GUILayout.Label(variable);
          GUILayout.EndHorizontal();
        }
      }
    }
  }
}