using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace FSMCore {

    public class FSMVariablesWindow : EditorWindow {
      private Vector2 scrollPos;

      [MenuItem("TwoDEngine/FSM/Variables")]
      public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(FSMVariablesWindow));
      }

      void OnGUI() {
        if (Selection.activeGameObject == null) {
          GUILayout.Label("No GameObject Selected", EditorStyles.boldLabel);
          return;
        }
        FiniteStateMachineController fsmController = Selection.activeGameObject.GetComponent<FiniteStateMachineController>();
        if (fsmController == null || fsmController.fsm == null) {
          GUILayout.Label("GameObject '" + Selection.activeGameObject.name + "' doesn't have a FiniteStateMachineController", EditorStyles.boldLabel);
          return;
        }
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);
        this.ShowCooldowns(fsmController.fsm);
        this.ShowVariables(fsmController.fsm);
        EditorGUILayout.EndScrollView();
      }

      public void OnInspectorUpdate() {
        this.Repaint();
      }

      void ShowCooldowns(FiniteStateMachine fsm) {
        GUILayout.Label("Cooldowns", EditorStyles.boldLabel);
        Dictionary<string, float> variables = fsm.cooldowns;
        foreach (string variable in variables.Keys) {
          GUILayout.Space(2);
          GUILayout.BeginHorizontal();
          string name = variable;
          EditorGUILayout.LabelField(name, variables[variable].ToString(), EditorStyles.boldLabel);
          GUILayout.EndHorizontal();
        }
      }
      void ShowVariables(FiniteStateMachine fsm) {
        GUILayout.Label("Variables", EditorStyles.boldLabel);
        Dictionary<string, object> variables = fsm.GetVariables();
        foreach (string variable in variables.Keys) {
          GUILayout.Space(2);
          GUILayout.BeginHorizontal();
          string name = variable;
          if (fsm.GetLock(variable)) {
            name += " (LOCKED)";
          }
          EditorGUILayout.LabelField(name, variables[variable].ToString(), EditorStyles.boldLabel);
          GUILayout.EndHorizontal();
        }
      }
    }
  }
}