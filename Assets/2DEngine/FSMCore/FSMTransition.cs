using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;

namespace TwoDEngine {
  namespace FSMCore {
    public class FSMTransition {
      public string fromState;
      public string toState;
      public List<ICondition> conditions = new List<ICondition>();

      public FSMTransition(string fromState, string toState) {
        this.fromState = fromState;
        this.toState = toState;
      }

      public void AddCondition(ICondition condition) {
        this.conditions.Add(condition);
      }

      public void AddConditions(List<ICondition> conditions) {
        this.conditions.AddRange(conditions);
      }

      public bool TransitionValid(Dictionary<string, object> parameters) {
        foreach (ICondition condition in this.conditions) {
          if (condition.GetType() == typeof(TimeCondition) || condition.GetType() == typeof(AnimationEndCondition)) {
            if (!condition.IsValid(Time.time)) {
              return false;
            }
          } else if (!parameters.ContainsKey(condition.conditionVariable) || !condition.IsValid(parameters[condition.conditionVariable])) {
            return false;
          }
        }
        return true;
      }

      public void UpdateConditions() {
        foreach (ICondition condition in this.conditions) {
          if (condition.GetType() == typeof(TimeCondition)) {
            ((TimeCondition)condition).startTime = Time.time;
          } else if (condition.GetType() == typeof(AnimationEndCondition)) {
            ((AnimationEndCondition)condition).startTime = Time.time;
          }
        }
      }

    }
  }
}