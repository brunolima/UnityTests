using UnityEngine;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace FSMCore {

    public class FiniteStateMachine {
      private FSMState initialState;
      public FSMState currentState;
      public Dictionary<string, FSMState> states = new Dictionary<string, FSMState>();
      public Dictionary<string, List<FSMTransition>> transitions = new Dictionary<string, List<FSMTransition>>();
      public Dictionary<string, float> cooldowns = new Dictionary<string, float>();
      protected Dictionary<string, object> variables = new Dictionary<string, object>();
      protected Dictionary<string, bool> locks = new Dictionary<string, bool>();
      protected bool isWorking;

      public string previousStateName;

      public delegate void Transition(string toState);
      public event Transition OnTransition;

      public void Start() {}

      public void StartMachine(FSMState initialState) {
        this.RegisterState(initialState);
        this.initialState = initialState;
        this.currentState = initialState;
        this.currentState.OnStart(ref this.variables, this.previousStateName);
        this.isWorking = true;
      }

      public void StopMachine() {
        this.isWorking = false;
      }

      public void Update() {
        if (!this.isWorking) {
          return;
        }
        this.CheckCooldowns();
        if (this.currentState.isFinalState) {
          this.StopMachine();
          // Debug.Log("Ending Machine with " + this.currentState.state);
          this.currentState.OnExit(ref this.variables, this.previousStateName, null);
        } else {
          this.currentState.Update(ref this.variables, this.previousStateName);
          foreach (FSMTransition transition in this.transitions[this.currentState.state]) {
            string toState = transition.toState;
            if (transition.TransitionValid(this.variables) && states.ContainsKey(toState)) {
              this.currentState.OnExit(ref this.variables, this.previousStateName, toState);
              // Debug.Log("Transitioning FROM " + this.currentState.state + " TO " + toState);
              if (this.OnTransition != null) {
                this.OnTransition(toState);
              }
              this.previousStateName = this.currentState.state;
              this.currentState = this.states[toState];
              this.currentState.OnStart(ref this.variables, this.previousStateName);
              foreach (FSMTransition newStateTransition in this.transitions[toState]) {
                newStateTransition.UpdateConditions();
              }
              break;
            }
          }
        }
      }

      public void AddCooldown(string variable, float cooldown, bool defaultValue = false) {
        this.cooldowns[variable] = Time.time + cooldown;
        this.variables[variable] = defaultValue;
      }

      void CheckCooldowns() {
        List<string> cooldownDone = new List<string>();
        foreach (string key in this.cooldowns.Keys) {
          if (((float)this.cooldowns[key]) - Time.time <= 0) {
            this.variables[key] = !((bool)this.variables[key]);
            cooldownDone.Add(key);
          }
        }
        foreach (string key in cooldownDone) {
          this.cooldowns.Remove(key);
        }
      }

      public void RegisterState(FSMState fsmState) {
        this.states[fsmState.state] = fsmState;
        if (!this.transitions.ContainsKey(fsmState.state)) {
          this.transitions[fsmState.state] = new List<FSMTransition>();
        }
        // Debug.Log("State Registered: " + fsmState.state + " with " + fsmState.startBehaviours.Count + ", " + fsmState.updateBehaviours.Count + " and " + fsmState.exitBehaviours.Count + " behaviours. There are " + this.states.Count + " states registered");
      }

      public void RemoveState(FSMState fsmState) {
        this.states.Remove(fsmState.state);
        this.transitions.Remove(fsmState.state);
        if (this.currentState == fsmState && !this.currentState.isFinalState) {
          this.currentState.OnExit(ref this.variables, this.previousStateName, this.initialState.state);
          this.previousStateName = this.currentState.state;
          this.currentState = initialState;
          this.currentState.OnStart(ref this.variables, this.previousStateName);
        }
        // Debug.Log("State Removed: " + fsmState.state + ". There are " + this.states.Count + " states registered");
        // Debug.Log("Transition removed. There are transition from " + this.transitions.Count + " states registered");
      }

      public void RegisterTransition(FSMTransition transition) {
        if (!this.transitions.ContainsKey(transition.fromState)) {
          this.transitions[transition.fromState] = new List<FSMTransition>();
        }
        transition.UpdateConditions();
        this.transitions[transition.fromState].Add(transition);
        // Debug.Log("Transition registered from state " + transition.fromState + " to " + transition.toState + " with " + transition.conditions.Count + " conditions. There are transition from " + this.transitions.Count + " states registered");
      }

      public void UpdateVariable(string name, object value) {
        if (!this.locks.ContainsKey(name)) {
          this.locks[name] = false;
        }
        if (!this.locks[name]) {
          this.variables[name] = value;
        }
      }

      public Dictionary<string, object> GetVariables() {
        return this.variables;
      }

      public bool GetLock(string name) {
        return this.locks[name];
      }

      public object GetVariable(string name) {
        return this.variables[name];
      }

      public void LockVariables(params string[] names) {
        foreach (string name in names) {
          this.locks[name] = true;
        }
      }

      public void UnlockVariables(params string[] names) {
        foreach (string name in names) {
          this.locks[name] = false;
        }
      }
    }
  }
}