using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class UpdateVelocityBehaviour : FSMBehaviour {
        private Vector2 multiplier;
        private Rigidbody2D rigidBody;

        public UpdateVelocityBehaviour(GameObject gameObject, Vector2 multiplier) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
          this.multiplier = multiplier;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          Vector2 velocity = this.rigidBody.velocity;
          velocity.Set(velocity.x * this.multiplier.x, velocity.y * this.multiplier.y);
          this.rigidBody.velocity = velocity;
        }
      }
    }
  }
}