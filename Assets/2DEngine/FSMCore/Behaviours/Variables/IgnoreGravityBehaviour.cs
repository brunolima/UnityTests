using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class IgnoreGravityBehaviour : FSMBehaviour {
        private Rigidbody2D rigidBody;

        public IgnoreGravityBehaviour(GameObject gameObject) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.rigidBody.gravityScale = 0f;
        }
      }
    }
  }
}