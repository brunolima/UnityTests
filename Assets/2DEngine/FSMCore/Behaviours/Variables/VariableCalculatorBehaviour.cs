using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class VariableCalculatorBehaviour : FSMBehaviour {
        private string variableName;
        private float floatValue;
        private bool addValue;
        private bool useFloat;

        private int intValue;

        public VariableCalculatorBehaviour(GameObject gameObject, string variableName) : base(gameObject) {
          this.variableName = variableName;
        }

        public VariableCalculatorBehaviour Add(float floatValue) {
          this.floatValue = floatValue;
          this.addValue = true;
          this.useFloat = true;
          return this;
        }

        public VariableCalculatorBehaviour Subtract(float floatValue) {
          this.floatValue = floatValue;
          this.useFloat = true;
          return this;
        }

        public VariableCalculatorBehaviour Add(int intValue) {
          this.intValue = intValue;
          this.addValue = true;
          this.useFloat = false;
          return this;
        }

        public VariableCalculatorBehaviour Subtract(int intValue) {
          this.intValue = intValue;
          this.useFloat = false;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          object newValue = 0;
          if (this.useFloat) {
            newValue = this.CalculateFloat(parameters);
          } else {
            newValue = this.CalculateInt(parameters);
          }
          parameters[this.variableName] = newValue;
        }

        private float CalculateFloat(Dictionary<string, object> parameters) {
          float originalValue = (float) parameters[this.variableName];
          if (this.addValue) {
            originalValue += floatValue;
          } else {
            originalValue -= floatValue;
          }
          return originalValue;
        }

        private int CalculateInt(Dictionary<string, object> parameters) {
          int originalValue = (int) parameters[this.variableName];
          if (this.addValue) {
            originalValue += intValue;
          } else {
            originalValue -= intValue;
          }
          return originalValue;
        }

      }
    }
  }
}