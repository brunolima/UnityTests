using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class SetVariableBehaviour : FSMBehaviour {
        private string variableName;
        private object value;
        private List<ICondition> conditions = new List<ICondition>();

        public SetVariableBehaviour(GameObject gameObject, string variableName, object value) : base(gameObject) {
          this.variableName = variableName;
          this.value = value;
        }

        public SetVariableBehaviour WithCondition(ICondition condition) {
          this.conditions.Add(condition);
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          foreach (ICondition condition in this.conditions) {
            if (!condition.IsValid(parameters[condition.conditionVariable])) {
              return;
            }
          }
          parameters[this.variableName] = this.value;
        }
      }
    }
  }
}