using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class LockAxisBehaviour : FSMBehaviour {
        private FiniteStateMachine fsm;
        private string xModeVariable;
        private string xValueVariable;
        private string yModeVariable;
        private string yValueVariable;
        private bool xor;

        public LockAxisBehaviour(GameObject gameObject, FiniteStateMachine fsm) : base(gameObject) {
          this.fsm = fsm;
        }

        public LockAxisBehaviour WithXInput(string modeVariable, string valueVariable) {
          this.xModeVariable = modeVariable;
          this.xValueVariable = valueVariable;
          return this;
        }

        public LockAxisBehaviour WithYInput(string modeVariable, string valueVariable) {
          this.yModeVariable = modeVariable;
          this.yValueVariable = valueVariable;
          return this;
        }

        public LockAxisBehaviour AsXOR() {
          this.xor = true;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.fsm.LockVariables(this.xModeVariable, this.xValueVariable, this.yModeVariable, this.yValueVariable);
          int xMode = (int) parameters[this.xModeVariable];
          float xValue = (float) parameters[this.xValueVariable];
          int yMode = (int) parameters[this.yModeVariable];
          float yValue = (float) parameters[this.yValueVariable];
          if (xValue > 0) {
            xValue = 1;
          } else if (xValue < 0) {
            xValue = -1;
          }
          if (yValue > 0) {
            yValue = 1;
          } else if (yValue < 0) {
            yValue = -1;
          }
          if (this.xor) {
            if (xMode > yMode && xValue != 0) {
              yValue = 0;
            } else if (yMode > xMode && yValue != 0) {
              xValue = 0;
            }
          }
          parameters[this.xValueVariable] = xValue;
          parameters[this.yValueVariable] = yValue;
        }
      }
    }
  }
}