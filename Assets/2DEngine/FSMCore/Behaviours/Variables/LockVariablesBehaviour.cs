using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class LockVariablesBehaviour : FSMBehaviour {
        private FiniteStateMachine fsm;
        private string[] variables;

        public LockVariablesBehaviour(GameObject gameObject, FiniteStateMachine fsm, params string[] variables) : base(gameObject) {
          this.fsm = fsm;
          this.variables = variables;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.fsm.LockVariables(variables);
        }
      }
    }
  }
}