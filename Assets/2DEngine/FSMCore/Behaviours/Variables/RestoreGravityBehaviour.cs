using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class RestoreGravityBehaviour : FSMBehaviour {
        private string variableName;
        private Rigidbody2D rigidBody;

        public RestoreGravityBehaviour(GameObject gameObject, string variableName) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
          this.variableName = variableName;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.rigidBody.gravityScale = (float)parameters[this.variableName];
        }
      }
    }
  }
}