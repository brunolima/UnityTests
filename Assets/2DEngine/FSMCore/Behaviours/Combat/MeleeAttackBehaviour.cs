using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class MeleeAttackBehaviour : FSMBehaviour {
        public float hitRadius;
        public string actionChecksVariable;
        public string whatIsEnemyVariable;
        public float baseDamage;
        public string chargeVariable;
        public int effect;
        public string facingDirectionVariable;

        public string verticalDirectionVariable;
        public float verticalDirectionDefaultValue;
        public float verticalDirectionMinValue;
        public float verticalDirectionMaxValue;

        public string horizontalDirectionVariable;
        public float horizontalDirectionDefaultValue;
        public bool horizontalDirectionForwards;

        public float effectForce;

        public List<GameObject> enemiesHit = new List<GameObject>();

        public MeleeAttackBehaviour(GameObject gameObject, string actionChecksVariable, float hitRadius, string whatIsEnemyVariable, string facingDirectionVariable) : base(gameObject) {
          this.actionChecksVariable = actionChecksVariable;
          this.hitRadius = hitRadius;
          this.whatIsEnemyVariable = whatIsEnemyVariable;
          this.facingDirectionVariable = facingDirectionVariable;
        }

        public MeleeAttackBehaviour WithDamage(float damage) {
          this.baseDamage = damage;
          return this;
        }

        public MeleeAttackBehaviour WithCharge(string chargeVariable) {
          this.chargeVariable = chargeVariable;
          return this;
        }

        public MeleeAttackBehaviour WithEffect(int effect) {
          this.effect = effect;
          return this;
        }

        public MeleeAttackBehaviour WithEffectVerticalDirection(string variable, float defaultValue, float minValue, float maxValue) {
          this.verticalDirectionVariable = variable;
          this.verticalDirectionDefaultValue = defaultValue;
          this.verticalDirectionMinValue = minValue;
          this.verticalDirectionMaxValue = maxValue;
          return this;
        }

        public MeleeAttackBehaviour WithEffectHorizontalDirection(string variable, float defaultValue, bool forwards) {
          this.horizontalDirectionVariable = variable;
          this.horizontalDirectionDefaultValue = defaultValue;
          this.horizontalDirectionForwards = forwards;
          return this;
        }

        public MeleeAttackBehaviour WithEffectForce(float force) {
          this.effectForce = force;
          return this;
        }

        public override void Reset() {
          this.enemiesHit.Clear();
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          LayerMask whatIsEnemy = (LayerMask) parameters[this.whatIsEnemyVariable];
          Transform[] actionChecks = (Transform[]) parameters[this.actionChecksVariable];
          foreach (Transform actionCheck in actionChecks) {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(actionCheck.position, this.hitRadius, whatIsEnemy);
            foreach (Collider2D collider in colliders) {
              if (collider.gameObject != this.gameObject && !this.enemiesHit.Contains(collider.gameObject)) {
                this.enemiesHit.Add(collider.gameObject);
                float chargePower = 1;
                if (this.chargeVariable != null) {
                  chargePower = (float) parameters[this.chargeVariable];
                }
                Vector2 effectDirection = this.CalculateDirection(parameters);
                collider.gameObject.BroadcastMessage("GetHit", new System.Object[] { this.baseDamage, this.gameObject, this.effect, chargePower, effectDirection, this.effectForce});
              }
            }
          }
        }

        private Vector2 CalculateDirection(Dictionary<string, object> parameters) {
          Vector2 direction = Vector2.zero;
          // Horizontal
          int facingDirection = (int) parameters[this.facingDirectionVariable];
          float hValue = this.horizontalDirectionDefaultValue;
          if (this.horizontalDirectionVariable != null) {
            hValue = (float) parameters[this.horizontalDirectionVariable];
            hValue = Mathf.Clamp(Mathf.Abs(hValue), 0, 1);
          }
          direction.x = facingDirection * hValue;
          if (!this.horizontalDirectionForwards) {
            direction.x *= -1;
          }
          // Vertical
          float vValue = this.verticalDirectionDefaultValue;
          if (this.verticalDirectionVariable != null) {
            vValue = (float) parameters[this.verticalDirectionVariable];
            vValue = Mathf.Clamp(Mathf.Abs(vValue), this.verticalDirectionMinValue, this.verticalDirectionMaxValue);
          }
          direction.y = vValue;
          return direction;
        }
      }
    }
  }
}