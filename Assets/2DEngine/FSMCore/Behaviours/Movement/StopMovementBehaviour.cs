using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class StopMovementBehaviour : FSMBehaviour {
        private Rigidbody2D rigidBody;
        private bool stopX;
        private bool stopY;

        public StopMovementBehaviour(GameObject gameObject) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
        }

        public StopMovementBehaviour WithXAxis() {
          this.stopX = true;
          return this;
        }

        public StopMovementBehaviour WithYAxis() {
          this.stopY = true;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          bool sleep = false;
          Vector2 velocity = this.rigidBody.velocity;
          if (this.stopX) {
            if (velocity.x != 0f) {
              sleep = true;
            }
            velocity.x = 0f;
            this.rigidBody.angularVelocity = 0f;
          }
          if (this.stopY) {
            if (velocity.y != 0f) {
              sleep = true;
            }
            velocity.y = 0f;
            this.rigidBody.angularVelocity = 0f;
          }
          this.rigidBody.velocity = velocity;
          if (sleep) {
            this.rigidBody.Sleep();
          }
        }
      }
    }
  }
}