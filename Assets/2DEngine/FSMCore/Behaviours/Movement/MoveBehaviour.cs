using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class MoveBehaviour : FSMBehaviour {
        protected Rigidbody2D rigidBody;
        protected string speedVariable;
        protected string forceVariable;
        protected string speedMultiplierVariable;
        protected string hitObstacleVariable;
        protected string obstacleDirectionVariable;
        protected string xInputVariable;
        protected string yInputVariable;
        protected bool moveX;
        protected bool moveY;
        protected string hitGroundVariable;
        protected string hitCeilingVariable;

        public MoveBehaviour(GameObject gameObject) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
        }

        public MoveBehaviour WithSpeed(string speedVariable, string forceVariable, string speedMultiplierVariable) {
          this.speedVariable = speedVariable;
          this.forceVariable = forceVariable;
          this.speedMultiplierVariable = speedMultiplierVariable;
          return this;
        }

        public MoveBehaviour InXAxis(string inputVariable) {
          this.moveX = true;
          this.xInputVariable = inputVariable;
          return this;
        }

        public MoveBehaviour InYAxis(string inputVariable) {
          this.moveY = true;
          this.yInputVariable = inputVariable;
          return this;
        }

        public MoveBehaviour AvoidObstacle(string hitObstacleVariable, string obstacleDirectionVariable) {
          this.hitObstacleVariable = hitObstacleVariable;
          this.obstacleDirectionVariable = obstacleDirectionVariable;
          return this;
        }

        public MoveBehaviour AvoidGround(string hitGroundVariable) {
          this.hitGroundVariable = hitGroundVariable;
          return this;
        }

        public MoveBehaviour AvoidCeiling(string hitCeilingVariable) {
          this.hitCeilingVariable = hitCeilingVariable;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          float speed = (float) parameters[this.speedVariable];
          float force = (float) parameters[this.forceVariable];
          float speedMultiplier = (float) parameters[this.speedMultiplierVariable];
          if (this.moveX) {
            float xInput = (float) parameters[this.xInputVariable];
            this.MoveX(parameters, xInput, speed, force, speedMultiplier);
          }
          if (this.moveY) {
            float yInput = (float) parameters[this.yInputVariable];
            this.MoveY(parameters, yInput, speed, force, speedMultiplier);
          }
        }

        protected void MoveX(Dictionary<string, object> parameters, float input, float speed, float force, float speedMultiplier) {
          bool hitObstacle = (bool) parameters[this.hitObstacleVariable];
          int obstacleDirection = (int) parameters[this.obstacleDirectionVariable];
          if (hitObstacle && Tools.GetValueSign(input) == obstacleDirection) {
            return;
          }
          this.rigidBody.AddForce(Vector2.right * input * force * speedMultiplier);
          if (Mathf.Abs(this.rigidBody.velocity.x) > speed * speedMultiplier) {
            this.rigidBody.velocity = new Vector2(Mathf.Sign(this.rigidBody.velocity.x) * speed * speedMultiplier, this.rigidBody.velocity.y);
          }
        }

        protected void MoveY(Dictionary<string, object> parameters, float input, float speed, float force, float speedMultiplier) {
          bool hitGround = (bool) parameters[this.hitGroundVariable];
          bool hitCeiling = (bool) parameters[this.hitCeilingVariable];
          if ((hitGround && input < 0) || (hitCeiling && input > 0)) {
            this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.x, 0f);
            return;
          }
          this.rigidBody.AddForce(Vector2.up * input * force * speedMultiplier);
          if (Mathf.Abs(this.rigidBody.velocity.y) > speed * speedMultiplier) {
            this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.x, Mathf.Sign(this.rigidBody.velocity.y) * speed * speedMultiplier);
          }
        }

      }
    }
  }
}