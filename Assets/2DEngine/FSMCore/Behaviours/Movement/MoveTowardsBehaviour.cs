using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class MoveTowardsBehaviour : MoveBehaviour {
        private Renderer renderer;
        private GameObject target;
        private string targetVariable;
        private Rigidbody2D targetRigidBody;
        private bool reachX;
        private bool reachY;

        public MoveTowardsBehaviour(GameObject gameObject) : base(gameObject) {
          this.renderer = this.gameObject.GetComponent<Renderer>();
        }

        public MoveTowardsBehaviour WithTarget(GameObject target) {
          this.target = target;
          return this;
        }

        public MoveTowardsBehaviour WithTargetVariable(string targetVariable) {
          this.targetVariable = targetVariable;
          return this;
        }

        public override void Reset() {
          this.targetRigidBody = null;
          this.reachX = false;
          this.reachY = false;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          float speed = (float) parameters[this.speedVariable];
          float force = (float) parameters[this.forceVariable];
          float speedMultiplier = (float) parameters[this.speedMultiplierVariable];
          if (this.targetVariable != null) {
            this.target = (GameObject) parameters[this.targetVariable];
          }
          if (this.targetRigidBody == null) {
            this.targetRigidBody = this.target.GetComponent<Rigidbody2D>();
          }
          if (this.moveX) {
            if (!this.reachX) {
              float xSize = renderer.bounds.size.x / 2f;
              float xInput = 0f;
              if (this.target.transform.position.x > this.gameObject.transform.position.x + xSize) {
                xInput = 1f;
              } else if (this.target.transform.position.x < this.gameObject.transform.position.x - xSize) {
                xInput = -1f;
              } else {
                this.reachX = true;
              }
              this.MoveX(parameters, xInput, speed, force, speedMultiplier);
            } else {
              Vector3 velocity = this.rigidBody.velocity;
              velocity.x = targetRigidBody.velocity.x;
              this.rigidBody.velocity = velocity;
            }
          }
          if (this.moveY) {
            if (!this.reachY) {
              float ySize = renderer.bounds.size.y / 2f;
              float yInput = 0f;
              if (this.target.transform.position.y > this.gameObject.transform.position.y + ySize) {
                yInput = 1f;
              } else if (this.target.transform.position.y < this.gameObject.transform.position.y - ySize) {
                yInput = -1f;
              } else {
                this.reachY = true;
              }
              this.MoveY(parameters, yInput, speed, force, speedMultiplier);
            } else {
              Vector3 velocity = this.rigidBody.velocity;
              velocity.y = targetRigidBody.velocity.y;
              this.rigidBody.velocity = velocity;
            }
          }
        }

      }
    }
  }
}