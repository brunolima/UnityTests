using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class KeepInsideBoundsBehaviour : FSMBehaviour {
        private Renderer renderer;
        private Rigidbody2D rigidBody;
        private string minPositionVariable;
        private string maxPositionVariable;
        private string facingDirectionVariable;
        private string horizontalInputVariable;
        private string verticalInputVariable;
        private bool checkX;
        private bool checkY;
        private string storingVariable;

        public KeepInsideBoundsBehaviour(GameObject gameObject, string minPositionVariable, string maxPositionVariable) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
          this.renderer = this.gameObject.GetComponent<Renderer>();
          this.minPositionVariable = minPositionVariable;
          this.maxPositionVariable = maxPositionVariable;
        }

        public KeepInsideBoundsBehaviour WithXAxis() {
          this.checkX = true;
          return this;
        }

        public KeepInsideBoundsBehaviour WithYAxis() {
          this.checkY = true;
          return this;
        }

        public KeepInsideBoundsBehaviour WithHInput(string horizontalInputVariable, string facingDirectionVariable) {
          this.horizontalInputVariable = horizontalInputVariable;
          this.facingDirectionVariable = facingDirectionVariable;
          this.checkX = true;
          return this;
        }

        public KeepInsideBoundsBehaviour WithVInput(string verticalInputVariable) {
          this.verticalInputVariable = verticalInputVariable;
          this.checkY = true;
          return this;
        }

        public KeepInsideBoundsBehaviour StoringCollision(string variableName) {
          this.storingVariable = variableName;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          bool collision = false;
          Vector2 minPosition = (Vector2) parameters[this.minPositionVariable];
          Vector2 maxPosition = (Vector2) parameters[this.maxPositionVariable];
          float hInput = 0f;
          float vInput = 0f;
          //Check X
          if (this.checkX) {
            if (this.horizontalInputVariable != null) {
              hInput = (float) parameters[this.horizontalInputVariable];
            }
            float xSize = renderer.bounds.size.x;
            bool outsideMin = this.gameObject.transform.position.x + xSize <= minPosition.x;
            bool outsideMax = this.gameObject.transform.position.x - xSize >= maxPosition.x;
            if (outsideMin) {
              collision = true;
              this.rigidBody.velocity = new Vector2(0, this.rigidBody.velocity.y);
              this.gameObject.transform.position = new Vector3(minPosition.x - xSize, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
              if (hInput < 0) {
                this.FlipTo(parameters, FacingDirections.LEFT);
                parameters[this.horizontalInputVariable] = 0f;
              }
            } else if (outsideMax) {
              collision = true;
              this.rigidBody.velocity = new Vector2(0, this.rigidBody.velocity.y);
              this.gameObject.transform.position = new Vector3(maxPosition.x + xSize, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
              if (hInput > 0) {
                parameters[this.horizontalInputVariable] = 0f;
                this.FlipTo(parameters, FacingDirections.RIGHT);
              }
            }
          }
          //Check Y
          if (this.checkY) {
            if (this.verticalInputVariable != null) {
              vInput = (float) parameters[this.verticalInputVariable];
            }
            float ySize = renderer.bounds.size.y;
            bool outsideMin = this.gameObject.transform.position.y <= minPosition.y;
            bool outsideMax = this.gameObject.transform.position.y + ySize >= maxPosition.y;
            if (outsideMin) {
              collision = true;
              this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, minPosition.y, this.gameObject.transform.position.z);
              this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.x, 0f);
              if (vInput < 0) {
                parameters[this.verticalInputVariable] = 0f;
              }
            } else if (outsideMax) {
              collision = true;
              this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, maxPosition.y - ySize, this.gameObject.transform.position.z);
              this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.x, 0f);
              if (vInput > 0) {
                parameters[this.verticalInputVariable] = 0f;
              }
            }
          }
          if (this.storingVariable != null) {
            parameters[this.storingVariable] = collision;
          }
        }

        public void FlipTo(Dictionary<string, object> parameters, FacingDirections facingDirection) {
          parameters[this.facingDirectionVariable] = (FacingDirections)((int)facingDirection);
          Vector3 localScale = this.gameObject.transform.localScale;
          if (Tools.GetValueSign(localScale.x) != (int)facingDirection) {
            localScale.x *= -1;
          }
          this.gameObject.transform.localScale = localScale;
        }
      }
    }
  }
}