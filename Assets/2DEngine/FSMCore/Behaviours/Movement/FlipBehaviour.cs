using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class FlipBehaviour : FSMBehaviour {
        private string facingDirectionVariable;
        private string inputVariable;
        private string forcedDirectionVariable;
        private bool invert;

        public FlipBehaviour(GameObject gameObject, string facingDirectionVariable) : base(gameObject) {
          this.facingDirectionVariable = facingDirectionVariable;
        }

        public FlipBehaviour WithInput(string inputVariable) {
          this.inputVariable = inputVariable;
          return this;
        }

        public FlipBehaviour WithForcedDirection(string forcedDirectionVariable) {
          this.forcedDirectionVariable = forcedDirectionVariable;
          return this;
        }

        public FlipBehaviour Invert() {
          this.invert = true;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          int direction = (int)parameters[this.facingDirectionVariable];
          if (this.forcedDirectionVariable != null) {
            direction = (int)parameters[this.forcedDirectionVariable];
          }
          if (this.inputVariable != null) {
            int input = Tools.GetValueSign((float)parameters[this.inputVariable]);
            if (input != 0) {
              direction = input;
            }
          }
          if (this.invert) {
            direction *= -1;
          }
          parameters[this.facingDirectionVariable] = direction;
          Vector3 localScale = this.gameObject.transform.localScale;
          if (Tools.GetValueSign(localScale.x) != direction) {
            localScale.x *= -1;
            this.gameObject.transform.localScale = localScale;
          }
        }
      }
    }
  }
}