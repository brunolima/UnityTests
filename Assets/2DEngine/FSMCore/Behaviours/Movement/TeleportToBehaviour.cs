using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class TeleportToBehaviour : FSMBehaviour {
        private LayerMask obstacleLayer;
        private Renderer renderer;

        private GameObject target;
        private string targetVariable;
        private Rigidbody2D targetRigidBody;

        private float distance;

        public TeleportToBehaviour(GameObject gameObject, LayerMask obstacleLayer) : base(gameObject) {
          this.renderer = this.gameObject.GetComponent<Renderer>();
          this.obstacleLayer = obstacleLayer;
        }

        public TeleportToBehaviour WithTarget(GameObject target) {
          this.target = target;
          return this;
        }

        public TeleportToBehaviour WithTargetVariable(string targetVariable) {
          this.targetVariable = targetVariable;
          return this;
        }

        public TeleportToBehaviour TeleportAhead(float distance) {
          this.distance = distance;
          return this;
        }

        public override void Reset() {
          this.targetRigidBody = null;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          if (this.targetVariable != null) {
            this.target = (GameObject) parameters[this.targetVariable];
          }
          if (this.targetRigidBody == null) {
            this.targetRigidBody = this.target.GetComponent<Rigidbody2D>();
          }
          Vector2 realDistance = new Vector2(this.distance, this.distance);
          Vector2 direction = this.targetRigidBody.velocity.normalized;
          RaycastHit2D hit = Physics2D.Raycast(new Vector2(this.target.transform.position.x, this.target.transform.position.y + 0.5f), direction, this.distance, this.obstacleLayer);
          if (hit.collider != null) {
            realDistance = (Vector2)hit.point - (Vector2)this.target.transform.position;
          }
          Vector2 destinationPoint = new Vector2(this.target.transform.position.x, this.target.transform.position.y) + new Vector2(direction.x * Mathf.Abs(realDistance.x), direction.y * Mathf.Abs(realDistance.y));
          float xSize = renderer.bounds.size.x / 2f;
          float ySize = renderer.bounds.size.y / 2f;
          if (direction.x < 0) {
            destinationPoint.x += xSize;
          } else if (direction.x > 0) {
            destinationPoint.x -= xSize;
          }
          if (direction.y < 0) {
            destinationPoint.y += ySize;
          } else if (direction.y > 0) {
            destinationPoint.y -= ySize;
          }
          this.gameObject.transform.position = destinationPoint;
        }

      }
    }
  }
}