using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class ApplyForceBehaviour : FSMBehaviour {
        private Rigidbody2D rigidBody;
        private Vector2 direction;
        private string directionVariable;
        private float force;
        private string forceVariable;
        private string chargeVariable;
        private string xRelativeDirectionVariable;
        private bool backwards;

        public ApplyForceBehaviour(GameObject gameObject) : base(gameObject) {
          this.rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
          this.direction = Vector2.zero;
        }

        public ApplyForceBehaviour WithDirection(Vector2 direction) {
          this.direction = direction;
          return this;
        }

        public ApplyForceBehaviour WithDirectionVariable(string directionVariable) {
          this.directionVariable = directionVariable;
          return this;
        }

        public ApplyForceBehaviour WithForce(float force) {
          this.force = force;
          return this;
        }
        public ApplyForceBehaviour WithForceVariable(string forceVariable) {
          this.forceVariable = forceVariable;
          return this;
        }

        public ApplyForceBehaviour WithChargeVariable(string chargeVariable) {
          this.chargeVariable = chargeVariable;
          return this;
        }

        public ApplyForceBehaviour WithRelativeXDirection(string directionVariable, bool backwards = false) {
          this.xRelativeDirectionVariable = directionVariable;
          this.backwards = backwards;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          float charge = 1;
          Vector2 forceDirection = this.direction;
          float forceValue = this.force;
          if (this.directionVariable != null) {
            forceDirection = (Vector2) parameters[this.directionVariable];
          }
          if (this.forceVariable != null) {
            forceValue = (float) parameters[this.forceVariable];
          }
          if (this.chargeVariable != null) {
            charge = (float) parameters[this.chargeVariable];
          }
          if (this.xRelativeDirectionVariable != null) {
            int relativeDirection = (int) parameters[this.xRelativeDirectionVariable];
            if ((relativeDirection == 1 && forceDirection.x < 0) || (relativeDirection == -1 && forceDirection.x > 0)) {
              forceDirection.x *= -1;
            }
            if (this.backwards) {
              forceDirection.x *= -1;
            }
          }
          this.rigidBody.AddForce(forceDirection * forceValue * charge);
        }
      }
    }
  }
}