using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class InstantiateBehaviour : FSMBehaviour {
        public GameObject objectToInstantiate;
        public bool asChild;

        public InstantiateBehaviour(GameObject gameObject, GameObject objectToInstantiate) : base(gameObject) {
          this.objectToInstantiate = objectToInstantiate;
        }

        public InstantiateBehaviour AsChild(){
          this.asChild = true;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.objectToInstantiate.SetActive(false);
          GameObject instance = (GameObject) Object.Instantiate(this.objectToInstantiate);
          Vector3 position = this.gameObject.transform.TransformDirection(this.gameObject.transform.position);
          if(asChild){
            position = this.gameObject.transform.position;
            instance.transform.SetParent(this.gameObject.transform);
          }
          instance.transform.position = position;
          instance.SetActive(true);
        }
      }
    }
  }
}