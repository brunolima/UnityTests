using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class DestroyBehaviour : FSMBehaviour {

        public DestroyBehaviour(GameObject gameObject) : base(gameObject) {
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.gameObject.SetActive(false);
        }
      }
    }
  }
}