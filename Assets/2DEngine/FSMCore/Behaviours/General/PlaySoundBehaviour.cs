using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class PlaySoundBehaviour : FSMBehaviour {
        public AudioSource audioSource;
        public bool force = true;

        public PlaySoundBehaviour(GameObject gameObject, AudioSource audioSource) : base(gameObject) {
          this.audioSource = audioSource;
        }

        public PlaySoundBehaviour Force() {
          this.force = true;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          if (this.force || !this.audioSource.isPlaying) {
            audioSource.Play();
          }
        }

      }
    }
  }
}