using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class SetAnimationSpeedBehaviour : FSMBehaviour {
        private Animator animator;
        private float speed;

        public SetAnimationSpeedBehaviour(GameObject gameObject, float speed) : base(gameObject) {
          this.animator = this.gameObject.GetComponent<Animator>();
          this.speed = speed;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.animator.speed = this.speed;
        }
      }
    }
  }
}