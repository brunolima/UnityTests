using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class CountInputBehaviour : FSMBehaviour {
        private string inputVariableName;
        private int inputMode;
        private string resultVariableName;

        public CountInputBehaviour(GameObject gameObject, string inputVariableName, int inputMode, string resultVariableName) : base(gameObject) {
          this.inputVariableName = inputVariableName;
          this.inputMode = inputMode;
          this.resultVariableName = resultVariableName;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          int inputMode = (int) parameters[this.inputVariableName];
          if (inputMode == this.inputMode) {
            int count = (int) parameters[this.resultVariableName];
            parameters[this.resultVariableName] = count + 1;
          }
        }
      }
    }
  }
}