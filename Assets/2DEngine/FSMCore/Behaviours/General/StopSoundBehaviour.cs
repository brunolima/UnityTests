using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class StopSoundBehaviour : FSMBehaviour {
        public AudioSource audioSource;
        
        public StopSoundBehaviour(GameObject gameObject, AudioSource audioSource) : base(gameObject) {
          this.audioSource = audioSource;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          if(this.audioSource.isPlaying){
            audioSource.Stop();
          }
        }

      }
    }
  }
}