using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class CooldownBehaviour : FSMBehaviour {
        private string variableName;
        private float cooldown;
        private FiniteStateMachine fsm;

        public CooldownBehaviour(GameObject gameObject, FiniteStateMachine fsm, string variableName, float cooldown) : base(gameObject) {
          this.fsm = fsm;
          this.variableName = variableName;
          this.cooldown = cooldown;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          this.fsm.AddCooldown(this.variableName, this.cooldown);
        }
      }
    }
  }
}