using UnityEngine;
using System.Collections.Generic;
namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class ChargeBehaviour : FSMBehaviour {
        private string variableName;
        private float maxChargeMultiplier;
        private float maxChargeSeconds;

        public ChargeBehaviour(GameObject gameObject, string variableName, float maxChargeMultiplier, float maxChargeSeconds) : base(gameObject) {
          this.variableName = variableName;
          this.maxChargeMultiplier = maxChargeMultiplier;
          this.maxChargeSeconds = maxChargeSeconds;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          float charge = (float) parameters[this.variableName];
          charge += (this.maxChargeMultiplier - 1f) * Time.deltaTime / this.maxChargeSeconds;
          charge = Mathf.Clamp(charge, 1f, this.maxChargeMultiplier);
          parameters[this.variableName] = charge;
        }
      }
    }
  }
}