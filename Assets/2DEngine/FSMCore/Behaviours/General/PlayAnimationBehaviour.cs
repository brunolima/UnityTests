using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;

namespace TwoDEngine {
  namespace FSMCore {
    namespace Behaviours {
      public class PlayAnimationBehaviour : FSMBehaviour {
        public Animator animator;
        public string defaultAnimation;
        public int layer;
        public Dictionary<string, ICondition> altAnimation = new Dictionary<string, ICondition>();
        public string currentAnimation;

        public PlayAnimationBehaviour(GameObject gameObject, string defaultAnimation) : base(gameObject) {
          this.animator = this.gameObject.GetComponent<Animator>();
          this.defaultAnimation = defaultAnimation;
        }

        public PlayAnimationBehaviour WithAltAnimation(string animation, ICondition condition) {
          this.altAnimation[animation] = condition;
          return this;
        }

        public PlayAnimationBehaviour InLayer(int layer) {
          this.layer = layer;
          return this;
        }

        public override void Behave(ref Dictionary<string, object> parameters) {
          foreach (string animationState in this.altAnimation.Keys) {
            ICondition condition = this.altAnimation[animationState];
            if (parameters.ContainsKey(condition.conditionVariable) && condition.IsValid(parameters[condition.conditionVariable])) {
              if (!String.Equals(this.currentAnimation, animationState)) {
                this.Play(animationState);
              }
              return;
            }
          }
          if (!String.Equals(this.currentAnimation, this.defaultAnimation)) {
            this.Play(this.defaultAnimation);
          }
        }

        public override void Reset() {
          this.currentAnimation = null;
        }

        void Play(string animation) {
          this.currentAnimation = animation;
          this.animator.Play(animation, this.layer);
        }

      }
    }
  }
}