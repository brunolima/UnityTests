using UnityEngine;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace FSMCore {

    public class FiniteStateMachineController : MonoBehaviour {
      public FiniteStateMachine fsm = new FiniteStateMachine();


      public virtual void OnEnable(){}

      public virtual void OnDisable(){
        this.fsm.StopMachine();
      }

      void Update() {
        this.CustomUpdate();  
        this.fsm.Update();
      }

      public virtual void CustomUpdate(){}


    }
  }
}