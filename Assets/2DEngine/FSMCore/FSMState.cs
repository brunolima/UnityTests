using UnityEngine;
using System;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace FSMCore {
    public class FSMState {
      public string state;
      public List<FSMBehaviour> startBehaviours = new List<FSMBehaviour>();
      public List<FSMBehaviour> updateBehaviours = new List<FSMBehaviour>();
      public List<FSMBehaviour> exitBehaviours = new List<FSMBehaviour>();
      public bool isFinalState;
      public bool hasBehaviours;
      public FSMState(string state) {
        this.state = state;
      }

      public FSMState(string state, bool isFinalState) {
        this.state = state;
        this.isFinalState = true;
      }

      public void Reset(){
        this.startBehaviours.Clear();
        this.updateBehaviours.Clear();
        this.exitBehaviours.Clear();
      }

      public void AddStartBehaviour(FSMBehaviour behaviour) {
        this.hasBehaviours = true;
        this.startBehaviours.Add(behaviour);
      }

      public void AddUpdateBehaviour(FSMBehaviour behaviour) {
        this.hasBehaviours = true;
        this.updateBehaviours.Add(behaviour);
      }

      public void AddExitBehaviour(FSMBehaviour behaviour) {
        this.hasBehaviours = true;
        this.exitBehaviours.Add(behaviour);
      }

      public virtual void WithDefaultBehaviours(GameObject gameObject) {}

      public void OnStart(ref Dictionary<string, object> parameters, string previousState) {
        foreach (FSMBehaviour behaviour in this.startBehaviours) {
          behaviour.Reset();
          behaviour.WithStates(previousState).ConditionalBehave(ref parameters);
        }
        foreach (FSMBehaviour behaviour in this.updateBehaviours) {
          behaviour.Reset();
        }
      }

      public void Update(ref Dictionary<string, object> parameters, string previousState) {
        if (!isFinalState) {
          foreach (FSMBehaviour behaviour in this.updateBehaviours) {
            behaviour.WithStates(previousState).ConditionalBehave(ref parameters);
          }
        }
      }

      public void OnExit(ref Dictionary<string, object> parameters, string previousState, string nextState) {
        foreach (FSMBehaviour behaviour in this.exitBehaviours) {
          behaviour.WithStates(previousState, nextState).ConditionalBehave(ref parameters);
        }
      }

    }
  }
}