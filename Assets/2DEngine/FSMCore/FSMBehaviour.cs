using UnityEngine;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace FSMCore {
    public abstract class FSMBehaviour {
      public GameObject gameObject;
      public string previousState;
      public string nextState;

      public bool allowPreviousStates;
      public bool allowNextStates;

      public string[] conditionalsNextStates = new string[0];
      public string[] conditionalsPreviousStates = new string[0];

      public FSMBehaviour(GameObject gameObject) {
        this.gameObject = gameObject;
      }

      public FSMBehaviour WithStates(string previousState, string nextState = null) {
        this.previousState = previousState;
        this.nextState = nextState;
        return this;
      }

      public FSMBehaviour WithNextStateIn(params string[] allowedStates) {
        this.conditionalsNextStates = allowedStates;
        this.allowNextStates = true;
        return this;
      }

      public FSMBehaviour WithNextStateNotIn(params string[] notAllowedStates) {
        this.conditionalsNextStates = notAllowedStates;
        this.allowNextStates = false;
        return this;
      }

      public FSMBehaviour WithPreviousStateIn(params string[] allowedStates) {
        this.conditionalsPreviousStates = allowedStates;
        this.allowPreviousStates = true;
        return this;
      }

      public FSMBehaviour WithPreviousStateNotIn(params string[] notAllowedStates) {
        this.conditionalsPreviousStates = notAllowedStates;
        this.allowPreviousStates = false;
        return this;
      }


      public void ConditionalBehave(ref Dictionary<string, object> parameters) {
        if (this.conditionalsPreviousStates.Length > 0) {
          foreach (string allowedState in this.conditionalsPreviousStates) {
            if (previousState == allowedState) {
              if (this.allowPreviousStates) {
                this.Behave(ref parameters);
              }
              return;
            }
          }
          if (!this.allowPreviousStates) {
            this.Behave(ref parameters);
          }
        } else if (this.conditionalsNextStates.Length > 0) {
          foreach (string allowedState in this.conditionalsNextStates) {
            if (nextState == allowedState) {
              if (this.allowNextStates) {
                this.Behave(ref parameters);
              }
              return;
            }
          }
          if (!this.allowNextStates) {
            this.Behave(ref parameters);
          }
        } else {
          this.Behave(ref parameters);
        }
      }
      public abstract void Behave(ref Dictionary<string, object> parameters);
      public virtual void Reset() {}
    }
  }
}