using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace Inputs {
    public class PlayerInputSettings {
      private static PlayerInputSettings instance;

      public static PlayerInputSettings Instance {
        get {
          if (instance == null) {
            instance = new PlayerInputSettings();
          }
          return instance;
        }
      }

      private PlayerInputSettings() {}

      public Dictionary<int, List<InputMapping>> mappings = new Dictionary<int, List<InputMapping>>();

      private void LoadSettings() {
        //TODO: Implementar
      }

      public List<InputMapping> GetInputMapping(int playerNumber) {
        if (!this.mappings.ContainsKey(playerNumber)) {
          this.CreateInputs();
        }
        return this.mappings[playerNumber];
      }

      private void CreateInputs() {
        for (int i = 0; i < 5; i++) {
          List<InputMapping> playerMapping = new List<InputMapping>();
          if (i == 1) {
            playerMapping.Add(new InputMapping(InputActions.Horizontal, "Horizontal"));
            playerMapping.Add(new InputMapping(InputActions.Vertical, "Vertical"));
            playerMapping.Add(new InputMapping(InputActions.Jump, KeyCode.K));
            playerMapping.Add(new InputMapping(InputActions.Fire1, KeyCode.J));
            playerMapping.Add(new InputMapping(InputActions.Fire2, KeyCode.H));
          } else if (i == 2) {
            playerMapping.Add(new InputMapping(InputActions.Horizontal, "Horizontal_P2"));
            playerMapping.Add(new InputMapping(InputActions.Vertical, "Vertical_P2"));
            playerMapping.Add(new InputMapping(InputActions.Jump, KeyCode.Keypad3));
            playerMapping.Add(new InputMapping(InputActions.Fire1, KeyCode.Keypad2));
            playerMapping.Add(new InputMapping(InputActions.Fire2, KeyCode.Keypad1));
          } else if (i == 3) {
            playerMapping.Add(new InputMapping(InputActions.Horizontal, "Horizontal"));
            playerMapping.Add(new InputMapping(InputActions.Vertical, "Vertical"));
            playerMapping.Add(new InputMapping(InputActions.Jump, KeyCode.P));
            playerMapping.Add(new InputMapping(InputActions.Fire1, KeyCode.J));
            playerMapping.Add(new InputMapping(InputActions.Fire2, KeyCode.H));
          } else {
            playerMapping.Add(new InputMapping(InputActions.Horizontal, "Horizontal_P2"));
            playerMapping.Add(new InputMapping(InputActions.Vertical, "Vertical_P2"));
            playerMapping.Add(new InputMapping(InputActions.Jump, KeyCode.Keypad9));
            playerMapping.Add(new InputMapping(InputActions.Fire1, KeyCode.Keypad2));
            playerMapping.Add(new InputMapping(InputActions.Fire2, KeyCode.Keypad1));
          }
          this.mappings[i] = playerMapping;
        }
      }


    }
  }
}