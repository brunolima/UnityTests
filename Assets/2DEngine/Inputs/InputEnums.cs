﻿namespace TwoDEngine {
  namespace Inputs {
    public enum InputActions { Horizontal, Vertical, Jump, Fire1, Fire2 }
    public enum InputModes { NONE, RELEASE, HOLD, PRESS, DOUBLE_TAP }
  }
}