﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TwoDEngine {
  namespace Inputs {
    public class InputHandler : MonoBehaviour {
      public int playerNumber = 1;
      public float inputMultipleTapSpeed = 0.5f;//in seconds
      private Dictionary<InputActions, PlayerInput> inputs = new Dictionary<InputActions, PlayerInput>();

      public delegate void UserInput(Dictionary<InputActions, PlayerInput> inputs);
      public event UserInput OnUserInput;


      void Awake() {
        if (playerNumber < 1) {
          playerNumber = 1;
        }
        this.Configure();
      }

      void Update() {
        foreach (InputActions action in inputs.Keys) {
          this.inputs[action].CalculateInput(this.inputMultipleTapSpeed);
        }
        if (this.OnUserInput != null) {
          this.OnUserInput(this.inputs);
        }
      }

      private void Configure() {
        List<InputMapping> mappings = PlayerInputSettings.Instance.GetInputMapping(this.playerNumber);
        foreach (InputMapping mapping in mappings) {
          this.inputs[mapping.action] = new PlayerInput(mapping);
        }
      }

    }
  }
}