﻿using UnityEngine;
using System;

namespace TwoDEngine {
  namespace Inputs {
    public class PlayerInput {

      public InputActions Action { get { return this.mapping.action; } }
      public InputModes Mode { get { return this.mode; } }
      public float Value { get { return this.value; } }
      public float PreviousValue { get { return this.previousValue; } }

      private InputModes mode = InputModes.NONE;
      private InputMapping mapping;
      private float value = -10f;
      private float previousValue;

      private float tapSpeed;
      private float doubleTapCheckTime;
      private float doubleTapCheckValue;

      public PlayerInput(InputMapping mapping) {
        this.mapping = mapping;
      }

      public bool IsAxis() {
        return this.mapping.useAxis;
      }

      public void CalculateInput(float tapSpeed) {
        this.tapSpeed = tapSpeed;
        this.previousValue = this.value;
        this.value = 0;
        if (this.mapping.useAxis) {
          this.value = Input.GetAxis(this.mapping.axis);
        }
        this.Calculate();
      }

      private void Calculate() {
        bool up = this.Up();
        bool down = this.Down();
        bool hold = this.Hold();
        if (up && !hold) {
          this.mode = InputModes.RELEASE;
        } else if (down && this.mode != InputModes.HOLD) {
          this.mode = InputModes.PRESS;
          this.CheckDoubleTap();
          if (this.mode == InputModes.PRESS) {
            this.doubleTapCheckValue = this.value;
            this.doubleTapCheckTime = Time.time;
          }
        } else if (hold) {
          this.mode = InputModes.HOLD;
        } else {
          this.mode = InputModes.NONE;
        }
      }

      private bool Up() {
        if (this.mapping.useAxis) {
          return Input.GetButtonUp(this.mapping.axis);
        } else {
          return Input.GetKeyUp(this.mapping.input);
        }
      }

      private bool Down() {
        if (this.mapping.useAxis) {
          return Input.GetButtonDown(this.mapping.axis);
        } else {
          return Input.GetKeyDown(this.mapping.input);
        }
      }

      private bool Hold() {
        if (this.mapping.useAxis) {
          return Input.GetButton(this.mapping.axis);
        } else {
          return Input.GetKey(this.mapping.input);
        }
      }

      private void CheckDoubleTap() {
        if (this.CanDoubleTap() && this.IsDoubleTap()) {
          this.mode = InputModes.DOUBLE_TAP;
        }
      }

      bool CanDoubleTap() {
        int inputValueSign = Tools.GetValueSign(this.value);
        int previousInputValueSign = Tools.GetValueSign(this.doubleTapCheckValue);
        return previousInputValueSign == 0 || inputValueSign == 0 || (inputValueSign != 0 && inputValueSign == previousInputValueSign);
      }

      bool IsDoubleTap() {
        return Time.time - this.doubleTapCheckTime < this.tapSpeed;
      }
    }
  }
}