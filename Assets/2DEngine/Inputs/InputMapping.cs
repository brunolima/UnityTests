using UnityEngine;

namespace TwoDEngine {
  namespace Inputs {
    public class InputMapping {
      public InputActions action;
      public string axis;
      public KeyCode input;
      public bool useAxis;

      public InputMapping(InputActions action, string axis) {
        this.action = action;
        this.axis = axis;
        this.useAxis = true;
      }

      public InputMapping(InputActions action, KeyCode input) {
        this.action = action;
        this.input = input;
        this.useAxis = false;
      }
    }
  }
}