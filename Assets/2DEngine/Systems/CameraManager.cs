﻿using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;

namespace TwoDEngine {
  namespace Systems {
    public enum CameraModes { NONE, FIXED, SINGLE_SCREEN, H_SPLIT_SCREEN, V_SPLIT_SCREEN }

    public class CameraManager : Singleton<CameraManager> {
      public int playersNumber = 1;
      public SortedDictionary<int, CustomCamera> playersCameras = new SortedDictionary<int, CustomCamera>();
      public GameObject cameraTemplate;

      public CameraModes mode = CameraModes.SINGLE_SCREEN;
      public float minDistance = 1.5f;
      public float maxDistance = 5f;
      public float borderMinDistance = 0.75f;
      public float lineSize = 0.002f;

      public Vector2 defaultOffset = new Vector2(0f, 0f);

      public Vector3 fixedDirection;
      public float cameraSpeed = 8;

      public Vector2 minPosition = Vector2.zero;
      public Vector2 maxPosition = Vector2.zero;

      public bool dynamic = true;

      private Vector2 cameraMinPosition = Vector2.zero;
      private Vector2 cameraMaxPosition = Vector2.zero;

      void Start() {
        CustomCamera camera = this.InstantiateCamera(0);
        camera.SetTarget(this.gameObject);
        this.playersCameras[0] = camera;
        camera.Activate();
      }

      void SetMinPosition(Vector2 position) {
        this.cameraMinPosition = position;
        foreach (int cameraIndex in this.playersCameras.Keys) {
          GameObject target = this.playersCameras[cameraIndex].target;
          if (cameraIndex > 0 && target != null) {
            target.SendMessage("SetMinPosition", position);
          }
        }
      }

      void SetMaxPosition(Vector2 position) {
        this.cameraMaxPosition = position;
        foreach (int cameraIndex in this.playersCameras.Keys) {
          GameObject target = this.playersCameras[cameraIndex].target;
          if (cameraIndex > 0 && target != null) {
            target.SendMessage("SetMaxPosition", position);
          }
        }
      }

      public void AddPlayer(int playerNumber, GameObject playerTarget) {
        CustomCamera camera = this.InstantiateCamera(playerNumber);
        camera.SetTarget(playerTarget);
        this.playersCameras[playerNumber] = camera;
      }

      void Update() {
        if (this.mode == CameraModes.NONE) {
          this.mode = CameraModes.SINGLE_SCREEN;
        }
        switch (mode) {
        case CameraModes.FIXED:
          this.ActivateCameras(0);
          break;
        case CameraModes.SINGLE_SCREEN:
          this.ActivateCameras(0);
          break;
        case CameraModes.H_SPLIT_SCREEN:
          if (InDynamicDistance()) {
            this.ActivateCameras(0);
          } else {
            this.ActivateCameras(1, 2, 3, 4);
          }
          break;
        case CameraModes.V_SPLIT_SCREEN:
          if (InDynamicDistance()) {
            this.ActivateCameras(0);
          } else {
            this.ActivateCameras(1, 2, 3, 4);
          }
          break;
        }
      }

      bool InDynamicDistance() {
        if (!this.dynamic) {
          return false;
        }
        foreach (int i in this.playersCameras.Keys) {
          Vector2 positionI = this.playersCameras[i].target.transform.position;
          foreach (int j in this.playersCameras.Keys) {
            Vector2 positionJ = this.playersCameras[j].target.transform.position;
            if (i > 0 && j > i) {
              if (Vector2.Distance(positionI, positionJ) > this.maxDistance + this.borderMinDistance) {
                return false;
              }
            }
          }
        }
        return true;
      }

      void LateUpdate() {
        foreach (int cameraIndex in this.playersCameras.Keys) {
          this.playersCameras[cameraIndex].UpdateViewPort(this.cameraSpeed);
        }
        switch (mode) {
        case CameraModes.FIXED:
          this.transform.position = Vector3.Lerp(this.transform.position, this.transform.position + this.fixedDirection, Time.deltaTime * this.cameraSpeed);
          break;
        default:
          this.UpdatePosition();
          break;
        }
        this.UpdateSingleScreenDistance();
      }

      private void UpdatePosition() {
        if (!this.playersCameras[0].IsActive()) {
          return;
        }
        Vector3 newPosition = Vector3.zero;
        float players = 0;
        foreach (int cameraIndex in this.playersCameras.Keys) {
          GameObject target = this.playersCameras[cameraIndex].target;
          if (cameraIndex > 0 && target != null) {
            newPosition += target.transform.position;
            players += 1f;
          }
        }
        if (players > 0f) {
          newPosition = newPosition / players;
          this.transform.position = new Vector3(newPosition.x - this.playersCameras[0].cameraFollowComponent.offset.x, newPosition.y - this.playersCameras[0].cameraFollowComponent.offset.y, this.transform.position.z);
        }
      }

      private bool IsTargetOusideBounds(Vector3 targetPosition, Vector3 targetSize) {
        bool outsideX = targetPosition.x + targetSize.x / 2f < this.cameraMinPosition.x || targetPosition.x - targetSize.x / 2f > this.cameraMaxPosition.x;
        bool outsideY = targetPosition.y + targetSize.y < this.cameraMinPosition.y || targetPosition.y - targetSize.y > this.cameraMaxPosition.y;
        Debug.Log(outsideX + " " + outsideY);
        Debug.Log(targetPosition.x + " " + targetSize.x + "  " +  this.cameraMinPosition.x + "  " + this.cameraMaxPosition.x);
        Debug.Log(targetPosition.y + " " + targetSize.y + "  " +  this.cameraMinPosition.y + "  " + this.cameraMaxPosition.y);
        return outsideX || outsideY;
      }



      private void UpdateSingleScreenDistance() {
        if (!this.dynamic || !this.playersCameras[0].IsActive()) {
          return;
        }
        float distance = 0;
        foreach (int cameraIndex in this.playersCameras.Keys) {
          CustomCamera camera = this.playersCameras[cameraIndex];
          if (camera.target != null) {
            float targetDistance = Vector2.Distance(this.transform.position, camera.target.transform.position);
            if (targetDistance > distance) {
              distance = targetDistance;
            }
          }
        }
        this.playersCameras[0].UpdateDistance(Mathf.Clamp(distance + this.borderMinDistance, this.minDistance, this.maxDistance));
      }

      private void ActivateCameras(params int[] indexes) {
        int activeCameras = 0;
        foreach (int cameraIndex in this.playersCameras.Keys) {
          CustomCamera camera = this.playersCameras[cameraIndex];
          camera.UpdateDistance(this.minDistance);
          if (ArrayContains(indexes, cameraIndex) && camera.target != null) {
            camera.Activate();
            activeCameras++;
          } else {
            camera.Deactivate();
            camera.SetViewPort(new Rect(0, 0, 1, 1));
          }
        }
        if (this.mode == CameraModes.H_SPLIT_SCREEN || this.mode == CameraModes.V_SPLIT_SCREEN) {
          int viewPortNumber = 0;
          foreach (int cameraIndex in this.playersCameras.Keys) {
            CustomCamera camera = this.playersCameras[cameraIndex];
            if (camera.IsActive()) {
              camera.SetViewPort(GetViewPort(viewPortNumber, activeCameras));
              viewPortNumber++;
            }
          }
        }
        if (activeCameras == 0) {
          this.playersCameras[0].Activate();
        }
      }

      private Rect GetViewPort(int viewPortNumber, int totalActiveCameras) {
        if (totalActiveCameras == 1) {
          return new Rect(0, 0, 1, 1);
        }
        float size = 1f / 2f;
        Rect rect = new Rect();
        if (totalActiveCameras > 2) {
          rect.x = viewPortNumber % 2 == 1 ? size : 0f;
          rect.y = viewPortNumber < 2 ? size : 0f;
          rect.width = size - this.lineSize;
          rect.height = size - this.lineSize;
        } else {
          if (this.mode == CameraModes.H_SPLIT_SCREEN) {
            rect.x = 0;
            rect.y = viewPortNumber % 2 == 0 ? size : 0f;
            rect.width = 1.0f;
            rect.height = size - this.lineSize;
          } else if (this.mode == CameraModes.V_SPLIT_SCREEN) {
            rect.x = viewPortNumber % 2 == 1 ? size : 0f;
            rect.y = 0;
            rect.width = size - this.lineSize;
            rect.height = 1.0f;
          }
        }
        return rect;
      }

      private bool ArrayContains(int[] array, int value) {
        foreach (int v in array) {
          if (v == value) {
            return true;
          }
        }
        return false;
      }

      private CustomCamera InstantiateCamera(int index) {
        GameObject instance = Instantiate(this.cameraTemplate);
        instance.SetActive(false);
        instance.name = "Camera" + index;
        instance.transform.position = this.transform.position;
        return new CustomCamera(instance, this.defaultOffset, this.minDistance, this.minPosition, this.maxPosition);
      }

    }

    public class CustomCamera {
      public GameObject camera;
      public GameObject target;
      public Renderer targetRenderer;
      public CameraFollow cameraFollowComponent;
      public Camera cameraComponent;
      public Rect viewPort = new Rect(0, 0, 1, 1);

      public CustomCamera(GameObject camera, Vector2 defaultOffset, float defaultDistance, Vector2 minPosition, Vector2 maxPosition) {
        this.camera = camera;
        this.cameraComponent = this.camera.GetComponent<Camera>();
        this.cameraComponent.enabled = true;
        this.cameraFollowComponent = this.camera.GetComponent<CameraFollow>();
        this.UpdateDistance(defaultDistance);
        this.cameraFollowComponent.offset = defaultOffset;
        this.cameraFollowComponent.minPosition = minPosition;
        this.cameraFollowComponent.maxPosition = maxPosition;
      }

      public void UpdateDistance(float distance) {
        this.cameraComponent.orthographicSize = distance;
        this.cameraFollowComponent.cameraHeight = distance;
        this.cameraFollowComponent.cameraWidth = distance * this.cameraComponent.aspect - (this.cameraComponent.aspect * this.viewPort.height);
      }

      public void UpdateViewPort(float speed = 1) {
        if (this.cameraComponent != null && this.viewPort != this.cameraComponent.rect) {
          this.cameraComponent.rect = Tools.RectLerp(this.cameraComponent.rect, this.viewPort, Time.deltaTime * speed);
        }
      }

      public void SetTarget(GameObject target) {
        this.target = target;
        this.cameraFollowComponent.target = this.target;
        this.targetRenderer = target.GetComponent<Renderer>();
      }

      public Vector3 GetTargetSize() {
        if (this.targetRenderer == null) {
          return Vector3.zero;
        }
        return this.targetRenderer.bounds.size;
      }

      public void SetViewPort(Rect viewPort) {
        this.viewPort = viewPort;
      }

      public bool IsActive() {
        return this.camera.activeSelf;
      }

      public void Activate() {
        this.camera.SetActive(true);
      }

      public void Deactivate() {
        this.camera.SetActive(false);
      }


    }
  }
}
