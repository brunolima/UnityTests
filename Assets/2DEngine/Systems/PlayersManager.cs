﻿using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;

namespace TwoDEngine {
  namespace Systems {
    public class PlayersManager : MonoBehaviour {
      public int playersNumber = 1;
      public GameObject character;

      void Start() {
        this.playersNumber = (int) Mathf.Clamp((float)this.playersNumber, 1F, 4F);
        for (int i = 0; i < this.playersNumber; i++) {
          GameObject player = new GameObject("Player_" + (i + 1));
          player.SetActive(false);
          player.transform.localScale = new Vector3(2F, 2F, 1F);
          InputHandler inputHandler = player.AddComponent<InputHandler>();
          inputHandler.playerNumber = i + 1;
          PlayerSelection selection = player.AddComponent<PlayerSelection>();
          selection.character = this.character;
          player.SetActive(true);
        }
      }

    }
  }
}
