﻿using UnityEngine;

namespace TwoDEngine {
  namespace Systems {
    public class CameraFollow : MonoBehaviour {

      public GameObject target;
      public Vector2 offset = Vector2.zero;
      public Vector2 minPosition = Vector2.zero;
      public Vector2 maxPosition = Vector2.zero;
      [HideInInspector]
      public float cameraHeight;
      [HideInInspector]
      public float cameraWidth;

      void LateUpdate() {
        if (this.target) {
          Vector2 newPosition = (Vector2)this.target.transform.position + this.offset;
          this.transform.position = new Vector3(Mathf.Clamp(newPosition.x, this.minPosition.x + cameraWidth, this.maxPosition.x - cameraWidth), Mathf.Clamp(newPosition.y, this.minPosition.y + cameraHeight, this.maxPosition.y - cameraHeight), -1);
          this.target.SendMessage("SetMinPosition", new Vector2(this.transform.position.x - cameraWidth, this.transform.position.y - cameraHeight));
          this.target.SendMessage("SetMaxPosition", new Vector2(this.transform.position.x + cameraWidth, this.transform.position.y + cameraHeight));
        }
      }
    }
  }
}