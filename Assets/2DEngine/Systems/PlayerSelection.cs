using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;

namespace TwoDEngine {
  namespace Systems {
    public class PlayerSelection : MonoBehaviour {
      [HideInInspector]
      public GameObject character;
      private InputHandler inputHandler;
      private bool created = false;

      void Awake() {
        if (this.inputHandler == null) {
          this.inputHandler = this.GetComponent<InputHandler>();
        }
      }

      void OnEnable() {
        this.inputHandler.OnUserInput += this.OnUserInput;
      }

      void OnDisable() {
        this.inputHandler.OnUserInput -= this.OnUserInput;
      }

      void OnUserInput(Dictionary<InputActions, PlayerInput> inputs) {
        if (!this.created && inputs[InputActions.Jump].Mode == InputModes.RELEASE) {
          this.created = true;
          character.SetActive(false);
          GameObject playerCharacter = Instantiate(character);
          playerCharacter.name += "_" + inputHandler.playerNumber; 
          playerCharacter.transform.SetParent(this.transform);
          playerCharacter.transform.localScale = new Vector3(1F, 1F, 1F);
          CameraManager.Instance.AddPlayer(inputHandler.playerNumber, playerCharacter);
          playerCharacter.SetActive(true);
        }
      }
    }

  }
}