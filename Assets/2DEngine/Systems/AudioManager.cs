﻿using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;

namespace TwoDEngine {
  namespace Systems {
    public class AudioManager : Singleton<AudioManager> {
      //TODO: Definir sons padrão
      [Header("Volume")]
      [Range(0f, 1f)]
      public float musicVolume = 1f;
      [Range(0f, 1f)]
      public float sfxVolume = 1f;
      [Range(0f, 1f)]
      public float voiceVolume = 1f;

      void Start() {
      }

    }
  }
}
