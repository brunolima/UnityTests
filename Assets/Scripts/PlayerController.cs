using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MyTests {
  // public enum FacingDirections {LEFT=-1, RIGHT=1}
  // public enum MovementModes {UPDATE_POSITION, APPLY_FORCE};

  public class PlayerController : MonoBehaviour {

    // public LayerMask whatIsObstacle;
    // public Collider2D bodyCollider;
    // public MovementModes movementMode;

    // public LayerMask whatIsGround;
    // public Transform groundCheck;
    // public LayerMask whatIsCeiling;
    // public Transform ceilingCheck;

    // [HideInInspector]
    // public FacingDirections facingDirection = FacingDirections.RIGHT;
    // [HideInInspector]
    // public float originalLinearDrag;
    // [HideInInspector]
    // public float originalGravityScale;

    // [HideInInspector]
    // public bool hitObstacle = false;
    // [HideInInspector]
    // public float hitRadius = 0.2f;
    // [HideInInspector]
    // public bool hitGround = false;
    // [HideInInspector]
    // public bool hitCeiling = false;

    // [HideInInspector]
    // public bool verticalMovementEnabled = false;

    // private float movementSpeed;
    // private float movementForce;
    // private float speedMultiplier = 1f;

    // private Rigidbody2D rigidBody;

    // private List<Collider2D> nonTriggerColliders = new List<Collider2D>();

    // void Awake() {
    //   this.rigidBody = this.GetComponent<Rigidbody2D>();
    //   this.originalLinearDrag = this.rigidBody.drag;
    //   this.originalGravityScale = this.rigidBody.gravityScale;

    //   foreach(Collider2D c in this.GetComponents<Collider2D> ()) {
    //     if(c.isTrigger == false){
    //       this.nonTriggerColliders.Add(c);
    //     }
    //   }
    // }

    // void OnCollisionEnter2D(Collision2D collision) {
    //   if(Tools.CollidedWithMask(collision, this.bodyCollider, whatIsObstacle)){
    //     this.hitObstacle = true;
    //   }
    // }

    // void OnCollisionExit2D(Collision2D collision) {
    //   if(Tools.IsInLayerMask(collision.gameObject, whatIsObstacle)){
    //     this.hitObstacle = false;
    //   }
    // }

    // void OnTriggerExit2D(Collider2D collider) {
    //   if(Tools.IsInLayerMask(collider.gameObject, whatIsObstacle)){
    //     this.hitObstacle = false;
    //   }
    // }

    // void FixedUpdate(){
    //   this.hitGround = Physics2D.OverlapCircle (this.groundCheck.position, this.hitRadius, this.whatIsGround);
    //   this.hitCeiling = Physics2D.OverlapCircle (this.ceilingCheck.position, this.hitRadius, this.whatIsCeiling);
    // }

    // public void MoveH(float inputValue, float speed, float moveForce = 0){
    //   this.movementSpeed = speed;
    //   this.movementForce = moveForce;
    //   this.MoveH(inputValue);
    // }

    // public void MoveHAndFlip(float inputValue, float speed, float moveForce = 0){
    //   this.MoveH(inputValue, speed, moveForce);
    //   if ((inputValue > 0 && this.facingDirection == FacingDirections.LEFT) || (inputValue < 0 && this.facingDirection == FacingDirections.RIGHT)) {
    //     this.Flip();
    //   }
    // }

    // public void MoveHAndFlip(float inputValue){
    //   this.MoveH(inputValue);
    //   if ((inputValue > 0 && this.facingDirection == FacingDirections.LEFT) || (inputValue < 0 && this.facingDirection == FacingDirections.RIGHT)) {
    //     this.Flip();
    //   }
    // }

    // public void MoveH(float inputValue){
    //   if(this.hitObstacle && this.IsMovingTo(this.facingDirection, inputValue)){
    //     return;
    //   }
    //   switch (this.movementMode) {
    //   case MovementModes.APPLY_FORCE:
    //     if (inputValue * this.rigidBody.velocity.x < this.movementSpeed) {
    //       this.rigidBody.AddForce (Vector2.right * inputValue * this.movementForce);
    //     }
    //     if (Mathf.Abs (this.rigidBody.velocity.x) > this.movementSpeed) {
    //       this.rigidBody.velocity = new Vector2 (Mathf.Sign (this.rigidBody.velocity.x) * this.movementSpeed * this.speedMultiplier, this.rigidBody.velocity.y);
    //     }
    //     break;
    //   case MovementModes.UPDATE_POSITION:
    //     Vector3 currentPosition = this.transform.position;
    //     Vector3 target = Vector3.right * inputValue * this.movementSpeed * this.speedMultiplier + currentPosition;
    //     this.transform.position = Vector3.Lerp( currentPosition, target, Time.deltaTime );
    //     this.rigidBody.velocity = new Vector2 (0f, this.rigidBody.velocity.y);
    //     break;
    //   }
    // }

    // public bool IsMovingTo(FacingDirections facingDirection, float inputValue){
    //   switch(facingDirection){
    //   case FacingDirections.LEFT:
    //     return inputValue < 0f;
    //   case FacingDirections.RIGHT:
    //     return inputValue > 0f;
    //   }
    //   return false;
    // }

    // public void Flip(){
    //   this.facingDirection = (FacingDirections) ((int)this.facingDirection * -1);
    //   Vector3 localScale = this.transform.localScale;
    //   localScale.x *= -1;
    //   this.transform.localScale = localScale;
    // }

    // public bool StoppedMovingH(float value){
    //   if(this.movementMode == MovementModes.UPDATE_POSITION){
    //     return true;
    //   } else if(this.movementMode == MovementModes.APPLY_FORCE && Mathf.Abs(this.rigidBody.velocity.x) <= 1f){
    //     return true;
    //   }
    //   return false;
    // }

    // public void MoveV(float inputValue, float speed, float moveForce = 0){
    //   this.movementSpeed = speed;
    //   this.movementForce = moveForce;
    //   this.MoveV(inputValue);
    // }

    // public void MoveV(float inputValue){
    //   if((this.hitGround && inputValue < 0) || (this.hitCeiling && inputValue > 0)){
    //     return;
    //   }
    //   switch (this.movementMode) {
    //   case MovementModes.APPLY_FORCE:
    //     if (inputValue * this.rigidBody.velocity.y < this.movementSpeed) {
    //       this.rigidBody.AddForce (Vector2.up * inputValue * this.movementForce);
    //     }
    //     if (Mathf.Abs (this.rigidBody.velocity.y) > this.movementSpeed) {
    //       this.rigidBody.velocity = new Vector2 (this.rigidBody.velocity.x, Mathf.Sign (this.rigidBody.velocity.y) * this.movementSpeed * this.speedMultiplier);
    //     }
    //     break;
    //   case MovementModes.UPDATE_POSITION:
    //     Vector3 currentPosition = this.transform.position;
    //     Vector3 target = Vector3.up * inputValue * this.movementSpeed * this.speedMultiplier + currentPosition;
    //     this.transform.position = Vector3.Lerp( currentPosition, target, Time.deltaTime );
    //     this.rigidBody.velocity = new Vector2 (this.rigidBody.velocity.y, 0f);
    //     break;
    //   }
    // }

    // public bool StoppedMovingV(float value){
    //   if(this.movementMode == MovementModes.UPDATE_POSITION){
    //     return true;
    //   } else if(this.movementMode == MovementModes.APPLY_FORCE && Mathf.Abs(this.rigidBody.velocity.y) <= 1f){
    //     return true;
    //   }
    //   return false;
    // }

    // public void UpdateNonTriggerColliders(bool asTrigger){
    //   foreach(Collider2D c in this.nonTriggerColliders) {
    //    c.isTrigger = asTrigger;
    //   }
    // }

    // public void IgnoreGravity(){
    //   this.rigidBody.gravityScale = 0;
    // }

    // public void RestoreGravity(){
    //   this.rigidBody.gravityScale = this.originalGravityScale;
    // }

    // public void SetLinearDrag(float linearDrag){
    //   this.rigidBody.drag = linearDrag;
    // }

    // public void RestoreLinearDrag(){
    //   this.rigidBody.drag = this.originalLinearDrag;
    // }

    // public void SetSpeedMultiplier(float multiplier){
    //   this.speedMultiplier = multiplier;
    // }

    // public void ResetSpeedMultiplier(){
    //   this.speedMultiplier = 1f;
    // }

    // public void ResetVVelocity(bool negative=true){
    //   if(!negative || this.rigidBody.velocity.y < 0f){
    //     Vector2 velocity = this.rigidBody.velocity;
    //     velocity.y = 0f;
    //     this.rigidBody.velocity = velocity;
    //   }
    // }

  }
}