using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;

public abstract class IAction : MonoBehaviour {
  [HideInInspector]
  public FiniteStateMachineController controller;
  [HideInInspector]
  public GameObject targetObject;

  public AudioSource soundStart;
  public AudioSource soundUpdate;
  public AudioSource soundExit;

  public virtual void Awake() {
    if (this.controller == null) {
      this.controller = this.GetComponentInParent<FiniteStateMachineController>();
    }
    if (this.targetObject == null) {
      this.targetObject = this.controller.gameObject;
    }
  }

  public abstract void RegisterStates();
  public abstract void RemoveStates();
  public virtual void AddBehaviours() {}
  public virtual void AddTransitions() {}

  public virtual void OnTransition(string toState) {
  }

  public virtual void PlaySound(FSMState state) {
    if (this.soundStart != null) {
      state.AddStartBehaviour(new PlaySoundBehaviour(this.targetObject, this.soundStart).Force());
    }
    if (this.soundUpdate != null) {
      state.AddUpdateBehaviour(new PlaySoundBehaviour(this.targetObject, this.soundUpdate));
      state.AddExitBehaviour(new StopSoundBehaviour(this.targetObject, this.soundUpdate));
    }
    if (this.soundExit != null) {
      state.AddExitBehaviour(new PlaySoundBehaviour(this.targetObject, this.soundExit).Force());
    }
  }

  public virtual void OnEnable() {
    this.RegisterStates();
    this.AddBehaviours();
    this.AddTransitions();
  }

  public virtual void OnDisable() {
    this.RemoveStates();
  }

}
