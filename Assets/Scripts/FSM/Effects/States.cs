using System.Collections.Generic;
namespace Effects {
  public class EffectStates {

    public static EffectState CREATE = new EffectState("Create");
    public static EffectState DESTROY = new EffectState("Destroy");

    public static List<string> All() {
      List<string> allStates = new List<string>();
      allStates.AddRange(CREATE.possibleNames);
      allStates.AddRange(DESTROY.possibleNames);
      return allStates;
    }
  }

  public enum Suffixes {
    None
  }

  public class EffectState {
    public string name;
    public List<string> possibleNames = new List<string>();
    public EffectState(string name) {
      this.name = name;
      this.possibleNames.Add(name);
    }

    public EffectState RegisterSuffixes(params Suffixes[] suffixes) {
      this.possibleNames.Add(this.ToS(suffixes));
      return this;
    }

    public string ToS(params Suffixes[] suffixes) {
      string allSuffixes = "";
      foreach (Suffixes suffix in suffixes) {
        if (suffix != Suffixes.None) {
          allSuffixes += "." + suffix;
        }
      }
      return this.name + allSuffixes;
    }
  }

}