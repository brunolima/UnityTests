using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;

namespace Effects {
  namespace Creation {
    public class CreateAction : IAction {

      private FSMState state = new FSMState(EffectStates.CREATE.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.StartMachine(this.state);
      }

      public override void AddBehaviours() {
        this.PlaySound(this.state);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }

    }
  }
}