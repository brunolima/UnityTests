using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;

namespace Effects {
  namespace Creation {
    public class TimeDestroyAction : IAction {

      public float timeToLive = 1f;

      private FSMState state = new FSMState(EffectStates.DESTROY.ToS(), true);

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.AddExitBehaviour(new DestroyBehaviour(this.targetObject));
      }

      public override void AddTransitions() {
        FSMTransition transition = null;
        transition = new FSMTransition(EffectStates.CREATE.ToS(), EffectStates.DESTROY.ToS());
        transition.AddCondition(new TimeCondition(this.timeToLive));
        this.controller.fsm.RegisterTransition(transition);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }

    }
  }
}