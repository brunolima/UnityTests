using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace Defense {

      public class BlockKnockbackAction : IAction {

        private FSMState state = new ActionState(CharStates.BLOCK.ToS(Suffixes.Knockback));

        public float stepForce = 40f;

        [HideInInspector]
        public DefenseController defenseController;

        public override void Awake() {
          base.Awake();
          if (this.defenseController == null) {
            this.defenseController = this.transform.parent.GetComponentInParent<DefenseController>();
          }
        }

        public override void OnEnable() {
          base.OnEnable();
          this.defenseController.OnBlockAttack += this.OnBlockAttack;
        }

        public override void OnDisable() {
          base.OnDisable();
          this.defenseController.OnBlockAttack -= this.OnBlockAttack;
        }

        public void OnBlockAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce) {
          // if (effect == AttackEffects.HIT) {
          // this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT.ToS(), effect);
          // this.controller.fsm.UpdateVariable(CharVars.NEW_FACING_DIRECTION.ToS(), attackDirection);
          // this.defenseController.SendSuccessMessage(attacker, effect);
          // }
        }

        public override void RegisterStates() {
          this.state.Reset();
          this.controller.fsm.RegisterState(this.state);
        }

        public override void AddBehaviours() {
          this.state.WithDefaultBehaviours(this.targetObject);
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.DAMAGE_EFFECT.ToS(), AttackEffects.NONE));
          this.state.AddStartBehaviour(
            new ApplyForceBehaviour(this.targetObject)
            .WithDirectionVariable(CharVars.DAMAGE_EFFECT_DIRECTION.ToS())
            .WithForce(this.stepForce)
            .WithRelativeXDirection(CharVars.NEW_FACING_DIRECTION.ToS(), true)
          );
          this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
          this.PlaySound(this.state);
        }

        public override void AddTransitions() {
          FSMTransition transition = null;
          transition = new FSMTransition(CharStates.BLOCK.ToS(), CharStates.BLOCK.ToS(Suffixes.Knockback));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.DAMAGE_EFFECT, new IntCondition(CharVars.DAMAGE_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.BLOCK.ToS(Suffixes.Knockback), CharStates.BLOCK.ToS());
          transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.BLOCK.ToS()));
          this.controller.fsm.RegisterTransition(transition);
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.state);
        }
      }
    }
  }
}