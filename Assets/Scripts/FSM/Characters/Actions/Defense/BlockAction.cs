using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace Defense {

      public class BlockAction : IAction {
        public InputActions input = InputActions.Fire2;

        private FSMState blockState = new ActionState(CharStates.BLOCK.ToS());
        private FSMState releaseState = new ActionState(CharStates.BLOCK.ToS(Suffixes.Release));

        public override void RegisterStates() {
          // this.controller.fsm.UpdateVariable(CharVars.BLOCK_RESISTANCE.ToS(), this.blockResistance);
          this.blockState.Reset();
          this.releaseState.Reset();
          this.controller.fsm.RegisterState(this.blockState);
          this.controller.fsm.RegisterState(this.releaseState);
        }

        public override void AddBehaviours() {
          this.blockState.WithDefaultBehaviours(this.targetObject);
          this.blockState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.blockState.state));
          this.blockState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_BLOCKING.ToS(), true));
          this.blockState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
          this.blockState.AddExitBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.DAMAGE_EFFECT.ToS(), AttackEffects.NONE));
          this.PlaySound(this.blockState);
          this.releaseState.WithDefaultBehaviours(this.targetObject);
          this.releaseState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_BLOCKING.ToS(), false));
          this.releaseState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.releaseState.state));
          this.releaseState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
          this.PlaySound(this.releaseState);
        }

        public override void AddTransitions() {
          this.AddTransitionsTo(CharStates.BLOCK.ToS(), CharStates.IDLE.ToS(), CharStates.MOVE.ToS(), CharStates.FLY.ToS());
          //// RELEASE
          FSMTransition transition = null;
          transition = new FSMTransition(CharStates.BLOCK.ToS(), CharStates.BLOCK.ToS(Suffixes.Release));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.BLOCK.ToS(Suffixes.Release), CharStates.FLY.ToS());
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, true))
            .Build()
          );
          transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.BLOCK.ToS(Suffixes.Release)));
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.BLOCK.ToS(Suffixes.Release), CharStates.IDLE.ToS());
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, false))
            .Build()
          );
          transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.BLOCK.ToS(Suffixes.Release)));
          this.controller.fsm.RegisterTransition(transition);
        }

        void AddTransitionsTo(string toState, params string[] fromStates) {
          FSMTransition transition = null;
          foreach (string fromState in fromStates) {
            transition = new FSMTransition(fromState, toState);
            transition.AddConditions(
              new VarConditionsBuilder()
              .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
              .WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.HOLD))
              .Build()
            );
            this.controller.fsm.RegisterTransition(transition);
          }
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.blockState);
          this.controller.fsm.RemoveState(this.releaseState);
        }
      }
    }
  }
}