using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace GetHit {

      public class FallAction : IAction {

        private FSMState state = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Fall));

        public override void RegisterStates() {
          this.state.Reset();
          this.controller.fsm.RegisterState(this.state);
        }

        public override void AddBehaviours() {
          this.state.WithDefaultBehaviours(this.targetObject);
          this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
          this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true));
          this.PlaySound(this.state);
        }

        public override void AddTransitions() {
          FSMTransition transition = null;
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Fall), CharStates.GET_HIT.ToS(Suffixes.Down));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.state);
        }

      }
    }
  }
}