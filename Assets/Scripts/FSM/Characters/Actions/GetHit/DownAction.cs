using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace GetHit {

      public class DownAction : IAction {

        public float downDuration = 2f;
        public float getUpForce = 100f;

        private FSMState downState = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Down));
        private FSMState getUpState = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Up));

        public override void RegisterStates() {
          this.downState.Reset();
          this.getUpState.Reset();
          this.controller.fsm.RegisterState(this.downState);
          this.controller.fsm.RegisterState(this.getUpState);
        }

        public override void AddBehaviours() {
          // // DOWN
          //
          this.downState.WithDefaultBehaviours(this.targetObject);
          this.downState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, CharStates.GET_HIT.ToS(Suffixes.Down)));
          this.downState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IGNORE_DRAG.ToS(), false));
          this.downState.AddStartBehaviour(new StopMovementBehaviour(this.targetObject).WithYAxis());
          this.downState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true));
          this.PlaySound(this.downState);
          // // UP
          //
          this.getUpState.WithDefaultBehaviours(this.targetObject);
          this.getUpState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Up)));
          this.getUpState.AddStartBehaviour(new ApplyForceBehaviour(this.targetObject).WithDirection(Vector2.up).WithForce(this.getUpForce));
          this.PlaySound(this.getUpState);
        }

        public override void AddTransitions() {
          FSMTransition transition = null;
          // // DOWN
          //
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Down), CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Up));
          transition.AddCondition(new TimeCondition(this.downDuration));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          // // GET UP
          //
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Up), CharStates.CROUCH.ToS());
          transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Up)));
          this.controller.fsm.RegisterTransition(transition);
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.downState);
          this.controller.fsm.RemoveState(this.getUpState);
        }

      }
    }
  }
}