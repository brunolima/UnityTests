using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace GetHit {

      public class HitAction : IAction {

        [Header("Hit")]
        public float hitForce = 50f;
        public float hitDuration = 0.3f;

        private FSMState state = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Hit));

        [HideInInspector]
        public DefenseController defenseController;

        public override void Awake() {
          base.Awake();
          if (this.defenseController == null) {
            this.defenseController = this.transform.parent.GetComponentInParent<DefenseController>();
          }
        }

        public override void OnEnable() {
          base.OnEnable();
          this.defenseController.OnReceiveAttack += this.OnReceiveAttack;
        }

        public override void OnDisable() {
          base.OnDisable();
          this.defenseController.OnReceiveAttack -= this.OnReceiveAttack;
        }

        public void OnReceiveAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce, bool blocked) {
          if (effect == AttackEffects.HIT) {
            this.controller.fsm.UpdateVariable(CharVars.NEW_FACING_DIRECTION.ToS(), attackDirection);
            this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT.ToS(), effect);
            this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT_DIRECTION.ToS(), effectDirection);
            this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT_FORCE.ToS(), this.hitForce);
            if (blocked) {
              this.defenseController.SendBlockAttack(damage, attacker, attackDirection, effect, power, effectDirection, effectForce);
            } else {
              this.controller.fsm.UpdateVariable(CharVars.IS_BLOCKING.ToS(), false);
              this.defenseController.SendSuccessMessage(attacker, effect);
            }
          }
        }

        public override void RegisterStates() {
          this.state.Reset();
          this.controller.fsm.RegisterState(this.state);
        }

        public override void AddBehaviours() {
          this.state.WithDefaultBehaviours(this.targetObject);
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.DAMAGE_EFFECT.ToS(), AttackEffects.NONE));
          this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
          this.state.AddStartBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, false).WithForcedDirection(CharVars.NEW_FACING_DIRECTION.ToS()));
          this.state.AddStartBehaviour(new StopMovementBehaviour(this.targetObject).WithXAxis().WithYAxis());
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IGNORE_DRAG.ToS(), false));
          this.state.AddStartBehaviour(
            new ApplyForceBehaviour(this.targetObject)
            .WithDirectionVariable(CharVars.DAMAGE_EFFECT_DIRECTION.ToS())
            .WithForceVariable(CharVars.DAMAGE_EFFECT_FORCE.ToS())
            .WithRelativeXDirection(CharVars.NEW_FACING_DIRECTION.ToS(), true)
          );
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_BLOCKING.ToS(), false));
          this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true));
          this.PlaySound(this.state);
        }

        public override void AddTransitions() {
          FSMTransition transition = null;
          // // HIT
          //
          foreach (string startingState in CharStates.All()) {
            transition = new FSMTransition(startingState, CharStates.GET_HIT.ToS(Suffixes.Hit));
            transition.AddConditions(
              new VarConditionsBuilder()
              .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
              .WithVar(CharVars.DAMAGE_EFFECT, new IntCondition(CharVars.DAMAGE_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.HIT))
              .WithVar(CharVars.IS_BLOCKING, new BoolCondition(CharVars.IS_BLOCKING.ToS(), Operators.EQUAL, false))
              .Build()
            );
            this.controller.fsm.RegisterTransition(transition);
          }
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Hit), CharStates.IDLE.ToS());
          transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.GET_HIT.ToS(Suffixes.Hit)));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, false))
            .Build()
          );
          transition.AddCondition(new TimeCondition(this.hitDuration));
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Hit), CharStates.FLY.ToS());
          transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.GET_HIT.ToS(Suffixes.Hit)));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, true))
            .Build()
          );
          transition.AddCondition(new TimeCondition(this.hitDuration));
          this.controller.fsm.RegisterTransition(transition);
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.state);
        }

      }
    }
  }
}