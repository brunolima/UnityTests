using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace GetHit {

      public class KnockbackRecoverAction : IAction {

        public int knockbackRecoverCount = 10;
        public float knockbackRecoverDuration = 1f;

        public float downRecoverDuration = 0.5f;

        private FSMState airRecoverState = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Knockback, Suffixes.Recover));
        private FSMState groundRecoverState = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Recover));

        public override void RegisterStates() {
          this.airRecoverState.Reset();
          this.groundRecoverState.Reset();
          this.controller.fsm.RegisterState(this.airRecoverState);
          this.controller.fsm.RegisterState(this.groundRecoverState);
        }

        public override void AddBehaviours() {
          // // KNOCKBACK RECOVER
          //
          this.airRecoverState.WithDefaultBehaviours(this.targetObject);
          this.airRecoverState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.airRecoverState.state));
          this.airRecoverState.AddStartBehaviour(new IgnoreGravityBehaviour(this.targetObject));
          this.airRecoverState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IGNORE_DRAG.ToS(), false));
          this.airRecoverState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), true));
          this.airRecoverState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true));
          this.airRecoverState.AddExitBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), false));
          this.airRecoverState.AddExitBehaviour(new StopMovementBehaviour(this.targetObject).WithXAxis().WithYAxis());
          this.PlaySound(this.airRecoverState);
          // // DOWN RECOVER
          //
          this.groundRecoverState.WithDefaultBehaviours(this.targetObject);
          this.groundRecoverState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.groundRecoverState.state));
          this.groundRecoverState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IGNORE_DRAG.ToS(), false));
          this.groundRecoverState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), true));
          this.groundRecoverState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true));
          this.groundRecoverState.AddExitBehaviour(new RestoreGravityBehaviour(this.targetObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()));
          this.groundRecoverState.AddExitBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), false));
          this.groundRecoverState.AddExitBehaviour(new StopMovementBehaviour(this.targetObject).WithXAxis().WithYAxis());
          this.PlaySound(this.groundRecoverState);
        }

        public override void AddTransitions() {
          FSMTransition transition = null;
          // // KNOCKBACK RECOVER
          //
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Knockback, Suffixes.Recover));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, false))
            .WithVar(CharVars.GENERIC_COUNTER, new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.EQUAL, this.knockbackRecoverCount))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback, Suffixes.Recover), CharStates.FLY.ToS());
          transition.AddCondition(new TimeCondition(this.knockbackRecoverDuration));
          this.controller.fsm.RegisterTransition(transition);
          // // DOWN RECOVER
          //
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Recover));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
            .WithVar(CharVars.GENERIC_COUNTER, new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.EQUAL, this.knockbackRecoverCount))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Down, Suffixes.Recover), CharStates.CROUCH.ToS());
          transition.AddCondition(new TimeCondition(this.downRecoverDuration));
          this.controller.fsm.RegisterTransition(transition);
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.airRecoverState);
          this.controller.fsm.RemoveState(this.groundRecoverState);
        }

      }
    }
  }
}