using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace GetHit {

      public class KnockbackAction : IAction {

        [Header("Knockback")]
        public float knockbackResistance = 1f;
        public float knockbackDuration = 2.5f;

        private FSMState state = new ActionState(CharStates.GET_HIT.ToS(Suffixes.Knockback));

        [HideInInspector]
        public DefenseController defenseController;

        public override void Awake() {
          base.Awake();
          if (this.defenseController == null) {
            this.defenseController = this.transform.parent.GetComponentInParent<DefenseController>();
          }
        }

        public override void OnEnable() {
          base.OnEnable();
          this.defenseController.OnReceiveAttack += this.OnReceiveAttack;
        }

        public override void OnDisable() {
          base.OnDisable();
          this.defenseController.OnReceiveAttack -= this.OnReceiveAttack;
        }

        public void OnReceiveAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce, bool blocked) {
          if (effect == AttackEffects.KNOCKBACK) {
            if (power - this.knockbackResistance > 0) {
              this.controller.fsm.UpdateVariable(CharVars.NEW_FACING_DIRECTION.ToS(), attackDirection);
              this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT.ToS(), effect);
              this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT_DIRECTION.ToS(), effectDirection);
              this.controller.fsm.UpdateVariable(CharVars.DAMAGE_EFFECT_FORCE.ToS(), effectForce);
              if (blocked) {
                this.defenseController.SendBlockAttack(damage, attacker, attackDirection, effect, power, effectDirection, effectForce);
              } else {
                this.controller.fsm.UpdateVariable(CharVars.IS_BLOCKING.ToS(), false);
                this.defenseController.SendSuccessMessage(attacker, effect);
              }
            } else {
              this.defenseController.SendReceiveAttack(damage, attacker, attackDirection, AttackEffects.HIT, power, effectDirection, effectForce, blocked);
            }
          }
        }

        public override void RegisterStates() {
          this.state.Reset();
          this.controller.fsm.RegisterState(this.state);
        }

        public override void AddBehaviours() {
          // // KNOCKBACK
          //
          this.state.WithDefaultBehaviours(this.targetObject);
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.DAMAGE_EFFECT.ToS(), AttackEffects.NONE));
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IGNORE_DRAG.ToS(), true));
          this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, CharStates.GET_HIT.ToS(Suffixes.Knockback)));
          this.state.AddStartBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, false).WithForcedDirection(CharVars.NEW_FACING_DIRECTION.ToS()));
          this.state.AddStartBehaviour(new IgnoreGravityBehaviour(this.targetObject));
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_FLYING.ToS(), false));
          this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_BLOCKING.ToS(), false));
          this.state.AddStartBehaviour(new StopMovementBehaviour(this.targetObject).WithXAxis().WithYAxis());
          this.state.AddStartBehaviour(
            new ApplyForceBehaviour(this.targetObject).WithDirectionVariable(CharVars.DAMAGE_EFFECT_DIRECTION.ToS())
            .WithForceVariable(CharVars.DAMAGE_EFFECT_FORCE.ToS()).WithRelativeXDirection(CharVars.NEW_FACING_DIRECTION.ToS(), true)
          );
          this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true));
          this.state.AddUpdateBehaviour(new CountInputBehaviour(this.targetObject, CharVars.JUMP_MODE.ToS(), (int) InputModes.RELEASE, CharVars.GENERIC_COUNTER.ToS()));
          this.state.AddExitBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.GENERIC_COUNTER.ToS(), 0));
          this.state.AddExitBehaviour(new RestoreGravityBehaviour(this.targetObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()));
          this.PlaySound(this.state);
        }

        public override void AddTransitions() {
          FSMTransition transition = null;
          foreach (string startingState in CharStates.All()) {
            transition = new FSMTransition(startingState, CharStates.GET_HIT.ToS(Suffixes.Knockback));
            transition.AddConditions(
              new VarConditionsBuilder()
              .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
              .WithVar(CharVars.DAMAGE_EFFECT, new IntCondition(CharVars.DAMAGE_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
              .WithVar(CharVars.IS_BLOCKING, new BoolCondition(CharVars.IS_BLOCKING.ToS(), Operators.EQUAL, false))
              .Build()
            );
            this.controller.fsm.RegisterTransition(transition);
          }
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Fall));
          transition.AddCondition(new TimeCondition(this.knockbackDuration));
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Fall));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_CEILING, new BoolCondition(CharVars.HIT_CEILING.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Fall));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_OBSTACLE, new BoolCondition(CharVars.HIT_OBSTACLE.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Fall));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_BOUNDS, new BoolCondition(CharVars.HIT_BOUNDS.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          transition = new FSMTransition(CharStates.GET_HIT.ToS(Suffixes.Knockback), CharStates.GET_HIT.ToS(Suffixes.Fall));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
            .WithVar(CharVars.VERTICAL_SPEED, new FloatCondition(CharVars.VERTICAL_SPEED.ToS(), Operators.LESS_THAN, 0f))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }

        public override void RemoveStates() {
          this.controller.fsm.RemoveState(this.state);
        }

      }
    }
  }
}