using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class MoveAction : IAction {

      private FSMState state = new ActionState(CharStates.MOVE.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.state);
      }

      public override void AddTransitions() {
        FSMTransition transition = null;
        transition = new FSMTransition(CharStates.IDLE.ToS(), CharStates.MOVE.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.HOLD))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.MOVE.ToS(), CharStates.IDLE.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }
    }
  }
}