using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class FlyAction : IAction {

      public float speedMultiplierIncrease = 0.5f;
      private FSMState state = new ActionState(CharStates.FLY.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new IgnoreGravityBehaviour(this.targetObject));
        this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_FLYING.ToS(), true));
        this.state.AddStartBehaviour(new VariableCalculatorBehaviour(this.targetObject, CharVars.SPEED_MULTIPLIER.ToS()).Add(speedMultiplierIncrease).WithPreviousStateIn(CharStates.JUMP.ToS(), CharStates.FALL.ToS()));
        this.state.AddUpdateBehaviour(
          new PlayAnimationBehaviour(this.targetObject, CharStates.FLY.ToS())
          .WithAltAnimation(CharStates.FLY_MOVE.ToS(Suffixes.H), new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.HOLD))
          .WithAltAnimation(CharStates.FLY_MOVE.ToS(Suffixes.V), new InputCondition(CharVars.VERTICAL_MODE.ToS(), Operators.EQUAL, InputModes.HOLD))
        );
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.state.AddExitBehaviour(new VariableCalculatorBehaviour(this.targetObject, CharVars.SPEED_MULTIPLIER.ToS()).Subtract(speedMultiplierIncrease).WithNextStateIn(CharStates.FALL.ToS()));
        this.PlaySound(this.state);
      }

      public override void AddTransitions() {
        this.AddTransitionToToggleFlight(CharStates.FLY.ToS(), CharStates.JUMP.ToS(), CharStates.FALL.ToS());
        this.AddTransitionToToggleFlight(CharStates.FALL.ToS(), CharStates.FLY.ToS());
      }

      private void AddTransitionToToggleFlight(string toState, params string[] fromStates) {
        FSMTransition transition = null;
        foreach (string fromState in fromStates) {
          transition = new FSMTransition(fromState, toState);
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }
    }
  }
}