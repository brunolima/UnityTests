using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;

namespace Characters {
  namespace Actions {
    public class IdleAction : IAction {

      private FSMState state = new ActionState(CharStates.IDLE.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.StartMachine(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.GENERIC_COUNTER.ToS(), 0));
        this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IGNORE_DRAG.ToS(), false));
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.state);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }

    }
  }
}