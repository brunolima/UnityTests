using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class GlideAction : IAction {
      public float glideSpeedMultiplier = 0.5f;

      private FSMState state = new ActionState(CharStates.GLIDE.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddUpdateBehaviour(new UpdateVelocityBehaviour(this.targetObject, new Vector2(1f, this.glideSpeedMultiplier)));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.state);
      }

      public override void AddTransitions() {
        FSMTransition transition = null;
        transition = new FSMTransition(CharStates.FALL.ToS(), CharStates.GLIDE.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.HOLD))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.GLIDE.ToS(), CharStates.FALL.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.GLIDE.ToS(), CharStates.FALL.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }
    }
  }
}