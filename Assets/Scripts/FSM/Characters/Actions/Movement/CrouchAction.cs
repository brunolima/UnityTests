using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    public class CrouchAction : IAction {

      private FSMState state = new ActionState(CharStates.CROUCH.ToS());
      private FSMState standState = new ActionState(CharStates.STAND.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.standState.Reset();
        this.controller.fsm.RegisterState(this.state);
        this.controller.fsm.RegisterState(this.standState);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.state);
        this.standState.WithDefaultBehaviours(this.targetObject);
        this.standState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.standState.state));
        this.standState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.standState);
      }

      public override void AddTransitions() {
        FSMTransition transition = null;
        this.AddTransitionsTo(CharStates.CROUCH.ToS(), CharStates.IDLE.ToS(), CharStates.MOVE.ToS());
        transition = new FSMTransition(CharStates.CROUCH.ToS(), CharStates.STAND.ToS());
        transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.CROUCH.ToS()));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.VERTICAL_MODE, new InputCondition(CharVars.VERTICAL_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.CROUCH.ToS(), CharStates.STAND.ToS());
        transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.CROUCH.ToS()));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.VERTICAL_VALUE, new FloatCondition(CharVars.VERTICAL_VALUE.ToS(), Operators.GREATER_THAN, -0.1f))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.STAND.ToS(), CharStates.IDLE.ToS());
        transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.STAND.ToS()));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.STAND.ToS(), CharStates.MOVE.ToS());
        transition.AddCondition(new AnimationEndCondition(this.targetObject.GetComponent<Animator>(), CharStates.STAND.ToS()));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.HOLD))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
      }

      void AddTransitionsTo(string toState, params string[] fromStates) {
        FSMTransition transition = null;
        foreach (string fromState in fromStates) {
          transition = new FSMTransition(fromState, toState);
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.VERTICAL_MODE, new InputCondition(CharVars.VERTICAL_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
            .WithVar(CharVars.VERTICAL_VALUE, new FloatCondition(CharVars.VERTICAL_VALUE.ToS(), Operators.LESS_THAN, 0f))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
        this.controller.fsm.RemoveState(this.standState);
      }

    }
  }
}