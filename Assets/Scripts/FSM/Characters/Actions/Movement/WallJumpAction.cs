using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class WallJumpAction : IAction {

      private FSMState state = new ActionState(CharStates.WALL_JUMP.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddStartBehaviour(new StopMovementBehaviour(this.targetObject).WithYAxis());
        this.state.AddStartBehaviour(
          new ApplyForceBehaviour(this.targetObject)
          .WithDirection(Vector2.up)
          .WithForceVariable(CharVars.JUMP_FORCE.ToS())
        );
        this.state.AddStartBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, false).Invert().WithForcedDirection(CharVars.WALL_DIRECTION.ToS()));
        this.state.AddUpdateBehaviour(
          new ApplyForceBehaviour(this.targetObject)
          .WithDirection(new Vector2(0.5f, 0f))
          .WithForceVariable(CharVars.JUMP_FORCE.ToS())
          .WithRelativeXDirection(CharVars.FACING_DIRECTION.ToS(), false)
        );
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.state);
      }

      public override void AddTransitions() {
        this.AddTransitionsTo(CharStates.WALL_JUMP.ToS(), CharStates.JUMP.ToS(), CharStates.FALL.ToS(), CharStates.WALL_JUMP.ToS());
      }

      void AddTransitionsTo(string toState, params string[] fromStates) {
        FSMTransition transition = null;
        foreach (string fromState in fromStates) {
          transition = new FSMTransition(fromState, toState);
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
            .WithVar(CharVars.HIT_WALL, new BoolCondition(CharVars.HIT_WALL.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }
    }
  }
}