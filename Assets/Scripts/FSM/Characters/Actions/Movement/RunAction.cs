using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class RunAction : IAction {

      public float speedMultiplierIncrease = 0.5f;
      private FSMState state = new ActionState(CharStates.RUN.ToS());

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddStartBehaviour(new VariableCalculatorBehaviour(this.targetObject, CharVars.SPEED_MULTIPLIER.ToS()).Add(speedMultiplierIncrease));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.state.AddExitBehaviour(new VariableCalculatorBehaviour(this.targetObject, CharVars.SPEED_MULTIPLIER.ToS()).Subtract(speedMultiplierIncrease));
        this.PlaySound(this.state);
      }

      public override void AddTransitions() {
        FSMTransition transition = null;
        transition = new FSMTransition(CharStates.IDLE.ToS(), CharStates.RUN.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.DOUBLE_TAP))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        transition = new FSMTransition(CharStates.RUN.ToS(), CharStates.IDLE.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }
    }
  }
}