using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class FallAction : IAction {
      private FSMState state = new ActionState(CharStates.FALL.ToS());
      public bool softLanding = true;

      public override void RegisterStates() {
        this.state.Reset();
        this.controller.fsm.RegisterState(this.state);
      }

      public override void AddBehaviours() {
        this.state.WithDefaultBehaviours(this.targetObject);
        this.state.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, this.state.state));
        this.state.AddStartBehaviour(new RestoreGravityBehaviour(this.targetObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()));
        this.state.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.IS_FLYING.ToS(), false));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        this.state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.state);
      }

      public override void AddTransitions() {
        this.AddTransitionsTo(CharStates.FALL.ToS(), CharStates.IDLE.ToS(), CharStates.MOVE.ToS(), CharStates.JUMP.ToS(), CharStates.AIR_JUMP.ToS(), CharStates.WALL_JUMP.ToS());
        FSMTransition transition = null;
        string landingState = CharStates.IDLE.ToS();
        if (this.softLanding) {
          landingState = CharStates.CROUCH.ToS();
        }
        transition = new FSMTransition(CharStates.FALL.ToS(), landingState);
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
          .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
        //TODO: Transition to Roll if SoftLanding is enabled?
        transition = new FSMTransition(CharStates.FALL.ToS(), CharStates.MOVE.ToS());
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.HOLD))
          .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
          // .WithVar(CharVars.SPEED_MULTIPLIER, new FloatCondition(CharVars.SPEED_MULTIPLIER.ToS(), Operators.EQUAL, 1f))
          .Build()
        );
        this.controller.fsm.RegisterTransition(transition);
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.state);
      }


      void AddTransitionsTo(string toState, params string[] fromStates) {
        FSMTransition transition = null;
        foreach (string fromState in fromStates) {
          transition = new FSMTransition(fromState, toState);
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.VERTICAL_SPEED, new FloatCondition(CharVars.VERTICAL_SPEED.ToS(), Operators.LESS_THAN, 0f))
            .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, false))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }
      }
    }
  }
}