using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class JumpAction : IAction {
      public bool allowCharge = false;
      [Header("Charged Jump")]
      public float maxChargeMultiplier = 2f;
      public float maxChargeSeconds = 2;
      public float minChargeSeconds = 1.75f;
      public bool moveWhileCharging = true;

      private FSMState chargeState = new ActionState(CharStates.JUMP.ToS(Suffixes.Charge));
      private FSMState jumpState = new ActionState(CharStates.JUMP.ToS());

      public override void RegisterStates() {
        if (this.allowCharge) {
          this.chargeState.Reset();
          this.controller.fsm.RegisterState(this.chargeState);
        }
        this.jumpState.Reset();
        this.controller.fsm.RegisterState(this.jumpState);
      }

      public override void AddBehaviours() {
        if (this.allowCharge) {
          this.chargeState.WithDefaultBehaviours(this.targetObject);
          this.chargeState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.GENERIC_CHARGE.ToS(), CharVars.GENERIC_CHARGE.defaultValue));
          this.chargeState.AddUpdateBehaviour(
            new PlayAnimationBehaviour(this.targetObject, CharStates.SUPER_JUMP.ToS(Suffixes.Charge))
            .WithAltAnimation(CharStates.MOVE.ToS(), new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.HOLD))
          );
          this.chargeState.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true));
          this.chargeState.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
          this.chargeState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
          this.chargeState.AddUpdateBehaviour(new ChargeBehaviour(this.targetObject, CharVars.GENERIC_CHARGE.ToS(), this.maxChargeMultiplier, this.maxChargeSeconds));
          this.PlaySound(this.chargeState);
        }
        this.jumpState.WithDefaultBehaviours(this.targetObject);
        this.jumpState.AddStartBehaviour(new PlayAnimationBehaviour(this.targetObject, CharStates.JUMP.ToS()));
        this.jumpState.AddStartBehaviour(new StopMovementBehaviour(this.targetObject).WithYAxis());
        ApplyForceBehaviour applyForceBehaviour = new ApplyForceBehaviour(this.targetObject).WithDirection(Vector2.up).WithForceVariable(CharVars.JUMP_FORCE.ToS());
        if (this.allowCharge) {
          applyForceBehaviour = applyForceBehaviour.WithChargeVariable(CharVars.GENERIC_CHARGE.ToS());
        }
        this.jumpState.AddStartBehaviour(applyForceBehaviour);
        this.jumpState.AddStartBehaviour(new SetVariableBehaviour(this.targetObject, CharVars.GENERIC_CHARGE.ToS(), CharVars.GENERIC_CHARGE.defaultValue));
        this.jumpState.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true));
        this.jumpState.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        this.jumpState.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        this.PlaySound(this.jumpState);
      }

      public override void AddTransitions() {
        if (!this.allowCharge) {
          this.AddTransitionsTo(CharStates.JUMP.ToS(), CharStates.IDLE.ToS(), CharStates.MOVE.ToS(), CharStates.RUN.ToS());
        } else {
          this.AddTransitionsTo(CharStates.JUMP.ToS(Suffixes.Charge), CharStates.IDLE.ToS(), CharStates.MOVE.ToS(), CharStates.RUN.ToS());
          FSMTransition transition = null;
          transition = new FSMTransition(CharStates.JUMP.ToS(Suffixes.Charge), CharStates.JUMP.ToS());
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }
      }

      void AddTransitionsTo(string toState, params string[] fromStates) {
        FSMTransition transition = null;
        foreach (string fromState in fromStates) {
          transition = new FSMTransition(fromState, toState);
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.JUMP_MODE, new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
            .WithVar(CharVars.HIT_GROUND, new BoolCondition(CharVars.HIT_GROUND.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
        }
      }

      public override void RemoveStates() {
        if (this.allowCharge) {
          this.controller.fsm.RemoveState(this.chargeState);
        }
        this.controller.fsm.RemoveState(this.jumpState);
      }
    }
  }
}