using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {

    public class DashAction : IAction {

      public float speedMultiplierIncrease = 1f;
      public float duration = 1f;
      public float cooldown = 1f;
      public bool ignoreGravity;
      public bool flightDash;
      private FSMState dashState = new ActionState(CharStates.DASH.ToS());
      private FSMState airDashState = new ActionState(CharStates.AIR_DASH.ToS());

      public override void RegisterStates() {
        this.dashState.Reset();
        this.controller.fsm.RegisterState(this.dashState);
        if (this.flightDash) {
          this.airDashState.Reset();
          this.controller.fsm.RegisterState(this.airDashState);
        }
      }

      public override void AddBehaviours() {
        this.AddBehavioursToState(this.dashState, false);
        if (this.flightDash) {
          this.AddBehavioursToState(this.airDashState, true);
        }
      }

      private void AddBehavioursToState(FSMState state, bool moveY) {
        state.WithDefaultBehaviours(this.targetObject);
        PlayAnimationBehaviour animationBehaviour = new PlayAnimationBehaviour(this.targetObject, CharStates.DASH.ToS(Suffixes.H));
        if (this.flightDash) {
          animationBehaviour = animationBehaviour.WithAltAnimation(CharStates.DASH.ToS(Suffixes.V), new InputCondition(CharVars.VERTICAL_MODE.ToS(), Operators.EQUAL, InputModes.DOUBLE_TAP));
        }
        state.AddStartBehaviour(animationBehaviour);
        if (this.ignoreGravity) {
          state.AddStartBehaviour(new IgnoreGravityBehaviour(this.targetObject));
        }
        state.AddStartBehaviour(new LockAxisBehaviour(this.targetObject, this.controller.fsm).WithXInput(CharVars.HORIZONTAL_MODE.ToS(), CharVars.HORIZONTAL_VALUE.ToS()).WithYInput(CharVars.VERTICAL_MODE.ToS(), CharVars.VERTICAL_VALUE.ToS()).AsXOR());
        state.AddStartBehaviour(new StopMovementBehaviour(this.targetObject).WithXAxis().WithYAxis());
        state.AddStartBehaviour(new VariableCalculatorBehaviour(this.targetObject, CharVars.SPEED_MULTIPLIER.ToS()).Add(speedMultiplierIncrease));
        state.AddUpdateBehaviour(BehavioursBuilder.BuildMove(this.targetObject, true, moveY));
        state.AddUpdateBehaviour(BehavioursBuilder.BuildFlip(this.targetObject, true));
        state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.targetObject, true, true, true));
        state.AddExitBehaviour(new CooldownBehaviour(this.targetObject, this.controller.fsm, CharVars.DASH_AVAILABLE.ToS(), this.cooldown));
        state.AddExitBehaviour(new VariableCalculatorBehaviour(this.targetObject, CharVars.SPEED_MULTIPLIER.ToS()).Subtract(speedMultiplierIncrease));
        state.AddExitBehaviour(new UnlockVariablesBehaviour(this.targetObject, this.controller.fsm, CharVars.HORIZONTAL_MODE.ToS(), CharVars.HORIZONTAL_VALUE.ToS(), CharVars.VERTICAL_MODE.ToS(), CharVars.VERTICAL_VALUE.ToS()));
        this.PlaySound(state);
      }


      public override void AddTransitions() {
        this.AddTransitionsTo(false, CharStates.DASH.ToS(), CharStates.IDLE.ToS(), CharStates.JUMP.ToS(), CharStates.FALL.ToS());
        FSMTransition transition = null;
        transition = new FSMTransition(CharStates.DASH.ToS(), CharStates.FALL.ToS());
        transition.AddCondition(new TimeCondition(this.duration));
        this.controller.fsm.RegisterTransition(transition);
        if (this.flightDash) {
          this.AddTransitionsTo(true, CharStates.AIR_DASH.ToS(), CharStates.FLY.ToS(), CharStates.FLY_MOVE.ToS());
          transition = new FSMTransition(CharStates.AIR_DASH.ToS(), CharStates.FLY.ToS());
          transition.AddCondition(new TimeCondition(this.duration));
          this.controller.fsm.RegisterTransition(transition);
        }
      }

      void AddTransitionsTo(bool allowVerticalDash, string toState, params string[] fromStates) {
        FSMTransition transition = null;
        foreach (string fromState in fromStates) {
          transition = new FSMTransition(fromState, toState);
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.HORIZONTAL_MODE, new InputCondition(CharVars.HORIZONTAL_MODE.ToS(), Operators.EQUAL, InputModes.DOUBLE_TAP))
            .WithVar(CharVars.DASH_AVAILABLE, new BoolCondition(CharVars.DASH_AVAILABLE.ToS(), Operators.EQUAL, true))
            .Build()
          );
          this.controller.fsm.RegisterTransition(transition);
          if (allowVerticalDash) {
            transition = new FSMTransition(fromState, toState);
            transition.AddConditions(
              new VarConditionsBuilder()
              .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
              .WithVar(CharVars.VERTICAL_MODE, new InputCondition(CharVars.VERTICAL_MODE.ToS(), Operators.EQUAL, InputModes.DOUBLE_TAP))
              .WithVar(CharVars.DASH_AVAILABLE, new BoolCondition(CharVars.DASH_AVAILABLE.ToS(), Operators.EQUAL, true))
              .Build()
            );
            this.controller.fsm.RegisterTransition(transition);
          }
        }
      }

      public override void RemoveStates() {
        this.controller.fsm.RemoveState(this.dashState);
        if (this.flightDash) {
          this.controller.fsm.RemoveState(this.airDashState);
        }
      }
    }
  }
}