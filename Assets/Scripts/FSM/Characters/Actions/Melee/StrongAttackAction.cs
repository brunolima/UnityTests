using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace Melee {

      public class StrongAttackAction : IAttackAction {
        public override void RegisterStates() {
          MeleeAttackBehaviour attackBehaviour = new MeleeAttackBehaviour(this.targetObject, CharVars.ACTION_CHECKS.ToS(), this.radius, CharVars.ENEMY_LAYER.ToS(), CharVars.FACING_DIRECTION.ToS())
          // .WithDamage(this.baseDamage)
          .WithCharge(CharVars.GENERIC_CHARGE.ToS())
          .WithEffect((int) this.effect)
          .WithEffectHorizontalDirection(null, 1f, true)
          .WithEffectVerticalDirection(CharVars.VERTICAL_VALUE.ToS(), 0f, 0f, 1f)
          .WithEffectForce(this.force);
          ;
          ChargedMeleeAttackBuilder attackBuilder =
            (ChargedMeleeAttackBuilder) new ChargedMeleeAttackBuilder(this.targetObject, this.input, CharVars.GENERIC_CHARGE.ToS(), (float)CharVars.GENERIC_CHARGE.defaultValue, this.maxChargeMultiplier, this.maxChargeSeconds)
            .WithState(CharStates.ATTACK_STRONG.ToS())
            .WithStep(this.stepForce)
            .WithAnimation(new PlayAnimationBehaviour(this.targetObject, CharStates.ATTACK_STRONG.ToS()))
            .WithStartingStates(CharStates.IDLE.ToS(), CharStates.MOVE.ToS(), CharStates.FLY.ToS())
            .WithUpdateBehaviours(attackBehaviour)
            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Pursuit), 1)
                       .WithCondition(new InputCondition(CharVars.JUMP_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                      )
            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Teleport), 2)
                       .WithCondition(new InputCondition(CharVars.FIRE1_MODE.ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                      );
          this.stateWrappers = attackBuilder.Build();
        }
      }
    }
  }
}