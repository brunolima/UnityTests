using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace Melee {

      public class StrongAttackPursuitAction : IAttackAction {
        [Header("Pursuit")]
        public bool enablePursuit = true;
        public InputActions pursuitInput = InputActions.Jump;
        public float speedIncrease = 1f;
        public float timeout = 1f;
        public int maxPursuits = 1;

        [Header("Teleport")]
        public bool enableTeleport = true;
        public InputActions teleportInput = InputActions.Fire1;
        public float teleportTimeout = 1f;
        public int maxTeleports = 1;
        public LayerMask teleportObstacles;
        public float distanceFromTarget = 10f;
        public AudioSource teleportStartSound;
        public AudioSource teleportUpdateSound;
        public AudioSource teleportExitSound;
        public GameObject teleportStartEffect;
        public GameObject teleportUpdateEffect;
        public GameObject teleportExitEffect;

        public override void RegisterStates() {
          ComboWrapper upCombo = new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Up), 1)
          .WithCondition(new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
          .WithCondition(new FloatCondition(CharVars.VERTICAL_VALUE.ToS(), Operators.GREATER_THAN, 0f))
          .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK));
          ComboWrapper downCombo = new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Down), 2)
          .WithCondition(new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
          .WithCondition(new FloatCondition(CharVars.VERTICAL_VALUE.ToS(), Operators.LESS_THAN, 0f))
          .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK));
          ComboWrapper forwardCombo = new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Forwards), 3)
          .WithCondition(new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
          .WithCondition(new FloatCondition(CharVars.VERTICAL_VALUE.ToS(), Operators.EQUAL, 0f))
          .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK));
          if (this.enablePursuit) {
            PursuitBuilder pursuitBuilder =
              new PursuitBuilder(this.targetObject, this.input, CharVars.TARGET.ToS(), this.timeout)
            .WithState(CharStates.ATTACK_STRONG.ToS(Suffixes.Pursuit))
            .WithMaxPursuits(CharVars.GENERIC_COUNTER, this.maxPursuits)
            .WithAnimation(new PlayAnimationBehaviour(this.targetObject, CharStates.DASH.ToS(Suffixes.H)))
            .WithSpeedMultiplierIncrease(this.speedIncrease)
            .WithCombo(upCombo)
            .WithCombo(downCombo)
            .WithCombo(forwardCombo)
            ;
            this.stateWrappers.AddRange(pursuitBuilder.Build());
          }
          if (this.enableTeleport) {
            TeleportBuilder teleportBuilder =
              new TeleportBuilder(this.targetObject, this.input, CharVars.TARGET.ToS(), this.timeout, this.teleportObstacles, this.distanceFromTarget)
            .WithState(CharStates.ATTACK_STRONG.ToS(Suffixes.Teleport))
            .WithMaxTeleports(CharVars.GENERIC_COUNTER, this.maxTeleports)
            .WithAnimation(new PlayAnimationBehaviour(this.targetObject, CharStates.WAIT_LOCKED.ToS(Suffixes.Disappear)))
            .WithWait(CharStates.ATTACK_STRONG.ToS(Suffixes.Wait), new PlayAnimationBehaviour(this.targetObject, CharStates.WAIT_LOCKED.ToS(Suffixes.Appear)))
            .WithFlip()
            .WithSounds(this.teleportStartSound, this.teleportUpdateSound, this.teleportExitSound)
            .WithEffects(this.teleportStartEffect, this.teleportUpdateEffect, this.teleportExitEffect)
            .WithCombo(upCombo)
            .WithCombo(downCombo)
            .WithCombo(forwardCombo)
            ;
            this.stateWrappers.AddRange(teleportBuilder.Build());
          }
          this.stateWrappers.AddRange(this.BuildAttackUp());
          this.stateWrappers.AddRange(this.BuildAttackDown());
          this.stateWrappers.AddRange(this.BuildAttackForward());
        }

        private List<StateWrapper> BuildAttackUp() {
          MeleeAttackBehaviour attackBehaviour = new MeleeAttackBehaviour(this.targetObject, CharVars.ACTION_CHECKS.ToS(), this.radius, CharVars.ENEMY_LAYER.ToS(), CharVars.FACING_DIRECTION.ToS())
          // .WithDamage(this.baseDamage)
          .WithCharge(CharVars.GENERIC_CHARGE.ToS())
          .WithEffect((int) this.effect)
          .WithEffectHorizontalDirection(CharVars.HORIZONTAL_VALUE.ToS(), 0f, true)
          .WithEffectVerticalDirection(CharVars.VERTICAL_VALUE.ToS(), 1f, 1f, 1f)
          .WithEffectForce(this.force)
          ;
          BasicMeleeAttackBuilder builder = (BasicMeleeAttackBuilder) new BasicMeleeAttackBuilder(this.targetObject, this.input)
                                            .WithState(CharStates.ATTACK_STRONG.ToS(Suffixes.Up))
                                            .WithAnimation(new PlayAnimationBehaviour(this.targetObject, CharStates.ATTACK_STRONG.ToS(Suffixes.Up)))
                                            .WithStartBehaviours(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), true), new IgnoreGravityBehaviour(this.targetObject))
                                            .WithUpdateBehaviours(attackBehaviour)
                                            .WithExitBehaviours(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), false), new RestoreGravityBehaviour(this.targetObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()).WithNextStateNotIn(CharStates.FLY.ToS()))
                                            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Pursuit), 1)
                                                       .WithCondition(new InputCondition(CharVars.FromInputMode(this.pursuitInput).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                                                       .WithCondition(new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.LESS_THAN, this.maxPursuits))
                                                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                                                      )
                                            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Teleport), 2)
                                                       .WithCondition(new InputCondition(CharVars.FromInputMode(this.teleportInput).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                                                       .WithCondition(new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.LESS_THAN, this.maxTeleports))
                                                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                                                      )
                                            ;
          return builder.Build();
        }

        private List<StateWrapper> BuildAttackDown() {
          MeleeAttackBehaviour attackBehaviour = new MeleeAttackBehaviour(this.targetObject, CharVars.ACTION_CHECKS.ToS(), this.radius, CharVars.ENEMY_LAYER.ToS(), CharVars.FACING_DIRECTION.ToS())
          // .WithDamage(this.baseDamage)
          .WithCharge(CharVars.GENERIC_CHARGE.ToS())
          .WithEffect((int) this.effect)
          .WithEffectHorizontalDirection(CharVars.HORIZONTAL_VALUE.ToS(), 0f, true)
          .WithEffectVerticalDirection(CharVars.VERTICAL_VALUE.ToS(), -1f, -1f, -1f)
          .WithEffectForce(this.force)
          ;
          BasicMeleeAttackBuilder builder = (BasicMeleeAttackBuilder)  new BasicMeleeAttackBuilder(this.targetObject, this.input)
                                            .WithState(CharStates.ATTACK_STRONG.ToS(Suffixes.Down))
                                            .WithAnimation(new PlayAnimationBehaviour(this.targetObject, CharStates.ATTACK_STRONG.ToS(Suffixes.Down)))
                                            .WithStartBehaviours(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), true), new IgnoreGravityBehaviour(this.targetObject))
                                            .WithUpdateBehaviours(attackBehaviour)
                                            .WithExitBehaviours(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), false), new RestoreGravityBehaviour(this.targetObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()).WithNextStateNotIn(CharStates.FLY.ToS()))
                                            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Pursuit), 1)
                                                       .WithCondition(new InputCondition(CharVars.FromInputMode(this.pursuitInput).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                                                       .WithCondition(new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.LESS_THAN, this.maxPursuits))
                                                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                                                      )
                                            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Teleport), 2)
                                                       .WithCondition(new InputCondition(CharVars.FromInputMode(this.teleportInput).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                                                       .WithCondition(new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.LESS_THAN, this.maxTeleports))
                                                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                                                      );
          return builder.Build();
        }

        private List<StateWrapper> BuildAttackForward() {
          MeleeAttackBehaviour attackBehaviour = new MeleeAttackBehaviour(this.targetObject, CharVars.ACTION_CHECKS.ToS(), this.radius, CharVars.ENEMY_LAYER.ToS(), CharVars.FACING_DIRECTION.ToS())
          // .WithDamage(this.baseDamage)
          .WithCharge(CharVars.GENERIC_CHARGE.ToS())
          .WithEffect((int) this.effect)
          .WithEffectHorizontalDirection(null, 1f, true)
          .WithEffectVerticalDirection(CharVars.VERTICAL_VALUE.ToS(), 0.25f, -0.25f, 0.25f)
          .WithEffectForce(this.force)
          ;
          BasicMeleeAttackBuilder builder = (BasicMeleeAttackBuilder) new BasicMeleeAttackBuilder(this.targetObject, this.input)
                                            .WithState(CharStates.ATTACK_STRONG.ToS(Suffixes.Forwards))
                                            .WithAnimation(new PlayAnimationBehaviour(this.targetObject, CharStates.ATTACK_STRONG.ToS(Suffixes.Forwards)))
                                            .WithStartBehaviours(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), true), new IgnoreGravityBehaviour(this.targetObject))
                                            .WithUpdateBehaviours(attackBehaviour)
                                            .WithExitBehaviours(new SetVariableBehaviour(this.targetObject, CharVars.IS_SLIDING.ToS(), false), new RestoreGravityBehaviour(this.targetObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()).WithNextStateNotIn(CharStates.FLY.ToS()))
                                            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Pursuit), 1)
                                                       .WithCondition(new InputCondition(CharVars.FromInputMode(this.pursuitInput).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                                                       .WithCondition(new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.LESS_THAN, this.maxPursuits))
                                                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                                                      )
                                            .WithCombo(new ComboWrapper(CharStates.ATTACK_STRONG.ToS(Suffixes.Teleport), 2)
                                                       .WithCondition(new InputCondition(CharVars.FromInputMode(this.teleportInput).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
                                                       .WithCondition(new IntCondition(CharVars.GENERIC_COUNTER.ToS(), Operators.LESS_THAN, this.maxTeleports))
                                                       .WithCondition(new IntCondition(CharVars.ATTACK_EFFECT.ToS(), Operators.EQUAL, (int)AttackEffects.KNOCKBACK))
                                                      );
          return builder.Build();
        }

      }
    }
  }
}