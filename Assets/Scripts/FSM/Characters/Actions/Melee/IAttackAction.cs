using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.Inputs;

namespace Characters {
  namespace Actions {
    namespace Melee {
      public abstract class IAttackAction : IAction {
        public InputActions input = InputActions.Fire1;
        public float force = 900f;
        public float stepForce = 40f;
        public float radius = 0.2f;
        // public float comboCooldown = 1f;

        public AttackEffects effect;

        [Header("Charge")]
        public float maxChargeMultiplier = 2f;
        public float maxChargeSeconds = 1;

        [HideInInspector]
        public List<StateWrapper> stateWrappers = new List<StateWrapper>();

        public override void AddBehaviours() {
          foreach (StateWrapper wrapper in this.stateWrappers) {
            this.controller.fsm.RegisterState(wrapper.state);
          }
        }

        public override void AddTransitions() {
          foreach (StateWrapper wrapper in this.stateWrappers) {
            foreach (FSMTransition transition in wrapper.transitions) {
              this.controller.fsm.RegisterTransition(transition);
            }
          }
        }

        public override void RemoveStates() {
          foreach (StateWrapper wrapper in this.stateWrappers) {
            this.controller.fsm.RemoveState(wrapper.state);
          }
        }
      }
    }
  }
}