using TwoDEngine.FSMCore;
using System.Collections.Generic;

namespace Characters {
  public class StateWrapper {
    public FSMState state;
    public List<FSMTransition> transitions = new List<FSMTransition>();
  }
}