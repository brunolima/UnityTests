using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;
using TwoDEngine.Inputs;

namespace Characters {

  public class CharVars {
    //// ATTRIBUTES

    //HEALTH
    //MAX_HEALTH
    //MANA
    //MAX_MANA
    public static CharVar MOVE_SPEED = new CharVar("MoveSpeed").WithDefaultValue(0f);
    public static CharVar MOVE_FORCE = new CharVar("MoveForce").WithDefaultValue(0f);
    public static CharVar SPEED_MULTIPLIER = new CharVar("SpeedMultiplier").WithDefaultValue(1f);
    public static CharVar JUMP_FORCE = new CharVar("JumpForce").WithDefaultValue(0f);
    public static CharVar HORIZONTAL_SPEED = new CharVar("HorizontalSpeed").WithDefaultValue(0f);
    public static CharVar VERTICAL_SPEED = new CharVar("VerticalSpeed").WithDefaultValue(0f).UseInTransitions();
    public static CharVar ORIGINAL_GRAVITY_SCALE = new CharVar("GravityScale").WithDefaultValue(1f);
    // public static CharVar BLOCK_RESISTANCE = new CharVar("BlockResistance").WithDefaultValue(1f);


    //// DIRECTIONS
    public static CharVar FACING_DIRECTION = new CharVar("FacingDirection").WithDefaultValue(1);
    public static CharVar NEW_FACING_DIRECTION = new CharVar("NewFacingDirection").WithDefaultValue(1);
    public static CharVar OBSTACLE_DIRECTION = new CharVar("ObstacleDirection").WithDefaultValue(1);
    public static CharVar WALL_DIRECTION = new CharVar("WallDirection").WithDefaultValue(1);

    //// POSITIONS
    public static CharVar BOUNDS_MIN_POSITION = new CharVar("BoundsMin");
    public static CharVar BOUNDS_MAX_POSITION = new CharVar("BoundsMax");

    //// FLAGS
    public static CharVar HIT_BOUNDS = new CharVar("HitBounds").WithDefaultValue(false).UseInTransitions();
    public static CharVar HIT_GROUND = new CharVar("HitGround").WithDefaultValue(false).UseInTransitions();
    public static CharVar HIT_CEILING = new CharVar("HitCeiling").WithDefaultValue(false).UseInTransitions();
    public static CharVar HIT_OBSTACLE = new CharVar("HitObstacle").WithDefaultValue(false).UseInTransitions();
    public static CharVar IS_FLYING = new CharVar("IsFlying").WithDefaultValue(false).UseInTransitions();
    public static CharVar HIT_WALL = new CharVar("HitWall").WithDefaultValue(false).UseInTransitions();
    public static CharVar IGNORE_DRAG = new CharVar("IgnoreDrag").WithDefaultValue(false);
    public static CharVar IS_SLIDING = new CharVar("IsSliding").WithDefaultValue(false);
    public static CharVar DASH_AVAILABLE = new CharVar("DashAvailable").WithDefaultValue(true).UseInTransitions();
    public static CharVar IS_BLOCKING = new CharVar("IsBlocking").WithDefaultValue(false).UseInTransitions();
    // public static CharVar INPUT_ALLOWED = new CharVar("InputAllowed").WithDefaultValue(false);

    //// INPUTS
    public static CharVar HORIZONTAL_MODE = new CharVar("Horizontal.Mode").WithDefaultValue(InputModes.NONE).UseInTransitions();
    public static CharVar HORIZONTAL_VALUE = new CharVar("Horizontal.Value").WithDefaultValue(0f);
    public static CharVar VERTICAL_MODE = new CharVar("Vertical.Mode").WithDefaultValue(InputModes.NONE).UseInTransitions();
    public static CharVar VERTICAL_VALUE = new CharVar("Vertical.Value").WithDefaultValue(0f).UseInTransitions();
    public static CharVar JUMP_MODE = new CharVar("Jump.Mode").WithDefaultValue(InputModes.NONE).UseInTransitions();
    public static CharVar FIRE1_MODE = new CharVar("Fire1.Mode").WithDefaultValue(InputModes.NONE).UseInTransitions();
    public static CharVar FIRE2_MODE = new CharVar("Fire2.Mode").WithDefaultValue(InputModes.NONE).UseInTransitions();

    //// COMBAT
    public static CharVar GENERIC_COUNTER = new CharVar("GenericCounter").WithDefaultValue(0).UseInTransitions();
    public static CharVar GENERIC_CHARGE = new CharVar("GenericCharge").WithDefaultValue(1f);
    public static CharVar ACTION_CHECKS = new CharVar("ActionChecks");
    public static CharVar ENEMY_LAYER = new CharVar("EnemyLayer");
    public static CharVar DAMAGE_EFFECT = new CharVar("DamageEffect").WithDefaultValue(0).UseInTransitions();
    public static CharVar DAMAGE_EFFECT_DIRECTION = new CharVar("DamageEffectDirection").WithDefaultValue(Vector2.zero);
    public static CharVar DAMAGE_EFFECT_FORCE = new CharVar("DamageEffectForce").WithDefaultValue(0f);
    public static CharVar TARGET = new CharVar("Target").UseInTransitions();
    public static CharVar COMBO_VARIANT = new CharVar("ComboVariant").WithDefaultValue(0).UseInTransitions();
    public static CharVar ATTACK_EFFECT = new CharVar("AttackEffect").WithDefaultValue(0);

    // LOCK_ON
    // TARGET
    // HIT_COUNT

    //// COMBO
    // COMBO_COUNT
    // CAN_COMBO

    // COOLDOWNS

    // namespace MyTests {
    //   namespace FSMs {
    //     public enum FSMStates {

    //     public enum FSMVariables {
    //       HIT_GROUND, HIT_CEILING, HIT_OBSTACLE, HIT_WALL, HIT_BOUNDS,
    //       OBSTACLE_DIRECTION, WALL_DIRECTION,
    //       VERTICAL_SPEED, ORIGINAL_LINEAR_DRAG, ORIGINAL_GRAVITY_SCALE,
    //       FLYING,
    //       MOVEMENT_MODE, MOVEMENT_SPEED, MOVEMENT_FORCE, FACING_DIRECTION, SPEED_MULTIPLIER, FIXED_HORIZONTAL_VALUE, FIXED_VERTICAL_VALUE, MIN_POSITION, MAX_POSITION,
    //       NEW_DIRECTION,
    //       AIR_JUMP_COUNT, JUMP_CHARGE, STRONG_ATTACK_CHARGE,
    //       DASHING_AVAILABLE,
    //       DAMAGE_EFFECT, DAMAGE_EFFECT_DIRECTION, DAMAGE_EFFECT_FORCE,
    //       KNOCKBACK_RECOVER_COUNT, HIT_COUNT, HIT_EFFECT, DISABLED_STRONG_ATTACK_COMBO, CAN_COMBO,
    //       ATTACK_TARGET, PURSUIT_COUNT,
    //     }

    //     public enum AttackEffects { NONE, HIT, KNOCKBACK }

    //   }
    // }

    public static List<CharVar> All() {
      List<CharVar> all = new List<CharVar>() { ATTACK_EFFECT };
      all.AddRange(Attributes());
      all.AddRange(Directions());
      all.AddRange(Positions());
      all.AddRange(Flags());
      all.AddRange(Inputs());
      all.AddRange(Combats());
      return all;
    }

    public static List<CharVar> Attributes() {
      return new List<CharVar>() {
        MOVE_SPEED, MOVE_FORCE, SPEED_MULTIPLIER, JUMP_FORCE, HORIZONTAL_SPEED, VERTICAL_SPEED, ORIGINAL_GRAVITY_SCALE
      };
    }

    public static List<CharVar> Directions() {
      return new List<CharVar>() {
        FACING_DIRECTION, NEW_FACING_DIRECTION, OBSTACLE_DIRECTION, WALL_DIRECTION
      };
    }

    public static List<CharVar> Positions() {
      return new List<CharVar>() {
        BOUNDS_MIN_POSITION, BOUNDS_MAX_POSITION
      };
    }

    public static List<CharVar> Flags() {
      return new List<CharVar>() {
        HIT_BOUNDS, HIT_GROUND, HIT_CEILING, HIT_OBSTACLE, IS_FLYING, HIT_WALL, IGNORE_DRAG, DASH_AVAILABLE, IS_SLIDING, IS_BLOCKING
      };
    }

    public static List<CharVar> Inputs() {
      return new List<CharVar>() {
        HORIZONTAL_MODE, HORIZONTAL_VALUE, VERTICAL_MODE, VERTICAL_VALUE, JUMP_MODE, FIRE1_MODE, FIRE2_MODE
      };
    }

    public static List<CharVar> Combats() {
      return new List<CharVar>() {
        GENERIC_COUNTER, GENERIC_CHARGE, ACTION_CHECKS, ENEMY_LAYER, DAMAGE_EFFECT, DAMAGE_EFFECT_DIRECTION, DAMAGE_EFFECT_FORCE, TARGET, COMBO_VARIANT
      };
    }

    public static List<CharVar> AllForTransitions() {
      List<CharVar> vars = new List<CharVar>();
      foreach (CharVar charVar in All()) {
        if (charVar.useInTransitions) {
          vars.Add(charVar);
        }
      }
      return vars;
    }

    public static List<CharVar> AllDefault() {
      List<CharVar> vars = new List<CharVar>();
      foreach (CharVar charVar in All()) {
        if (charVar.hasDefaultValue) {
          vars.Add(charVar);
        }
      }
      return vars;
    }

    public static CharVar FromInputMode(InputActions input) {
      switch (input) {
      case InputActions.Horizontal:
        return HORIZONTAL_MODE;
      case InputActions.Vertical:
        return VERTICAL_MODE;
      case InputActions.Jump:
        return JUMP_MODE;
      case InputActions.Fire1:
        return FIRE1_MODE;
      case InputActions.Fire2:
        return FIRE2_MODE;
      default:
        return null;
      }
    }

    public static CharVar FromInputValue(InputActions input) {
      switch (input) {
      case InputActions.Horizontal:
        return HORIZONTAL_VALUE;
      case InputActions.Vertical:
        return VERTICAL_VALUE;
      default:
        return null;
      }
    }
  }

  public class CharVar {
    public string name;
    public object defaultValue;
    public bool hasDefaultValue;
    public bool useInTransitions;

    public CharVar(string name) {
      this.name = name;
    }

    public CharVar WithDefaultValue(object defaultValue) {
      this.defaultValue = defaultValue;
      this.hasDefaultValue = true;
      return this;
    }

    public CharVar UseInTransitions() {
      this.useInTransitions = true;
      return this;
    }

    public string ToS() {
      return name;
    }
  }

}