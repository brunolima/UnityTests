namespace Characters {
  public enum AttackEffects {
    NONE,
    HIT,
    KNOCKBACK,
  }
}