using System.Collections.Generic;
namespace Characters {
  public class CharStates {

    public static CharState IDLE = new CharState("Idle");
    public static CharState MOVE = new CharState("Move");
    public static CharState CROUCH = new CharState("Crouch");
    public static CharState STAND = new CharState("Stand");
    public static CharState FALL = new CharState("Fall");
    public static CharState JUMP = new CharState("Jump");
    public static CharState SUPER_JUMP = new CharState("SuperJump").RegisterSuffixes(Suffixes.Charge);
    public static CharState AIR_JUMP = new CharState("AirJump");
    public static CharState GLIDE = new CharState("Glide");
    public static CharState FLY = new CharState("FlyIdle");
    public static CharState FLY_MOVE = new CharState("FlyMove").RegisterSuffixes(Suffixes.H).RegisterSuffixes(Suffixes.V);
    public static CharState RUN = new CharState("Run");
    public static CharState FLY_FAST = new CharState("FlyFast").RegisterSuffixes(Suffixes.H).RegisterSuffixes(Suffixes.V);
    public static CharState DASH = new CharState("Dash").RegisterSuffixes(Suffixes.H).RegisterSuffixes(Suffixes.V);
    public static CharState AIR_DASH = new CharState("AirDash");
    public static CharState WALL_JUMP = new CharState("WallJump");

    public static CharState ATTACK_STRONG = new CharState("AttackStrong").RegisterSuffixes(Suffixes.Charge).RegisterSuffixes(Suffixes.Down).RegisterSuffixes(Suffixes.Up)
    .RegisterSuffixes(Suffixes.Forwards).RegisterSuffixes(Suffixes.Pursuit).RegisterSuffixes(Suffixes.Teleport).RegisterSuffixes(Suffixes.Wait);
    public static CharState GET_HIT = new CharState("GetHit").RegisterSuffixes(Suffixes.Hit).RegisterSuffixes(Suffixes.Knockback).RegisterSuffixes(Suffixes.Knockback, Suffixes.Recover)
    .RegisterSuffixes(Suffixes.Fall).RegisterSuffixes(Suffixes.Down).RegisterSuffixes(Suffixes.Down, Suffixes.Up).RegisterSuffixes(Suffixes.Down, Suffixes.Recover);
    public static CharState WAIT_LOCKED = new CharState("WaitLocked").RegisterSuffixes(Suffixes.Appear).RegisterSuffixes(Suffixes.Disappear);

    public static CharState BLOCK = new CharState("Block").RegisterSuffixes(Suffixes.Release).RegisterSuffixes(Suffixes.Hit).RegisterSuffixes(Suffixes.Knockback);

    public static List<string> All() {
      List<string> allStates = new List<string>();
      allStates.AddRange(IDLE.possibleNames);
      allStates.AddRange(MOVE.possibleNames);
      allStates.AddRange(CROUCH.possibleNames);
      allStates.AddRange(STAND.possibleNames);
      allStates.AddRange(FALL.possibleNames);
      allStates.AddRange(JUMP.possibleNames);
      allStates.AddRange(SUPER_JUMP.possibleNames);
      allStates.AddRange(AIR_JUMP.possibleNames);
      allStates.AddRange(GLIDE.possibleNames);
      allStates.AddRange(FLY.possibleNames);
      allStates.AddRange(FLY_MOVE.possibleNames);
      allStates.AddRange(RUN.possibleNames);
      allStates.AddRange(FLY_FAST.possibleNames);
      allStates.AddRange(DASH.possibleNames);
      allStates.AddRange(AIR_DASH.possibleNames);
      allStates.AddRange(WALL_JUMP.possibleNames);
      allStates.AddRange(ATTACK_STRONG.possibleNames);
      allStates.AddRange(GET_HIT.possibleNames);
      allStates.AddRange(WAIT_LOCKED.possibleNames);
      allStates.AddRange(BLOCK.possibleNames);
      return allStates;
    }
  }

  public enum Suffixes {
    None, Charge, H, V, Pursuit, Up, Down, Forwards,
    Hit, Knockback, Fall, Recover, Teleport, Wait, Release, Appear, Disappear
  }

  public class CharState {
    public string name;
    public List<string> possibleNames = new List<string>();
    public CharState(string name) {
      this.name = name;
      this.possibleNames.Add(name);
    }

    public CharState RegisterSuffixes(params Suffixes[] suffixes) {
      this.possibleNames.Add(this.ToS(suffixes));
      return this;
    }

    public string ToS(params Suffixes[] suffixes) {
      string allSuffixes = "";
      foreach (Suffixes suffix in suffixes) {
        if (suffix != Suffixes.None) {
          allSuffixes += "." + suffix;
        }
      }
      return this.name + allSuffixes;
    }
  }

}