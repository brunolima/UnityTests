using UnityEngine;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
namespace Characters {
  public class ActionState : FSMState {

    public ActionState(string state): base(state) {}

    public override void WithDefaultBehaviours(GameObject gameObject) {
      this.AddStartBehaviour(new SetVariableBehaviour(gameObject, CharVars.COMBO_VARIANT.ToS(), 0));
    }
  }
}