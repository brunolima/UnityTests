using UnityEngine;
using TwoDEngine.FSMCore.Behaviours;

namespace Characters {
  public class BehavioursBuilder {
    public static MoveBehaviour BuildMove(GameObject gameObject, bool moveX, bool moveY = false) {
      MoveBehaviour behaviour = new MoveBehaviour(gameObject).WithSpeed(CharVars.MOVE_SPEED.ToS(), CharVars.MOVE_FORCE.ToS(), CharVars.SPEED_MULTIPLIER.ToS())
      .AvoidObstacle(CharVars.HIT_OBSTACLE.ToS(), CharVars.OBSTACLE_DIRECTION.ToS()).AvoidGround(CharVars.HIT_GROUND.ToS()).AvoidCeiling(CharVars.HIT_CEILING.ToS());
      if (moveX) {
        behaviour = behaviour.InXAxis(CharVars.HORIZONTAL_VALUE.ToS());
      }
      if (moveY) {
        behaviour = behaviour.InYAxis(CharVars.VERTICAL_VALUE.ToS());
      }
      return behaviour;
    }

    public static MoveTowardsBehaviour BuildMoveTowards(GameObject gameObject, bool moveX, bool moveY = false) {
      MoveBehaviour behaviour = new MoveTowardsBehaviour(gameObject).WithSpeed(CharVars.MOVE_SPEED.ToS(), CharVars.MOVE_FORCE.ToS(), CharVars.SPEED_MULTIPLIER.ToS())
      .AvoidObstacle(CharVars.HIT_OBSTACLE.ToS(), CharVars.OBSTACLE_DIRECTION.ToS()).AvoidGround(CharVars.HIT_GROUND.ToS()).AvoidCeiling(CharVars.HIT_CEILING.ToS());
      if (moveX) {
        behaviour = behaviour.InXAxis(CharVars.HORIZONTAL_VALUE.ToS());
      }
      if (moveY) {
        behaviour = behaviour.InYAxis(CharVars.VERTICAL_VALUE.ToS());
      }
      return (MoveTowardsBehaviour) behaviour;
    }

    public static FlipBehaviour BuildFlip(GameObject gameObject, bool useInput) {
      FlipBehaviour behaviour = new FlipBehaviour(gameObject, CharVars.FACING_DIRECTION.ToS());
      if (useInput) {
        behaviour = behaviour.WithInput(CharVars.HORIZONTAL_VALUE.ToS());
      }
      return behaviour;
    }

    public static KeepInsideBoundsBehaviour BuildKeepInsideBounds(GameObject gameObject, bool checkX, bool checkY, bool withInput = false, bool storeCollision = true) {
      KeepInsideBoundsBehaviour behaviour = new KeepInsideBoundsBehaviour(gameObject, CharVars.BOUNDS_MIN_POSITION.ToS(), CharVars.BOUNDS_MAX_POSITION.ToS());
      if (checkX) {
        if (withInput) {
          behaviour = behaviour.WithHInput(CharVars.HORIZONTAL_VALUE.ToS(), CharVars.FACING_DIRECTION.ToS());
        } else {
          behaviour = behaviour.WithXAxis();
        }
      }
      if (checkY) {
        if (withInput) {
          behaviour = behaviour.WithVInput(CharVars.VERTICAL_VALUE.ToS());
        } else {
          behaviour = behaviour.WithYAxis();
        }
      }
      if (storeCollision) {
        behaviour = behaviour.StoringCollision(CharVars.HIT_BOUNDS.ToS());
      }
      return behaviour;
    }

  }
}