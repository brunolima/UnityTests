using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;

namespace Characters {

  public class ChargedMeleeAttackBuilder : IMeleeAttackBuilder {
    private string chargeStateName;
    private string chargeVariable;
    private float initialChargeMultiplier;
    private float maxChargeMultiplier;
    private float maxChargeSeconds;

    public ChargedMeleeAttackBuilder(GameObject gameObject, InputActions input, string chargeVariable, float initialChargeMultiplier, float maxChargeMultiplier, float maxChargeSeconds) : base(gameObject, input) {
      this.chargeVariable = chargeVariable;
      this.initialChargeMultiplier = initialChargeMultiplier;
      this.maxChargeMultiplier = maxChargeMultiplier;
      this.maxChargeSeconds = maxChargeSeconds;
    }

    public override IMeleeAttackBuilder WithState(string stateName) {
      this.stateName = stateName;
      this.chargeStateName = stateName + "." + Suffixes.Charge.ToString();
      return this;
    }

    public override List<StateWrapper> Build() {
      List<StateWrapper> states = new List<StateWrapper>();
      states.Add(this.BuildCharge());
      BasicMeleeAttackBuilder basicAttackBuilder = (BasicMeleeAttackBuilder) new BasicMeleeAttackBuilder(this.gameObject, this.input)
                                                   .WithState(this.stateName)
                                                   .WithStep(this.stepForce)
                                                   .WithAnimation(this.animationBehaviour)
                                                   .WithStartingStates(this.chargeStateName)
                                                   .WithUpdateBehaviours(this.customUpdateBehaviours)
                                                   .WithExitBehaviours(this.customExitBehaviours);
      foreach (ComboWrapper combo in this.combos.Values) {
        basicAttackBuilder = (BasicMeleeAttackBuilder) basicAttackBuilder.WithCombo(combo);
      }
      states.AddRange(basicAttackBuilder.FromCharge().Build());
      return states;
    }

    private StateWrapper BuildCharge() {
      StateWrapper stateWrapper = new StateWrapper();
      FSMState state = new ActionState(this.chargeStateName);
      state.WithDefaultBehaviours(this.gameObject);
      if (this.animationBehaviour != null) {
        state.AddStartBehaviour(this.animationBehaviour);
      }
      state.AddStartBehaviour(new SetAnimationSpeedBehaviour(this.gameObject, 0f));
      state.AddStartBehaviour(new SetVariableBehaviour(this.gameObject, this.chargeVariable, this.initialChargeMultiplier));
      foreach (FSMBehaviour behaviour in this.customStartBehaviours) {
        state.AddStartBehaviour(behaviour);
      }
      state.AddUpdateBehaviour(new ChargeBehaviour(this.gameObject, this.chargeVariable, this.maxChargeMultiplier, this.maxChargeSeconds));
      state.AddExitBehaviour(new SetAnimationSpeedBehaviour(this.gameObject, 1f));
      stateWrapper.state = state;
      FSMTransition transition = null;
      foreach (string startingState in this.startingStates) {
        transition = new FSMTransition(startingState, this.chargeStateName);
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
          .Build()
        );
        stateWrapper.transitions.Add(transition);
      }
      return stateWrapper;
    }

  }
}