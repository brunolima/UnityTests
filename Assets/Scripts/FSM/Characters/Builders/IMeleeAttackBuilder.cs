using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.Inputs;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;

namespace Characters {

  public abstract class IMeleeAttackBuilder {
    public GameObject gameObject;
    public InputActions input;

    public string stateName;
    public PlayAnimationBehaviour animationBehaviour;
    public float stepForce;
    public string[] startingStates = new string[0];
    public List<string> animationNames = new List<string>();
    public Dictionary<CharVar, ICondition> customTransitions = new Dictionary<CharVar, ICondition>();

    public FSMBehaviour[] customStartBehaviours = new FSMBehaviour[0];
    public FSMBehaviour[] customUpdateBehaviours = new FSMBehaviour[0];
    public FSMBehaviour[] customExitBehaviours = new FSMBehaviour[0];

    public Dictionary<int, ComboWrapper> combos = new Dictionary<int, ComboWrapper>();

    public IMeleeAttackBuilder(GameObject gameObject, InputActions input) {
      this.gameObject = gameObject;
      this.input = input;
    }

    public virtual IMeleeAttackBuilder WithStep(float stepForce) {
      this.stepForce = stepForce;
      return this;
    }

    public virtual IMeleeAttackBuilder WithState(string stateName) {
      this.stateName = stateName;
      return this;
    }

    public virtual IMeleeAttackBuilder WithStartBehaviours(params FSMBehaviour[] behaviours) {
      this.customStartBehaviours = behaviours;
      return this;
    }

    public virtual IMeleeAttackBuilder WithUpdateBehaviours(params FSMBehaviour[] behaviours) {
      this.customUpdateBehaviours = behaviours;
      return this;
    }

    public virtual IMeleeAttackBuilder WithExitBehaviours(params FSMBehaviour[] behaviours) {
      this.customExitBehaviours = behaviours;
      return this;
    }

    public virtual IMeleeAttackBuilder WithTransition(CharVar charVar, ICondition condition) {
      this.customTransitions[charVar] = condition;
      return this;
    }

    public virtual IMeleeAttackBuilder WithCombo(ComboWrapper comboWrapper) {
      this.combos[comboWrapper.variant] = comboWrapper;
      return this;
    }

    public IMeleeAttackBuilder WithAnimation(PlayAnimationBehaviour animationBehaviour) {
      this.animationBehaviour = animationBehaviour;
      this.animationNames.Add(animationBehaviour.defaultAnimation);
      foreach (string altAnimation in animationBehaviour.altAnimation.Keys) {
        this.animationNames.Add(altAnimation);
      }
      return this;
    }

    public IMeleeAttackBuilder WithStartingStates(params string[] startingStates) {
      this.startingStates = startingStates;
      return this;
    }

    public abstract List<StateWrapper> Build();

  }

  public class ComboWrapper {
    public string toState;
    public int variant;
    public SetVariableBehaviour behaviour;
    public List<ICondition> conditions = new List<ICondition>();

    public ComboWrapper(string toState, int variant) {
      this.toState = toState;
      this.variant = variant;
      if (this.variant == 0) {
        throw new Exception("Combo cannot have a variant of 0 ");
      }
    }

    public ComboWrapper WithCondition(ICondition condition) {
      this.conditions.Add(condition);
      return this;
    }



  }
}