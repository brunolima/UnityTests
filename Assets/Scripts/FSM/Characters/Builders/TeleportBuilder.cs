using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;

namespace Characters {

  public class TeleportBuilder {
    public GameObject gameObject;
    public InputActions input;

    public string stateName;
    public PlayAnimationBehaviour animationBehaviour;
    public float speedIncrease;
    public float timeout;
    public string[] startingStates = new string[0];
    public List<string> animationNames = new List<string>();
    public LayerMask obstacles;
    public float distance;

    public FSMBehaviour[] customStartBehaviours = new FSMBehaviour[0];
    public FSMBehaviour[] customUpdateBehaviours = new FSMBehaviour[0];
    public FSMBehaviour[] customExitBehaviours = new FSMBehaviour[0];

    public GameObject target;
    public string targetVariable;
    public Dictionary<int, ComboWrapper> combos = new Dictionary<int, ComboWrapper>();

    public CharVar counterVariable;
    public int maxTeleports;

    public string waitStateName;
    public PlayAnimationBehaviour waitAnimationBehaviour;
    public bool flip;

    public AudioSource soundStart;
    public AudioSource soundUpdate;
    public AudioSource soundExit;

    public GameObject effectStart;
    public GameObject effectUpdate;
    public GameObject effectExit;

    public TeleportBuilder(GameObject gameObject, InputActions input, GameObject target, float timeout, LayerMask obstacles, float distance) {
      this.gameObject = gameObject;
      this.input = input;
      this.target = target;
      this.timeout = timeout;
      this.obstacles = obstacles;
      this.distance = distance;
    }

    public TeleportBuilder(GameObject gameObject, InputActions input, string targetVariable, float timeout, LayerMask obstacles, float distance) {
      this.gameObject = gameObject;
      this.input = input;
      this.targetVariable = targetVariable;
      this.timeout = timeout;
      this.obstacles = obstacles;
      this.distance = distance;
    }

    public TeleportBuilder WithState(string stateName) {
      this.stateName = stateName;
      return this;
    }

    public TeleportBuilder WithSpeedMultiplierIncrease(float speedIncrease) {
      this.speedIncrease = speedIncrease;
      return this;
    }

    public TeleportBuilder WithStartBehaviours(params FSMBehaviour[] behaviours) {
      this.customStartBehaviours = behaviours;
      return this;
    }

    public TeleportBuilder WithUpdateBehaviours(params FSMBehaviour[] behaviours) {
      this.customUpdateBehaviours = behaviours;
      return this;
    }

    public TeleportBuilder WithExitBehaviours(params FSMBehaviour[] behaviours) {
      this.customExitBehaviours = behaviours;
      return this;
    }

    public TeleportBuilder WithAnimation(PlayAnimationBehaviour animationBehaviour) {
      this.animationBehaviour = animationBehaviour;
      this.animationNames.Add(animationBehaviour.defaultAnimation);
      foreach (string altAnimation in animationBehaviour.altAnimation.Keys) {
        this.animationNames.Add(altAnimation);
      }
      return this;
    }

    public TeleportBuilder WithWait(string waitStateName, PlayAnimationBehaviour animationBehaviour) {
      this.waitStateName = waitStateName;
      this.waitAnimationBehaviour = animationBehaviour;
      return this;
    }

    public TeleportBuilder WithStartingStates(params string[] startingStates) {
      this.startingStates = startingStates;
      return this;
    }

    public virtual TeleportBuilder WithCombo(ComboWrapper comboWrapper) {
      this.combos[comboWrapper.variant] = comboWrapper;
      return this;
    }

    public virtual TeleportBuilder WithMaxTeleports(CharVar counterVariable, int maxTeleports) {
      this.counterVariable = counterVariable;
      this.maxTeleports = maxTeleports;
      return this;
    }

    public virtual TeleportBuilder WithFlip() {
      this.flip = true;
      return this;
    }

    public virtual TeleportBuilder WithSounds(AudioSource soundStart, AudioSource soundUpdate, AudioSource soundExit) {
      this.soundStart = soundStart;
      this.soundUpdate = soundUpdate;
      this.soundExit = soundExit;
      return this;
    }

    public virtual TeleportBuilder WithEffects(GameObject effectStart, GameObject effectUpdate, GameObject effectExit) {
      this.effectStart = effectStart;
      this.effectUpdate = effectUpdate;
      this.effectExit = effectExit;
      return this;
    }

    public List<StateWrapper> Build() {
      List<StateWrapper> states = new List<StateWrapper>();
      states.Add(this.BuildTeleport());
      if (this.waitStateName != null) {
        states.Add(this.BuildWait());
      }
      return states;
    }

    private StateWrapper BuildTeleport() {
      StateWrapper stateWrapper = new StateWrapper();
      FSMState state = new ActionState(this.stateName);
      state.WithDefaultBehaviours(this.gameObject);
      if (this.animationBehaviour != null) {
        state.AddStartBehaviour(this.animationBehaviour);
      }
      if (this.counterVariable != null) {
        state.AddStartBehaviour(new VariableCalculatorBehaviour(this.gameObject, this.counterVariable.ToS()).Add(1));
      }
      if (this.flip && this.waitStateName == null) {
        state.AddStartBehaviour(BehavioursBuilder.BuildFlip(this.gameObject, false).Invert());
      }
      if (this.soundStart != null) {
        state.AddStartBehaviour(new PlaySoundBehaviour(this.gameObject, this.soundStart).Force());
      }
      if (this.effectStart != null) {
        state.AddStartBehaviour(new InstantiateBehaviour(this.gameObject, this.effectStart));
      }
      foreach (FSMBehaviour behaviour in this.customStartBehaviours) {
        state.AddStartBehaviour(behaviour);
      }
      if (this.soundUpdate != null) {
        state.AddUpdateBehaviour(new PlaySoundBehaviour(this.gameObject, this.soundUpdate));
        state.AddExitBehaviour(new StopSoundBehaviour(this.gameObject, this.soundUpdate));
      }
      if (this.effectUpdate != null) {
        state.AddUpdateBehaviour(new InstantiateBehaviour(this.gameObject, this.effectUpdate));
      }
      foreach (FSMBehaviour behaviour in this.customUpdateBehaviours) {
        state.AddUpdateBehaviour(behaviour);
      }
      state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.gameObject, true, true));
      if (this.waitStateName == null) {
        foreach (ComboWrapper combo in this.combos.Values) {
          SetVariableBehaviour behaviour = new SetVariableBehaviour(this.gameObject, CharVars.COMBO_VARIANT.ToS(), combo.variant);
          foreach (ICondition condition in combo.conditions) {
            behaviour = behaviour.WithCondition(condition);
          }
          state.AddUpdateBehaviour(behaviour);
        }
      }
      TeleportToBehaviour teleportToBehaviour = new TeleportToBehaviour(this.gameObject, this.obstacles).TeleportAhead(this.distance);
      if (this.target != null) {
        teleportToBehaviour = teleportToBehaviour.WithTarget(this.target);
      }
      if (this.targetVariable != null) {
        teleportToBehaviour = teleportToBehaviour.WithTargetVariable(this.targetVariable);
      }
      state.AddExitBehaviour(teleportToBehaviour);
      foreach (FSMBehaviour behaviour in this.customExitBehaviours) {
        state.AddExitBehaviour(behaviour);
      }
      if (this.soundExit != null) {
        state.AddExitBehaviour(new PlaySoundBehaviour(this.gameObject, this.soundExit).Force());
      }
      if (this.effectExit != null) {
        state.AddExitBehaviour(new InstantiateBehaviour(this.gameObject, this.effectExit));
      }
      stateWrapper.state = state;
      //TRANSITIONS
      FSMTransition transition = null;
      foreach (string startingState in this.startingStates) {
        transition = new FSMTransition(startingState, this.stateName);
        VarConditionsBuilder builder = new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
        .WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
        .WithVar(CharVars.TARGET, new ObjectCondition(this.targetVariable));
        if (this.counterVariable != null) {
          builder = builder.WithVar(this.counterVariable, new IntCondition(this.counterVariable.ToS(), Operators.LESS_THAN, this.maxTeleports));
        }
        transition.AddConditions(builder.Build());
        stateWrapper.transitions.Add(transition);
      }
      //RETURN
      if (this.waitStateName == null) {
        foreach (string animationName in this.animationNames) {
          foreach (ComboWrapper combo in this.combos.Values) {
            transition = new FSMTransition(this.stateName, combo.toState);
            transition.AddCondition(new AnimationEndCondition(this.gameObject.GetComponent<Animator>(), animationName));
            transition.AddConditions(
              new VarConditionsBuilder()
              .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
              .WithVar(CharVars.COMBO_VARIANT, new IntCondition(CharVars.COMBO_VARIANT.ToS(), Operators.EQUAL, combo.variant))
              .Build()
            );
            stateWrapper.transitions.Add(transition);
          }
        }
        transition = new FSMTransition(this.stateName, CharStates.FALL.ToS());
        transition.AddCondition(new TimeCondition(this.timeout));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, false))
          .Build()
        );
        stateWrapper.transitions.Add(transition);
        transition = new FSMTransition(this.stateName, CharStates.FLY.ToS());
        transition.AddCondition(new TimeCondition(this.timeout));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, true))
          .Build()
        );
        stateWrapper.transitions.Add(transition);
      }
      return stateWrapper;
    }

    private StateWrapper BuildWait() {
      StateWrapper stateWrapper = new StateWrapper();
      FSMState state = new ActionState(this.waitStateName);
      state.WithDefaultBehaviours(this.gameObject);
      if (this.waitAnimationBehaviour != null) {
        state.AddStartBehaviour(this.waitAnimationBehaviour);
      }
      state.AddStartBehaviour(new StopMovementBehaviour(this.gameObject).WithXAxis().WithYAxis());
      state.AddStartBehaviour(new IgnoreGravityBehaviour(this.gameObject));
      state.AddStartBehaviour(new SetVariableBehaviour(this.gameObject, CharVars.IGNORE_DRAG.ToS(), false));
      if (this.flip) {
        state.AddStartBehaviour(BehavioursBuilder.BuildFlip(this.gameObject, false).Invert());
      }
      foreach (FSMBehaviour behaviour in this.customStartBehaviours) {
        state.AddStartBehaviour(behaviour);
      }
      foreach (FSMBehaviour behaviour in this.customUpdateBehaviours) {
        state.AddUpdateBehaviour(behaviour);
      }
      state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.gameObject, true, true));
      foreach (ComboWrapper combo in this.combos.Values) {
        SetVariableBehaviour behaviour = new SetVariableBehaviour(this.gameObject, CharVars.COMBO_VARIANT.ToS(), combo.variant);
        foreach (ICondition condition in combo.conditions) {
          behaviour = behaviour.WithCondition(condition);
        }
        state.AddUpdateBehaviour(behaviour);
      }
      state.AddExitBehaviour(new RestoreGravityBehaviour(this.gameObject, CharVars.ORIGINAL_GRAVITY_SCALE.ToS()));
      foreach (FSMBehaviour behaviour in this.customExitBehaviours) {
        state.AddExitBehaviour(behaviour);
      }
      stateWrapper.state = state;
      //TRANSITIONS
      FSMTransition transition = null;
      foreach (string animationName in this.animationNames) {
        transition = new FSMTransition(this.stateName, this.waitStateName);
        transition.AddCondition(new AnimationEndCondition(this.gameObject.GetComponent<Animator>(), animationName));
        stateWrapper.transitions.Add(transition);
      }
      //RETURN
      foreach (ComboWrapper combo in this.combos.Values) {
        transition = new FSMTransition(this.waitStateName, combo.toState);
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.COMBO_VARIANT, new IntCondition(CharVars.COMBO_VARIANT.ToS(), Operators.EQUAL, combo.variant))
          .Build()
        );
        stateWrapper.transitions.Add(transition);
      }
      transition = new FSMTransition(this.waitStateName, CharStates.FALL.ToS());
      transition.AddCondition(new TimeCondition(this.timeout));
      transition.AddConditions(
        new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
        .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, false))
        .Build()
      );
      stateWrapper.transitions.Add(transition);
      transition = new FSMTransition(this.waitStateName, CharStates.FLY.ToS());
      transition.AddCondition(new TimeCondition(this.timeout));
      transition.AddConditions(
        new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
        .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, true))
        .Build()
      );
      stateWrapper.transitions.Add(transition);
      return stateWrapper;
    }
  }
}