using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.FSMCore.Conditions;

namespace Characters {

  public class VarConditionsBuilder {
    public Dictionary<string, bool> conditionsToBuild = new Dictionary<string, bool>();
    public Dictionary<string, ICondition> varConditions = new Dictionary<string, ICondition>();

    public VarConditionsBuilder() {
      foreach (CharVar charVar in CharVars.AllForTransitions()) {
        this.conditionsToBuild[charVar.ToS()] = true;
      }
    }

    public VarConditionsBuilder IgnoringVars(params CharVar[] charVars) {
      foreach (CharVar charVar in charVars) {
        if (!this.conditionsToBuild.ContainsKey(charVar.name)) {
          throw new Exception("Trying to ignore variable " + charVar.name + ", but it is not a transition variable");
        }
        this.conditionsToBuild[charVar.name] = false;
      }
      return this;
    }

    public VarConditionsBuilder IgnoringAttributes() {
      foreach (CharVar charVar in CharVars.Attributes()) {
        if (charVar.useInTransitions) {
          this.conditionsToBuild[charVar.name] = false;
        }
      }
      return this;
    }

    public VarConditionsBuilder IgnoringDirections() {
      foreach (CharVar charVar in CharVars.Directions()) {
        if (charVar.useInTransitions) {
          this.conditionsToBuild[charVar.name] = false;
        }
      }
      return this;
    }

    public VarConditionsBuilder IgnoringPositions() {
      foreach (CharVar charVar in CharVars.Positions()) {
        if (charVar.useInTransitions) {
          this.conditionsToBuild[charVar.name] = false;
        }
      }
      return this;
    }

    public VarConditionsBuilder IgnoringFlags() {
      foreach (CharVar charVar in CharVars.Flags()) {
        if (charVar.useInTransitions) {
          this.conditionsToBuild[charVar.name] = false;
        }
      }
      return this;
    }

    public VarConditionsBuilder IgnoringInputs() {
      foreach (CharVar charVar in CharVars.Inputs()) {
        if (charVar.useInTransitions) {
          this.conditionsToBuild[charVar.name] = false;
        }
      }
      return this;
    }

    public VarConditionsBuilder IgnoringCombat() {
      foreach (CharVar charVar in CharVars.Combats()) {
        if (charVar.useInTransitions) {
          this.conditionsToBuild[charVar.name] = false;
        }
      }
      return this;
    }

    public VarConditionsBuilder WithVar(CharVar charVar, ICondition condition) {
      if (!this.conditionsToBuild.ContainsKey(charVar.name)) {
        throw new Exception("Trying to add a condition to the variable " + charVar.name + ", but it is not a transition variable");
      }
      this.conditionsToBuild[charVar.name] = true;
      this.varConditions[charVar.name] = condition;
      return this;
    }

    public List<ICondition> Build() {
      List<ICondition> conditions = new List<ICondition>();
      foreach (string charVar in this.conditionsToBuild.Keys) {
        if (this.conditionsToBuild[charVar]) {
          if (!this.varConditions.ContainsKey(charVar)) {
            Debug.LogError("Expected transition with key '" + charVar + "', but it was not present");
          }
          conditions.Add(this.varConditions[charVar]);
        }
      }
      return conditions;
    }

  }
}