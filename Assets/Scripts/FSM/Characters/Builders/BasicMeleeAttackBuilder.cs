using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;

namespace Characters {

  public class BasicMeleeAttackBuilder : IMeleeAttackBuilder {
    public bool fromCharge;

    public BasicMeleeAttackBuilder(GameObject gameObject, InputActions input) : base(gameObject, input) {
    }

    public BasicMeleeAttackBuilder FromCharge() {
      this.fromCharge = true;
      return this;
    }

    public override List<StateWrapper> Build() {
      List<StateWrapper> states = new List<StateWrapper>();
      states.Add(this.BuildAttack());
      return states;
    }

    private StateWrapper BuildAttack() {
      StateWrapper stateWrapper = new StateWrapper();
      FSMState state = new ActionState(this.stateName);
      state.WithDefaultBehaviours(this.gameObject);
      if (this.animationBehaviour != null) {
        state.AddStartBehaviour(this.animationBehaviour);
      }
      state.AddStartBehaviour(new SetVariableBehaviour(this.gameObject, CharVars.ATTACK_EFFECT.ToS(), (int) AttackEffects.NONE));
      if (this.stepForce != 0) {
        state.AddStartBehaviour(
          new ApplyForceBehaviour(this.gameObject)
          .WithDirection(new Vector2(1f, 0f))
          .WithForce(this.stepForce)
          .WithRelativeXDirection(CharVars.FACING_DIRECTION.ToS(), false)
        );
      }
      foreach (ComboWrapper combo in this.combos.Values) {
        SetVariableBehaviour behaviour = new SetVariableBehaviour(this.gameObject, CharVars.COMBO_VARIANT.ToS(), combo.variant);
        foreach (ICondition condition in combo.conditions) {
          behaviour = behaviour.WithCondition(condition);
        }
        state.AddUpdateBehaviour(behaviour);
      }
      foreach (FSMBehaviour behaviour in this.customStartBehaviours) {
        state.AddStartBehaviour(behaviour);
      }
      foreach (FSMBehaviour behaviour in this.customUpdateBehaviours) {
        state.AddUpdateBehaviour(behaviour);
      }
      foreach (FSMBehaviour behaviour in this.customExitBehaviours) {
        state.AddExitBehaviour(behaviour);
      }
      stateWrapper.state = state;
      FSMTransition transition = null;
      foreach (string startingState in this.startingStates) {
        transition = new FSMTransition(startingState, this.stateName);
        VarConditionsBuilder conditions = new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat();
        if (this.fromCharge) {
          conditions.WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.RELEASE, InputModes.NONE));
        } else {
          conditions.WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP));
        }
        foreach (CharVar charVar in this.customTransitions.Keys) {
          conditions.WithVar(charVar, this.customTransitions[charVar]);
        }
        transition.AddConditions(conditions.Build());
        stateWrapper.transitions.Add(transition);
      }
      // // RETURN
      foreach (string animationName in this.animationNames) {
        foreach (ComboWrapper combo in this.combos.Values) {
          transition = new FSMTransition(this.stateName, combo.toState);
          transition.AddCondition(new AnimationEndCondition(this.gameObject.GetComponent<Animator>(), animationName));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.COMBO_VARIANT, new IntCondition(CharVars.COMBO_VARIANT.ToS(), Operators.EQUAL, combo.variant))
            .Build()
          );
          stateWrapper.transitions.Add(transition);
        }
        transition = new FSMTransition(this.stateName, CharStates.IDLE.ToS());
        transition.AddCondition(new AnimationEndCondition(this.gameObject.GetComponent<Animator>(), animationName));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, false))
          .Build()
        );
        stateWrapper.transitions.Add(transition);
        transition = new FSMTransition(this.stateName, CharStates.FLY.ToS());
        transition.AddCondition(new AnimationEndCondition(this.gameObject.GetComponent<Animator>(), animationName));
        transition.AddConditions(
          new VarConditionsBuilder()
          .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
          .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, true))
          .Build()
        );
        stateWrapper.transitions.Add(transition);
      }
      return stateWrapper;
    }

  }
}