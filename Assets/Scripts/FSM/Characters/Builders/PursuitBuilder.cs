using UnityEngine;
using System.Collections.Generic;
using TwoDEngine.Inputs;
using TwoDEngine.FSMCore;
using TwoDEngine.FSMCore.Behaviours;
using TwoDEngine.FSMCore.Conditions;

namespace Characters {

  public class PursuitBuilder {
    public GameObject gameObject;
    public InputActions input;

    public string stateName;
    public PlayAnimationBehaviour animationBehaviour;
    public float speedIncrease;
    public float timeout;
    public string[] startingStates = new string[0];
    public List<string> animationNames = new List<string>();

    public FSMBehaviour[] customStartBehaviours = new FSMBehaviour[0];
    public FSMBehaviour[] customUpdateBehaviours = new FSMBehaviour[0];
    public FSMBehaviour[] customExitBehaviours = new FSMBehaviour[0];

    public GameObject target;
    public string targetVariable;
    public Dictionary<int, ComboWrapper> combos = new Dictionary<int, ComboWrapper>();

    public CharVar counterVariable;
    public int maxPursuits;

    public PursuitBuilder(GameObject gameObject, InputActions input, GameObject target, float timeout) {
      this.gameObject = gameObject;
      this.input = input;
      this.target = target;
      this.timeout = timeout;
    }

    public PursuitBuilder(GameObject gameObject, InputActions input, string targetVariable, float timeout) {
      this.gameObject = gameObject;
      this.input = input;
      this.targetVariable = targetVariable;
      this.timeout = timeout;
    }

    public PursuitBuilder WithState(string stateName) {
      this.stateName = stateName;
      return this;
    }

    public PursuitBuilder WithSpeedMultiplierIncrease(float speedIncrease) {
      this.speedIncrease = speedIncrease;
      return this;
    }

    public PursuitBuilder WithStartBehaviours(params FSMBehaviour[] behaviours) {
      this.customStartBehaviours = behaviours;
      return this;
    }

    public PursuitBuilder WithUpdateBehaviours(params FSMBehaviour[] behaviours) {
      this.customUpdateBehaviours = behaviours;
      return this;
    }

    public PursuitBuilder WithExitBehaviours(params FSMBehaviour[] behaviours) {
      this.customExitBehaviours = behaviours;
      return this;
    }

    public PursuitBuilder WithAnimation(PlayAnimationBehaviour animationBehaviour) {
      this.animationBehaviour = animationBehaviour;
      this.animationNames.Add(animationBehaviour.defaultAnimation);
      foreach (string altAnimation in animationBehaviour.altAnimation.Keys) {
        this.animationNames.Add(altAnimation);
      }
      return this;
    }

    public PursuitBuilder WithStartingStates(params string[] startingStates) {
      this.startingStates = startingStates;
      return this;
    }

    public virtual PursuitBuilder WithCombo(ComboWrapper comboWrapper) {
      this.combos[comboWrapper.variant] = comboWrapper;
      return this;
    }

    public virtual PursuitBuilder WithMaxPursuits(CharVar counterVariable, int maxPursuits) {
      this.counterVariable = counterVariable;
      this.maxPursuits = maxPursuits;
      return this;
    }

    public List<StateWrapper> Build() {
      List<StateWrapper> states = new List<StateWrapper>();
      states.Add(this.BuildPursuit());
      return states;
    }

    private StateWrapper BuildPursuit() {
      StateWrapper stateWrapper = new StateWrapper();
      FSMState state = new ActionState(this.stateName);
      state.WithDefaultBehaviours(this.gameObject);
      if (this.animationBehaviour != null) {
        state.AddStartBehaviour(this.animationBehaviour);
      }
      state.AddStartBehaviour(new VariableCalculatorBehaviour(this.gameObject, CharVars.SPEED_MULTIPLIER.ToS()).Add(this.speedIncrease));
      if (this.counterVariable != null) {
        state.AddStartBehaviour(new VariableCalculatorBehaviour(this.gameObject, this.counterVariable.ToS()).Add(1));
      }
      state.AddStartBehaviour(new SetVariableBehaviour(this.gameObject, CharVars.IGNORE_DRAG.ToS(), true));
      foreach (FSMBehaviour behaviour in this.customStartBehaviours) {
        state.AddStartBehaviour(behaviour);
      }
      foreach (FSMBehaviour behaviour in this.customUpdateBehaviours) {
        state.AddUpdateBehaviour(behaviour);
      }
      MoveTowardsBehaviour moveTowardsBehaviour = BehavioursBuilder.BuildMoveTowards(this.gameObject, true, true);
      if (this.target != null) {
        moveTowardsBehaviour = moveTowardsBehaviour.WithTarget(this.target);
      }
      if (this.targetVariable != null) {
        moveTowardsBehaviour = moveTowardsBehaviour.WithTargetVariable(this.targetVariable);
      }
      state.AddUpdateBehaviour(moveTowardsBehaviour);
      state.AddUpdateBehaviour(BehavioursBuilder.BuildKeepInsideBounds(this.gameObject, true, true));
      foreach (ComboWrapper combo in this.combos.Values) {
        SetVariableBehaviour behaviour = new SetVariableBehaviour(this.gameObject, CharVars.COMBO_VARIANT.ToS(), combo.variant);
        foreach (ICondition condition in combo.conditions) {
          behaviour = behaviour.WithCondition(condition);
        }
        state.AddUpdateBehaviour(behaviour);
      }
      state.AddExitBehaviour(new VariableCalculatorBehaviour(this.gameObject, CharVars.SPEED_MULTIPLIER.ToS()).Subtract(this.speedIncrease));
      state.AddExitBehaviour(new SetVariableBehaviour(this.gameObject, CharVars.IGNORE_DRAG.ToS(), false));
      foreach (FSMBehaviour behaviour in this.customExitBehaviours) {
        state.AddExitBehaviour(behaviour);
      }
      stateWrapper.state = state;
      //TRANSITIONS
      FSMTransition transition = null;
      foreach (string startingState in this.startingStates) {
        transition = new FSMTransition(startingState, this.stateName);
        VarConditionsBuilder builder = new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
        .WithVar(CharVars.FromInputMode(this.input), new InputCondition(CharVars.FromInputMode(this.input).ToS(), Operators.EQUAL, InputModes.PRESS, InputModes.DOUBLE_TAP))
        .WithVar(CharVars.TARGET, new ObjectCondition(this.targetVariable));
        if (this.counterVariable != null) {
          builder = builder.WithVar(this.counterVariable, new IntCondition(this.counterVariable.ToS(), Operators.LESS_THAN, this.maxPursuits));
        }
        transition.AddConditions(builder.Build());
        stateWrapper.transitions.Add(transition);
      }
      //RETURN
      foreach (string animationName in this.animationNames) {
        foreach (ComboWrapper combo in this.combos.Values) {
          transition = new FSMTransition(this.stateName, combo.toState);
          transition.AddCondition(new AnimationEndCondition(this.gameObject.GetComponent<Animator>(), animationName));
          transition.AddConditions(
            new VarConditionsBuilder()
            .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
            .WithVar(CharVars.COMBO_VARIANT, new IntCondition(CharVars.COMBO_VARIANT.ToS(), Operators.EQUAL, combo.variant))
            .Build()
          );
          stateWrapper.transitions.Add(transition);
        }
      }
      transition = new FSMTransition(this.stateName, CharStates.FALL.ToS());
      transition.AddCondition(new TimeCondition(this.timeout));
      transition.AddConditions(
        new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
        .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, false))
        .Build()
      );
      stateWrapper.transitions.Add(transition);
      transition = new FSMTransition(this.stateName, CharStates.FLY.ToS());
      transition.AddCondition(new TimeCondition(this.timeout));
      transition.AddConditions(
        new VarConditionsBuilder()
        .IgnoringInputs().IgnoringAttributes().IgnoringDirections().IgnoringPositions().IgnoringFlags().IgnoringCombat()
        .WithVar(CharVars.IS_FLYING, new BoolCondition(CharVars.IS_FLYING.ToS(), Operators.EQUAL, true))
        .Build()
      );
      stateWrapper.transitions.Add(transition);
      return stateWrapper;
    }
  }
}