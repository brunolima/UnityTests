using UnityEngine;

namespace Characters {

  public class DefenseController : MonoBehaviour {
    [HideInInspector]
    public CharController controller;

    public delegate void ReceiveAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce, bool blocked);
    public event ReceiveAttack OnReceiveAttack;

    public delegate void BlockAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce);
    public event BlockAttack OnBlockAttack;


    void Awake() {
      if (this.controller == null) {
        this.controller = this.GetComponent<CharController>();
      }
    }

    public void GetHit(System.Object[] parameters) {
      bool isBlocking = (bool)this.controller.fsm.GetVariable(CharVars.IS_BLOCKING.ToS());
      float damage = (float) parameters[0];
      GameObject attacker = (GameObject) parameters[1];
      AttackEffects effect = (AttackEffects) parameters[2];
      float power = (float) parameters[3];
      Vector2 effectDirection = (Vector2) parameters[4];
      float effectForce = (float) parameters[5];
      FacingDirections attackDirection = CalculateAttackDirection(attacker.transform);
      bool blocked = isBlocking && IsFrontAttack(attacker);
      this.SendReceiveAttack(damage, attacker, attackDirection, effect, power, effectDirection, effectForce, blocked);
    }

    public void SendReceiveAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce, bool blocked) {
      if (OnReceiveAttack != null) {
        this.OnReceiveAttack(damage, attacker, attackDirection, effect, power, effectDirection, effectForce, blocked);
      }
    }

    public void SendBlockAttack(float damage, GameObject attacker, FacingDirections attackDirection, AttackEffects effect, float power, Vector2 effectDirection, float effectForce) {
      if (OnBlockAttack != null) {
        this.OnBlockAttack(damage, attacker, attackDirection, effect, power, effectDirection, effectForce);
      }
    }

    public bool IsFrontAttack(GameObject attacker) {
      return (attacker.transform.localScale.x > 0 && this.gameObject.transform.localScale.x < 0) || (attacker.transform.localScale.x < 0 && this.gameObject.transform.localScale.x > 0);
    }

    public FacingDirections CalculateAttackDirection(Transform attackerTransform) {
      return attackerTransform.localScale.x > 0 ? FacingDirections.LEFT : FacingDirections.RIGHT;
    }

    public void SendSuccessMessage(GameObject attacker, AttackEffects effect) {
      attacker.BroadcastMessage("AttackSuccess", new System.Object[] { this.gameObject, (int)effect});
    }

    public void SendBlockMessage(GameObject attacker, AttackEffects effect) {
      attacker.BroadcastMessage("AttackBlock", new System.Object[] { this.gameObject, (int)effect});
    }


  }

}
