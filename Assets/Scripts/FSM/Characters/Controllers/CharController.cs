using UnityEngine;
using System;
using System.Collections.Generic;
using TwoDEngine.Inputs;
using TwoDEngine.FSMCore;

namespace Characters {

  public class CharController : FiniteStateMachineController {
    [Header("Movement")]
    public float movementSpeed = 10f;
    public float movementForce = 100f;
    [Range(0.0f, 1.0f)]
    public float drag = 0.5f;
    [Range(0.0f, 1.0f)]
    public float slideDrag = 0.9f;
    public FacingDirections facingDirection = FacingDirections.RIGHT;
    public float jumpForce = 400f;

    [Header("Collisions")]
    public float hitRadius = 0.2f;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public LayerMask whatIsCeiling;
    public Transform ceilingCheck;
    public Vector3 ceilingCheckOffset = new Vector3(0, 0.2f, 0);
    public LayerMask whatIsWall;
    public Transform wallCheck;
    public Transform[] actionChecks;
    public LayerMask whatIsEnemy;

    public LayerMask whatIsObstacle;
    public Collider2D bodyCollider;

    [HideInInspector]
    public InputHandler inputHandler;
    [HideInInspector]
    public Rigidbody2D rigidBody;

    void Awake() {
      if (this.inputHandler == null) {
        this.inputHandler = this.transform.parent.GetComponent<InputHandler>();
      }
      this.rigidBody = this.GetComponent<Rigidbody2D>();
    }

    public override void OnEnable() {
      base.OnEnable();
      this.inputHandler.OnUserInput += this.OnUserInput;
      this.rigidBody.drag = 0;
      foreach (CharVar charVar in CharVars.AllDefault()) {
        this.fsm.UpdateVariable(charVar.ToS(), charVar.defaultValue);
      }
      this.fsm.UpdateVariable(CharVars.FACING_DIRECTION.ToS(), this.facingDirection);
      this.fsm.UpdateVariable(CharVars.ORIGINAL_GRAVITY_SCALE.ToS(), this.rigidBody.gravityScale);
    }

    public override void OnDisable() {
      base.OnDisable();
      this.inputHandler.OnUserInput -= this.OnUserInput;
    }

    void FixedUpdate() {
      if (!(bool)this.fsm.GetVariable(CharVars.IGNORE_DRAG.ToS())) {
        float realDrag = this.drag;
        if ((bool)this.fsm.GetVariable(CharVars.IS_SLIDING.ToS())) {
          realDrag = this.slideDrag;
        }
        Vector3 velocity = this.rigidBody.velocity;
        velocity.x *= (1 - realDrag);
        if ((bool)this.fsm.GetVariable(CharVars.IS_FLYING.ToS())) {
          velocity.y *= (1 - realDrag);
        }
        this.rigidBody.velocity = velocity;
      }
      if (this.gameObject.name == "Goku(Clone)_2" && Mathf.Abs(this.rigidBody.velocity.x) > 11) {
        Debug.Log(this.fsm.GetVariable(CharVars.IGNORE_DRAG.ToS()) + " " +  this.rigidBody.velocity);
        throw new Exception("PERAÍ, TIO");
      }
    }

    public override void CustomUpdate() {
      this.EnsureValues();
    }

    #region SET_VALUES
    void OnUserInput(Dictionary<InputActions, PlayerInput> inputs) {
      foreach (InputActions action in inputs.Keys) {
        CharVar modeVar = CharVars.FromInputMode(action);
        if (modeVar != null) {
          this.fsm.UpdateVariable(modeVar.ToS(), inputs[action].Mode);
        }
        CharVar valueVar = CharVars.FromInputValue(action);
        if (valueVar != null) {
          this.fsm.UpdateVariable(valueVar.ToS(), inputs[action].Value);
        }
      }
    }

    public void SetMinPosition(Vector2 position) {
      this.fsm.UpdateVariable(CharVars.BOUNDS_MIN_POSITION.ToS(), position);
    }

    public void SetMaxPosition(Vector2 position) {
      this.fsm.UpdateVariable(CharVars.BOUNDS_MAX_POSITION.ToS(), position);
    }

    void EnsureValues() {
      this.fsm.UpdateVariable(CharVars.MOVE_SPEED.ToS(), this.movementSpeed);
      this.fsm.UpdateVariable(CharVars.MOVE_FORCE.ToS(), this.movementForce);
      this.fsm.UpdateVariable(CharVars.JUMP_FORCE.ToS(), this.jumpForce);
      this.fsm.UpdateVariable(CharVars.HORIZONTAL_SPEED.ToS(), this.rigidBody.velocity.x);
      this.fsm.UpdateVariable(CharVars.VERTICAL_SPEED.ToS(), this.rigidBody.velocity.y);
      this.fsm.UpdateVariable(CharVars.HIT_GROUND.ToS(), (bool) Physics2D.Linecast(this.transform.position, this.groundCheck.position, this.whatIsGround));
      this.fsm.UpdateVariable(CharVars.HIT_CEILING.ToS(), (bool) Physics2D.Linecast(this.transform.position + this.ceilingCheckOffset, this.ceilingCheck.position, this.whatIsCeiling));
      RaycastHit2D wallHit = Physics2D.Linecast(this.transform.position, this.wallCheck.position, this.whatIsWall);
      if (wallHit) {
        if (wallHit.transform.position.x > this.transform.position.x) {
          this.fsm.UpdateVariable(CharVars.WALL_DIRECTION.ToS(), FacingDirections.RIGHT);
        } else {
          this.fsm.UpdateVariable(CharVars.WALL_DIRECTION.ToS(), FacingDirections.LEFT);
        }
      }
      this.fsm.UpdateVariable(CharVars.HIT_WALL.ToS(), (bool) wallHit);
      this.fsm.UpdateVariable(CharVars.ACTION_CHECKS.ToS(), this.actionChecks);
      this.fsm.UpdateVariable(CharVars.ENEMY_LAYER.ToS(), this.whatIsEnemy);
    }

    void OnCollisionEnter2D(Collision2D collision) {
      if (Tools.CollidedWithMask(collision, this.bodyCollider, whatIsObstacle)) {
        this.fsm.UpdateVariable(CharVars.HIT_OBSTACLE.ToS(), true);
        if (collision.gameObject.transform.position.x > this.transform.position.x) {
          this.fsm.UpdateVariable(CharVars.OBSTACLE_DIRECTION.ToS(), (int)FacingDirections.RIGHT);
        } else {
          this.fsm.UpdateVariable(CharVars.OBSTACLE_DIRECTION.ToS(), (int)FacingDirections.LEFT);
        }
      }
    }

    void OnCollisionStay2D(Collision2D collision) {
      if (Tools.CollidedWithMask(collision, this.bodyCollider, whatIsObstacle)) {
        this.fsm.UpdateVariable(CharVars.HIT_OBSTACLE.ToS(), true);
      }
    }

    void OnCollisionExit2D(Collision2D collision) {
      if (Tools.IsInLayerMask(collision.gameObject, whatIsObstacle)) {
        this.fsm.UpdateVariable(CharVars.HIT_OBSTACLE.ToS(), false);
      }
    }

    #endregion
  }

}