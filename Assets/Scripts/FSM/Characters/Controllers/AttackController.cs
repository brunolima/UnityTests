using UnityEngine;

namespace Characters {

  public class AttackController : MonoBehaviour {
    [HideInInspector]
    public CharController controller;

    void Awake() {
      if (this.controller == null) {
        this.controller = this.GetComponent<CharController>();
      }
    }

    void AttackSuccess(System.Object[] parameters) {
      GameObject target = (GameObject) parameters[0];
      AttackEffects effect = (AttackEffects) parameters[1];
      this.controller.fsm.UpdateVariable(CharVars.ATTACK_EFFECT.ToS(), effect);
      this.controller.fsm.UpdateVariable(CharVars.TARGET.ToS(), target);
    }

  }

}
