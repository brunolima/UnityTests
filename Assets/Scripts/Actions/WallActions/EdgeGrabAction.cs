using UnityEngine;
// using MyTests.CharacterStates;

namespace MyTests {
  namespace Actions {
    namespace WallActions {
      // public class EdgeGrabAction : BaseAction {

      // public LayerMask whatIsEdge;
      // public Transform edgeCheck;
      // public float climbForce = 300f;

      // private bool inEdge = false;
      // private bool checkEdge = true;
      // public float hitRadius = 0.2f;

      // private Vector3 closestEdgePoint;

      // void Update(){
      //   if(this.stateMachine.CurrentStateIs(PlayerStates.EDGE_HANGING)){
      //     this.controller.IgnoreGravity();
      //     this.rigidBody.velocity = new Vector2(0f,0f);
      //   }

      // }

      // void FixedUpdate(){
      //   if(this.edgeCheck != null){
      //     BoxCollider2D collider  = (BoxCollider2D)Physics2D.OverlapCircle (this.edgeCheck.position, this.hitRadius, this.whatIsEdge);
      //     this.inEdge = collider;
      //     if(this.checkEdge && this.inEdge){
      //       this.closestEdgePoint = collider.bounds.ClosestPoint(this.edgeCheck.position);
      //       this.stateMachine.TransitionTo(PlayerStates.EDGE_HANGING);
      //     } else if(!this.inEdge || this.inputs[InputActions.Vertical].Value > 0f){
      //       this.checkEdge = true;
      //     }
      //   }

      //   if(this.stateMachine.CurrentStateIs(PlayerStates.EDGE_CLIMBING) && this.controller.hitGround && this.rigidBody.velocity.y <= 0f){
      //     this.stateMachine.TransitionTo(PlayerStates.IDLE);
      //   }
      //   if(this.stateMachine.CurrentStateIs(PlayerStates.EDGE_HANGING)){
      //     this.CheckInput();
      //   }
      // }

      // public override void OnStateTransition(PlayerStates fromState, PlayerStates toState, float hSpeed, float vSpeed) {
      //   if(fromState == PlayerStates.EDGE_HANGING){
      //     this.controller.RestoreGravity();
      //     this.inEdge = false;
      //   } else if(fromState == PlayerStates.EDGE_CLIMBING){
      //     this.controller.UpdateNonTriggerColliders(false);
      //   }
      //   if(toState == PlayerStates.EDGE_HANGING){
      //     this.transform.position = Vector2.MoveTowards(this.transform.position, this.transform.position + this.closestEdgePoint - this.edgeCheck.position, 100 * Time.deltaTime);
      //   } else if(toState == PlayerStates.EDGE_CLIMBING){
      //     this.Climb();
      //   }
      // }

      // public override void CheckInput(){
      //   this.OnHorizontalUserInput(this.inputs[InputActions.Horizontal].Mode, this.inputs[InputActions.Horizontal].Value);
      //   this.OnVerticalUserInput(this.inputs[InputActions.Vertical].Mode, this.inputs[InputActions.Vertical].Value);
      // }

      // void OnHorizontalUserInput(InputModes mode, float value){
      //   if( this.InputModeIs(mode, InputModes.HOLD) && ((value < 0 && this.controller.facingDirection == FacingDirections.RIGHT) || (value > 0 && this.controller.facingDirection == FacingDirections.LEFT))){
      //     this.stateMachine.TransitionTo(PlayerStates.FALLING);
      //   }
      // }

      // void Climb(){
      //   this.controller.UpdateNonTriggerColliders(true);
      //   Vector2 force = new Vector2((int)this.controller.facingDirection * 0.1f,1.1f);
      //   this.rigidBody.AddForce( force * this.climbForce);
      // }

      // void OnVerticalUserInput(InputModes mode, float value){
      //   if(this.InputModeIs(mode, InputModes.PRESS, InputModes.DOUBLE_TAP)){
      //     if(this.stateMachine.CurrentStateIs(PlayerStates.EDGE_HANGING) && value < 0){
      //       this.checkEdge = false;
      //       this.stateMachine.TransitionTo(PlayerStates.FALLING);
      //     } else if(this.stateMachine.CurrentStateIs(PlayerStates.EDGE_HANGING) && value > 0){
      //       this.stateMachine.TransitionTo(PlayerStates.EDGE_CLIMBING);
      //     }
      //   }
      // }

      // }
    }
  }
}