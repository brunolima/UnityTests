using UnityEngine;
// using MyTests.CharacterStates;

namespace MyTests {
  namespace Actions {
    namespace WallActions {
      // public class WallClimbAction : BaseAction {

      // public LayerMask whatIsWall;
      // public Transform wallCheck;
      // public float hitRadius = 0.1f;

      // public float climbSpeed = 5f;
      // public float climbForce = 200f;


      // void FixedUpdate(){

      //   if(this.stateMachine.CurrentStateIs(PlayerStates.IN_WALL, PlayerStates.WALL_CLIMBING) && this.controller.hitGround){
      //     this.stateMachine.TransitionTo(PlayerStates.IDLE);
      //   }

      //   if(Physics2D.OverlapCircle (this.wallCheck.position, this.hitRadius, this.whatIsWall) && !this.controller.hitGround){
      //     if(!this.stateMachine.CurrentStateIs(PlayerStates.IN_WALL, PlayerStates.WALL_CLIMBING)){
      //       this.stateMachine.TransitionTo(PlayerStates.IN_WALL);
      //     }
      //   } else if(this.stateMachine.CurrentStateIs(PlayerStates.IN_WALL, PlayerStates.WALL_CLIMBING)){
      //     this.stateMachine.TransitionTo(PlayerStates.FALLING);
      //   }

      //   this.CheckInput();
      // }

      // public override void OnStateTransition(PlayerStates fromState, PlayerStates toState, float hSpeed, float vSpeed) {
      //   if(fromState == PlayerStates.IN_WALL || fromState == PlayerStates.WALL_CLIMBING){
      //     this.controller.verticalMovementEnabled = false;
      //     this.controller.RestoreGravity();
      //     this.controller.RestoreLinearDrag();
      //     this.rigidBody.drag = this.controller.originalLinearDrag;
      //     this.rigidBody.velocity = new Vector2(0f,0f);
      //   }
      //   if(toState == PlayerStates.IN_WALL || toState == PlayerStates.WALL_CLIMBING){
      //     this.controller.verticalMovementEnabled = true;
      //     this.controller.IgnoreGravity();
      //     this.controller.SetLinearDrag(10f);
      //     this.rigidBody.velocity = new Vector2(0f,0f);
      //   }
      // }

      // public override void CheckInput(){
      //   this.OnHorizontalUserInput(this.inputs[InputActions.Horizontal].Mode, this.inputs[InputActions.Horizontal].Value);
      //   this.OnVerticalUserInput(this.inputs[InputActions.Vertical].Mode, this.inputs[InputActions.Vertical].Value);
      // }

      // void OnHorizontalUserInput(InputModes mode, float value){
      //   if( this.InputModeIs(mode, InputModes.HOLD) && ((value < 0 && this.controller.facingDirection == FacingDirections.RIGHT) || (value > 0 && this.controller.facingDirection == FacingDirections.LEFT))){
      //     this.stateMachine.TransitionTo(PlayerStates.FALLING);
      //   }
      // }

      // void OnVerticalUserInput(InputModes mode, float value){
      //   if(this.InputModeIs(mode, InputModes.PRESS, InputModes.HOLD)){
      //     if(this.stateMachine.CurrentStateIs(PlayerStates.WALL_CLIMBING)){
      //       this.controller.MoveV(value, this.climbSpeed, this.climbForce);
      //     } else {
      //       this.stateMachine.TransitionTo(PlayerStates.WALL_CLIMBING);
      //     }
      //   } else if(this.InputModeIs(mode, InputModes.NONE) && this.controller.StoppedMovingV(value) && this.stateMachine.CurrentStateIs(PlayerStates.WALL_CLIMBING)){
      //     this.stateMachine.TransitionTo(PlayerStates.IN_WALL);
      //   }
      // }


      // }

    }
  }
}