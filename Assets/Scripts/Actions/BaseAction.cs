﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
// using MyTests.CharacterStates;

namespace MyTests {
  namespace Actions {
    // public abstract class BaseAction : MonoBehaviour {

    /*      protected InputHandler inputHandler;
          protected CharacterStateMachine stateMachine;
          protected Rigidbody2D rigidBody;
          protected PlayerController controller;

          protected Dictionary<InputActions, PlayerInput> inputs;

          public void Awake() {
            this.inputHandler = this.transform.parent.GetComponent<InputHandler>();
            this.stateMachine = this.transform.parent.GetComponent<CharacterStateMachine>();
            this.rigidBody = this.GetComponent<Rigidbody2D>();
            this.controller = this.GetComponent<PlayerController>();
          }

          public void OnEnable() {
            this.inputHandler.OnUserInput += this.OnUserInput;
            this.stateMachine.OnStateTransition += this.OnStateTransition;

            this.inputs = new Dictionary<InputActions, PlayerInput>();
            this.ClearInputs();
          }

          public void OnDisable() {
            this.inputHandler.OnUserInput -= this.OnUserInput;
            this.stateMachine.OnStateTransition -= this.OnStateTransition;
          }

          public void ClearInputs(){
            foreach (InputActions action in System.Enum.GetValues(typeof(InputActions))) {
              this.inputs[action] = new PlayerInput(new InputMapping());
            }
          }

          public void SetNextState(InputActions key, PlayerStates state){
            this.stateMachine.SetNextState(key.ToString(), state);
          }

          public bool HasNextState(InputActions key){
            return this.stateMachine.HasNextState(key.ToString());
          }

          public PlayerStates GetNextState(InputActions key){
            return this.stateMachine.GetNextState(key.ToString());
          }

          public bool NextStateIs(InputActions key, PlayerStates state){
            return !this.HasNextState(key) || this.stateMachine.NextStateIs(key.ToString(),state);
          }

          public abstract void CheckInput();

          public virtual void OnUserInput(Dictionary<InputActions, PlayerInput> inputs){
            this.inputs = inputs;
          }

          public virtual void OnStateTransition(PlayerStates fromState, PlayerStates toState, float hSpeed, float vSpeed) {
          }

          public bool InputModeIs(InputModes mode, params InputModes[] modes){
            return Array.Exists(modes, element => element == mode);
          }*/

    // }
  }
}