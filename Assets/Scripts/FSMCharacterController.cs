using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MyTests {
  // public enum FacingDirections {LEFT = -1, RIGHT = 1}
  // public enum MovementModes {APPLY_FORCE, UPDATE_POSITION};

  public class FSMCharacterController : MonoBehaviour {

    // public MovementModes movementMode;
    // public FacingDirections facingDirection = FacingDirections.RIGHT;
    // public LayerMask whatIsObstacle;
    // public Collider2D bodyCollider;

    // public LayerMask whatIsGround;
    // public Transform groundCheck;
    // public LayerMask whatIsCeiling;
    // public Transform ceilingCheck;


    // public float hitRadius = 0.2f;
    // [HideInInspector]
    // public bool hitObstacle = false;
    // [HideInInspector]
    // public bool hitGround = false;
    // [HideInInspector]
    // public bool hitCeiling = false;
    // [HideInInspector]
    // public FacingDirections obstacleDirection = FacingDirections.RIGHT;


    // [HideInInspector]
    // public float originalLinearDrag;
    // [HideInInspector]
    // public float originalGravityScale;
    // [HideInInspector]
    // public List<float> gravityScaleUpdates = new List<float>();
    // [HideInInspector]
    // public List<float> linearDragUpdates = new List<float>();
    // [HideInInspector]
    // public List<float> speedMultiplierUpdates = new List<float>();

    // [HideInInspector]
    // public Dictionary<string, float> charges = new Dictionary<string, float>();

    // // [HideInInspector]
    // // public bool animationLocked = false;
    // // // private List<Collider2D> nonTriggerColliders = new List<Collider2D>();

    // // [HideInInspector]
    // // public Dictionary<ConditionFields, object> transitionParameters = new Dictionary<ConditionFields, object>();

    // // [HideInInspector]
    // // public Dictionary<InputActions, PlayerInput> inputs;
    // // private InputHandler inputHandler;
    // // [HideInInspector]
    // // private CharacterStateMachine stateMachine;

    // // public Animator anim;
    // [HideInInspector]
    // public Rigidbody2D rigidBody;

    // private float movementSpeed = 1f;
    // private float movementForce = 1f;
    // private float speedMultiplier = 1f;


    // void Awake() {
    //   this.rigidBody = this.GetComponent<Rigidbody2D>();
    //   this.originalLinearDrag = this.rigidBody.drag;
    //   this.originalGravityScale = this.rigidBody.gravityScale;
    //   // foreach(Collider2D c in this.GetComponents<Collider2D> ()) {
    //   //   if(c.isTrigger == false){
    //   //     this.nonTriggerColliders.Add(c);
    //   //   }
    //   // }
    // }

    // void FixedUpdate() {
    //   this.CheckCollisions();
    //   this.UpdateValues();
    // }

    // #region Collisions
    // void OnCollisionEnter2D(Collision2D collision) {
    //   if (Tools.CollidedWithMask(collision, this.bodyCollider, whatIsObstacle)) {
    //     this.hitObstacle = true;
    //     if (collision.gameObject.transform.position.x > this.transform.position.x) {
    //       this.obstacleDirection = FacingDirections.RIGHT;
    //     } else {
    //       this.obstacleDirection = FacingDirections.LEFT;
    //     }
    //   }
    // }

    // void OnCollisionStay2D(Collision2D collision) {
    //   if (Tools.CollidedWithMask(collision, this.bodyCollider, whatIsObstacle)) {
    //     this.hitObstacle = true;
    //   }
    // }

    // void OnCollisionExit2D(Collision2D collision) {
    //   if (Tools.IsInLayerMask(collision.gameObject, whatIsObstacle)) {
    //     this.hitObstacle = false;
    //   }
    // }

    // void OnTriggerExit2D(Collider2D collider) {
    //   if (Tools.IsInLayerMask(collider.gameObject, whatIsObstacle)) {
    //     this.hitObstacle = false;
    //   }
    // }

    // void CheckCollisions() {
    //   this.hitGround = Physics2D.OverlapCircle(this.groundCheck.position, this.hitRadius, this.whatIsGround);
    //   this.hitCeiling = Physics2D.OverlapCircle(this.ceilingCheck.position, this.hitRadius, this.whatIsCeiling);
    // }
    // #endregion

    // #region Movement
    // public void Move(float hInputValue, float vInputValue, float speed, float moveForce = 0) {
    //   this.movementSpeed = speed;
    //   this.movementForce = moveForce;
    //   if (hInputValue != 0f) {
    //     this.MoveH(hInputValue);
    //   }
    //   if (vInputValue != 0f) {
    //     this.MoveV(vInputValue);
    //   }
    //   if ((hInputValue > 0 && this.facingDirection == FacingDirections.LEFT) || (hInputValue < 0 && this.facingDirection == FacingDirections.RIGHT)) {
    //     this.Flip();
    //   }
    // }

    // public void Move(float hInputValue, float vInputValue) {
    //   if (hInputValue != 0f) {
    //     this.MoveH(hInputValue);
    //   }
    //   if (vInputValue != 0f) {
    //     this.MoveV(vInputValue);
    //   }
    //   if ((hInputValue > 0 && this.facingDirection == FacingDirections.LEFT) || (hInputValue < 0 && this.facingDirection == FacingDirections.RIGHT)) {
    //     this.Flip();
    //   }
    // }

    // private void MoveH(float inputValue) {
    //   if (this.hitObstacle && this.IsMovingTo(this.obstacleDirection, inputValue)) {
    //     return;
    //   }
    //   switch (this.movementMode) {
    //   case MovementModes.APPLY_FORCE:
    //     if (inputValue * this.rigidBody.velocity.x < this.movementSpeed) {
    //       this.rigidBody.AddForce(Vector2.right * inputValue * this.movementForce);
    //     }
    //     if (Mathf.Abs(this.rigidBody.velocity.x) > this.movementSpeed) {
    //       this.rigidBody.velocity = new Vector2(Mathf.Sign(this.rigidBody.velocity.x) * this.movementSpeed * this.speedMultiplier, this.rigidBody.velocity.y);
    //     }
    //     break;
    //   case MovementModes.UPDATE_POSITION:
    //     Vector3 currentPosition = this.transform.position;
    //     Vector3 target = Vector3.right * inputValue * this.movementSpeed * this.speedMultiplier + currentPosition;
    //     this.transform.position = Vector3.Lerp(currentPosition, target, Time.deltaTime);
    //     this.rigidBody.velocity = new Vector2(0f, this.rigidBody.velocity.y);
    //     break;
    //   }
    // }

    // private void MoveV(float inputValue) {
    //   if ((this.hitGround && inputValue < 0) || (this.hitCeiling && inputValue > 0)) {
    //     return;
    //   }
    //   switch (this.movementMode) {
    //   case MovementModes.APPLY_FORCE:
    //     if (inputValue * this.rigidBody.velocity.y < this.movementSpeed) {
    //       this.rigidBody.AddForce(Vector2.up * inputValue * this.movementForce);
    //     }
    //     if (Mathf.Abs(this.rigidBody.velocity.y) > this.movementSpeed) {
    //       this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.x, Mathf.Sign(this.rigidBody.velocity.y) * this.movementSpeed * this.speedMultiplier);
    //     }
    //     break;
    //   case MovementModes.UPDATE_POSITION:
    //     Vector3 currentPosition = this.transform.position;
    //     Vector3 target = Vector3.up * inputValue * this.movementSpeed * this.speedMultiplier + currentPosition;
    //     this.transform.position = Vector3.Lerp(currentPosition, target, Time.deltaTime);
    //     this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.y, 0f);
    //     break;
    //   }
    // }

    // // public bool StoppedMovingH(float value){
    // //   if(this.movementMode == MovementModes.UPDATE_POSITION){
    // //     return true;
    // //   } else if(this.movementMode == MovementModes.APPLY_FORCE && Mathf.Abs(this.rigidBody.velocity.x) <= 1f){
    // //     return true;
    // //   }
    // //   return false;
    // // }

    // // public bool StoppedMovingV(float value){
    // //   if(this.movementMode == MovementModes.UPDATE_POSITION){
    // //     return true;
    // //   } else if(this.movementMode == MovementModes.APPLY_FORCE && Mathf.Abs(this.rigidBody.velocity.y) <= 1f){
    // //     return true;
    // //   }
    // //   return false;
    // // }

    // public bool IsMovingTo(FacingDirections facingDirection, float inputValue) {
    //   switch (facingDirection) {
    //   case FacingDirections.LEFT:
    //     return inputValue < 0f;
    //   case FacingDirections.RIGHT:
    //     return inputValue > 0f;
    //   }
    //   return false;
    // }

    // public void Flip() {
    //   this.facingDirection = (FacingDirections)((int)this.facingDirection * -1);
    //   Vector3 localScale = this.transform.localScale;
    //   localScale.x *= -1;
    //   this.transform.localScale = localScale;
    // }

    // #endregion

    // #region RigidBodyValues

    // public void IgnoreGravity() {
    //   this.gravityScaleUpdates.Add(0);
    // }

    // public void RestoreGravity() {
    //   if (this.gravityScaleUpdates.Count == 0) {
    //     this.gravityScaleUpdates.Add(this.originalGravityScale);
    //   }
    // }

    // public void SetLinearDrag(float linearDrag) {
    //   this.linearDragUpdates.Add(linearDrag);
    // }

    // public void ResetLinearDrag() {
    //   if (this.linearDragUpdates.Count == 0) {
    //     this.linearDragUpdates.Add(this.originalLinearDrag);
    //   }
    // }

    // public void SetSpeedMultiplier(float multiplier) {
    //   this.speedMultiplierUpdates.Add(multiplier);
    // }

    // public void ResetSpeedMultiplier() {
    //   if (this.speedMultiplierUpdates.Count == 0) {
    //     this.speedMultiplierUpdates.Add(1f);
    //   }
    // }

    // public void SetCharge(string name, float value) {
    //   this.charges[name] = value;
    // }

    // public float GetCharge(string name) {
    //   if (!this.charges.ContainsKey(name)) {
    //     return 1f;
    //   }
    //   return this.charges[name];
    // }

    // public float ConsumeCharge(string name) {
    //   float charge = this.GetCharge(name);
    //   this.charges[name] = 1f;
    //   return charge;
    // }

    // private void UpdateValues() {
    //   foreach (float v in this.gravityScaleUpdates) {
    //     this.rigidBody.gravityScale = v;
    //   }
    //   foreach (float v in this.linearDragUpdates) {
    //     this.rigidBody.drag = v;
    //   }
    //   foreach (float v in this.speedMultiplierUpdates) {
    //     this.speedMultiplier = v;
    //   }
    //   this.gravityScaleUpdates.Clear();
    //   this.linearDragUpdates.Clear();
    //   this.speedMultiplierUpdates.Clear();
    // }

    // public void ResetXVelocity() {
    //   Vector2 velocity = this.rigidBody.velocity;
    //   velocity.x = 0f;
    //   this.rigidBody.velocity = velocity;
    // }

    // public void ResetVVelocity(bool negative = true) {
    //   if (!negative || this.rigidBody.velocity.y < 0f) {
    //     Vector2 velocity = this.rigidBody.velocity;
    //     velocity.y = 0f;
    //     this.rigidBody.velocity = velocity;
    //   }
    // }

    // #endregion




    // // public void UpdateNonTriggerColliders(bool asTrigger){
    // //   foreach(Collider2D c in this.nonTriggerColliders) {
    // //    c.isTrigger = asTrigger;
    // //   }
    // // }

    // // public void SetSpeedMultiplier(float multiplier){
    // //   this.speedMultiplier = multiplier;
    // // }

    // // public void ResetSpeedMultiplier(){
    // //   this.speedMultiplier = 1f;
    // // }


  }
}